package com.dst.dduapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class BarcodeReaderModule extends ReactContextBaseJavaModule {
    private final ReactApplicationContext reactContext;
    private BroadcastReceiver resultReceiver;
    private ScanReader scanReader ;
    public BarcodeReaderModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        scanReader = new ScanReader(reactContext);
        scanReader.init();
    }
    @NonNull
    @Override
    public String getName() {
        return "BarcodeReaderModule";
    }
    @ReactMethod
    public void startScanning() {
        // Your native module logic here
        scanReader.startScan();
    }
    @ReactMethod
    public void stopScanning() {
        // Your native module logic here
        scanReader.closeScan();
    }

    @ReactMethod
    public void registerReceiver() {
        if (resultReceiver == null) {
            resultReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    byte[] barcode = intent.getByteArrayExtra(ScanReader.SCAN_RESULT);
                    if (barcode != null) {
                        String scannedData = new String(barcode);
                        sendEvent("onScanData", scannedData);
                    }
                }
            };

            IntentFilter filter = new IntentFilter();
            filter.addAction(ScanReader.ACTION_SCAN_RESULT);
            reactContext.registerReceiver(resultReceiver, filter);
        }
    }
    private void sendEvent(String eventName, String data) {

        WritableMap params = Arguments.createMap();
        params.putString("data", data);
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
    @ReactMethod
    public void startScan() {
        // Start your barcode scanning process
        // When a barcode is successfully scanned, send an event
        String scannedData = "Scanned Data Example"; // Replace with your actual scanned data

        WritableMap params = Arguments.createMap();
        params.putString("data", scannedData);

        // Send the scanned data as an event to JavaScript
        getReactApplicationContext()
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("onScanData", params);
    }
}
