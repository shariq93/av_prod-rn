package com.dst.dduapp.scanner;

import android.content.Context;
import android.content.res.Configuration;
import android.media.SoundPool;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.example.z_android_sdk.Reader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

//import rfid.scanner.Activities.Sound;
//import rfid.scanner.Utils.MyPrefs;
import uhf.api.ActiveTag;
import uhf.api.CommandType;
import uhf.api.MultiLableCallBack;
import uhf.api.ShareData;

public class Scanner {
    public static final String TAG = "App Scanner -=-=-=-=-";
    public static Scanner scanner = null;
    // InventorActivity
    public static int Max_Tags_TempBuff = 10000;
    public static String[] tag_str_tmp = new String[Max_Tags_TempBuff];
    public static String[] tag_str_rssi = new String[Max_Tags_TempBuff];
    public static String[] tag_str_tid = new String[Max_Tags_TempBuff];
    public static String[] tag_str_temp = new String[Max_Tags_TempBuff];
    public static int tag_str_tmp_write = 0;
    public static int tag_str_tmp_Read = 0;
    public static int tagsTimes_count = 0;
    public static int newtagFlag = 0;


    public static String Str_DevMode = "P6300-191031";




    public static Reader ReaderController;

    public ScannerCallbacks scannerCallbacks;
    public static epclist_Thread epclist_thread;

    public static Reader getReader(Context context) {

        if (ReaderController == null) {
            scanner = new Scanner();
//            Str_DevMode = new MyPrefs(context).getDevMode();
            ReaderController = new Reader(Str_DevMode);
        }
        return ReaderController;
    }

    public static Scanner getScanner() {

        if (scanner == null) {
            scanner = new Scanner();

        }
        return scanner;
    }


    public void setScannerCallback(ScannerCallbacks scannerCallbacks) {
        this.scannerCallbacks = scannerCallbacks;
    }

    public static void destroyInstance() {


        try {

            ReaderController.UHF_CMD(CommandType.Read_TagTemp_Stop, null);

            Thread.sleep(100);

            Boolean ret = ReaderController.UHF_CMD(CommandType.STOP_MULTI_QUERY_TAGS_EPC, null);
            if (!ret) {
                ReaderController.UHF_CMD(CommandType.STOP_MULTI_QUERY_TAGS_EPC, null);
            }


            Thread.sleep(100);// 等thread退出



        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.i(TAG, e.toString());
        }
    }

    public void epclist_Thread_start() {
        if (epclist_thread == null) {

            tag_str_tmp = new String[Max_Tags_TempBuff];
            tag_str_rssi = new String[Max_Tags_TempBuff];
            tag_str_tid = new String[Max_Tags_TempBuff];
            tag_str_temp = new String[Max_Tags_TempBuff];
            tag_str_tmp_write = 0;
            tag_str_tmp_Read = 0;
            tagsTimes_count = 0;
            newtagFlag = 0;
            epclist_thread = new epclist_Thread();
            epclist_thread.stop = false;

            epclist_thread.start();
        }
    }


    public void epclist_Thread_stop() {
        if (epclist_thread != null) {
            epclist_thread.stop = true;
            epclist_thread = null;
        }
    }


    public class epclist_Thread extends Thread {
        public boolean stop;
        private int count = 0;

        public void run() {
            while (!stop) {
                // 处理功能

                // 通过睡眠线程来设置定时时间
                try {

                    Thread.sleep(25);
                    count = 0;
//                    Log.i(TAG, "Condition Says " + (tag_str_tmp_Read != tag_str_tmp_write));
                    while (tag_str_tmp_Read != tag_str_tmp_write) {

//                        Log.i(TAG, "showMessage " + tag_str_tid[tag_str_tmp_Read]);
                        scannerCallbacks.showMessage(tag_str_tmp[tag_str_tmp_Read],
                                tag_str_rssi[tag_str_tmp_Read],
                                tag_str_temp[tag_str_tmp_Read],
                                tag_str_tid[tag_str_tmp_Read], false);

                        tag_str_tmp_Read++;
                        if (tag_str_tmp_Read >= Scanner.Max_Tags_TempBuff)
                            tag_str_tmp_Read = 0;
                        count++;
                        if (count > 50)
                            break;// 每次显示20条，每秒800张足够，刷新频率过于频繁会导致丢失串口接收的EPC数据
                    }
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    Log.i(TAG, "Error in epc list thread\n" + e.toString());
                }
            }
        }

        ;
    }

    public interface ScannerCallbacks {
        void showMessage(String epc, String rssi, String temp, String tid,
                         Boolean ishead);

        void onConfigurationChanged(Configuration newConfig);

        void finish();
    }

}