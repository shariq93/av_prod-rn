package com.dst.dduapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.dst.dduapp.scanner.Scanner;
import com.example.z_android_sdk.Reader;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.rscja.deviceapi.Module;
import com.rscja.deviceapi.exception.ConfigurationException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import uhf.api.ActiveTag;
import uhf.api.CommandType;
import uhf.api.MultiLableCallBack;
import uhf.api.Multi_query_epc;
import uhf.api.Power;
import uhf.api.ShareData;
import uhf.api.Temperature;
import uhf.api.Version9200;
import uhf.api.Ware;

import com.facebook.react.bridge.WritableArray;

public class RFIDReaderModule extends ReactContextBaseJavaModule
        implements MultiLableCallBack, Scanner.ScannerCallbacks {
    private final ReactApplicationContext reactContext;
    Scanner scanner;

    // ===========
    boolean checkTagStatus = true;
    private GetPowerThread GetPowerThread;
    public String Str_DevMode = "P6300-191031";
    public static final boolean Language_English = true;

    private static final String TAG = "=-=-=-=";
    public Sound mSound;

    private ArrayList<Map<String, String>> receptionArrayList = new ArrayList<Map<String, String>>();
    private final String lvAdptrlabRssi = "Rssi";
    private SoundPool soundPool;
    private static Boolean isStart = false;

    private static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Date curDate = null;// = new Date(System.currentTimeMillis());
    // PROCESSING
    private Date endDate = null;// = new Date(System.currentTimeMillis());
    private long diff = 0;// = endDate.getTime() - curDate.getTime();

    private int int_power_read = 30;
    public static String ClickEPC = ""; // �����EPC��
    private Button tempBtn;
    private CheckBox mCb_temp = null;

    private ArrayAdapter<String> adapter_power;
    private int int_power = 0;
    private int int_power_temp = 0;
    private static String[] st_power = { "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18",
            "19", "20",
            "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };

    // private TextView mText_threshold_value;
    // private SeekBar mSb_threshold_value;
    public Boolean IsInView = false;// �Ƿ���μ���ҳ��

    public int NO_Filter_Mode = 0;
    public int EPC_Filter_Mode = 1;
    public int TID_Filter_Mode = 2;
    public int SoundMode = NO_Filter_Mode;

    public String SoundData = "";// ����ʲô����
    public Boolean SoundState = false;// �Ƿ�����������

    // ˫���¼���¼���һ�ε����ID
    private int lastClickId;
    // ˫���¼���¼���һ�ε����ʱ��
    private long lastClickTime;
    public Button soundBtn;

    // ASCII CheckBox
    private CheckBox mCb_ascii = null;
    public static boolean isAscii = false;

    public static Reader ReaderController;

    public String Str_Action = "Scan";
    public String Str_MuiltEPC = "Scan";
    public String Str_StopMuiltEPC = "Stop";
    public String GetVersionSuccess = Str_DevMode + " V3 Version ";
    public String GetVersionFail = "Connection failed, please restart the device";
    public String Str_ReadTags = "Reading...";
    public String Str_Prompt = "Prompt";
    public String Str_Confirm = "Confirm";
    public String Str_Cancel = "Cancel";
    public String Str_Get = "Get";
    public String Str_Set = "Set";
    public String Str_Power = "Power";
    public String Str_Success = "Success";
    public String Str_Fail = "Fail";
    public String Str_TagsNum = "TagsNum:";
    public String Str_ReadTagsTimeClick = "Please do not do any other operation because it is currently in the state of mark reading.";
    public String Str_IsExitDemo = "Exit the system or not?";
    public String Str_SoundMode0 = "no alarm when reading any tags";
    public String Str_SoundMode1 = "alarm when reading any tags";
    public String Str_SoundMode2 = "alarm when reading specific tags";
    public String StrSelectSoundMode = "Select the sound mode you want to set";
    public String StrInputData = "Input filter epc or tid";

    public int PowerMinValue = 5;
    public int PowerMaxValue = 30;
    public String PowerTempValue = "-5";
    public static Boolean IsMoudle9200 = false;
    public Boolean IsShowTemp = false;
    public Boolean LastTempIsCheck = false;
    public String SaveTagsNum = "0";

    // ===========

    ArrayList<String> list = new ArrayList();

    public RFIDReaderModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        LoadStrActionAndView();

    }

    @NonNull
    @Override
    public String getName() {
        return "RFIDReaderModule";
    }

    @ReactMethod
    public void onScanPressed() {
        ShowToast("STARTED");
        Test();

       
    }

    @ReactMethod
    public void initDevice() {
        scanner = Scanner.getScanner();
        scanner.setScannerCallback(this);
        ReaderController = Scanner.getReader(reactContext);

        ReaderController.SetCallBack(this);// ע��ص�ʹ��

       
        handleSetPower(int_power_read);
         LoadInventorView();
        LoadMainView();

        IsInView = true;

        LoadDevModeMethod();

        LoadViewHandle();
        GetPowerThread_start();
        LoadDevPowerToViewMessage();

    }

    @ReactMethod
    public void stopRFScanner() {

        Scanner.destroyInstance();
        list = new ArrayList();
        list.clear();
    }

    @ReactMethod
    public void getScannedTags(Callback successCallback) {
        // Create a new ArrayList

        // Convert ArrayList to WritableArray
        WritableArray array = Arguments.createArray();
        for (String item : list) {
            WritableMap map = Arguments.createMap();
            map.putString("tag", item);
            array.pushMap(map);
        }

        // Invoke success callback with the ArrayList
        successCallback.invoke(array);
    }

    // ================

    @Override
    public void showMessage(final String epc, final String rssi, final String temp, final String tid, Boolean ishead) {
        // Log.i(TAG, "TAGs run time==>> " + epc);
        if (!list.contains(epc)) {
           WritableMap params = Arguments.createMap();
           params.putString("tag", epc);

           getReactApplicationContext()
                   .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                   .emit("onTagFound", params);

            list.add(epc);

        }

    }

    public static Handler mHandler;// ҳ����Ϣ������

    private class GetPowerThread extends Thread {
        public void run() {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            GetPowerRange();
        }
    }

    private void GetPowerThread_start() {
        if (GetPowerThread == null) {
            GetPowerThread = new GetPowerThread();
            GetPowerThread.start();
        }
    }

    public void GetPowerRange() {
        Version9200 ver = new Version9200();
        ver.com_type = CommandType.GET_MODULE_VERSION_9200;
        ver.RssiState = (byte) 0x00;
        int count = 0;
        try {
            Boolean ret = ReaderController.UHF_CMD(CommandType.GET_MODULE_VERSION_9200, ver);
            if (ret) {
                IsMoudle9200 = true;
                if (CommandType.MODULE_VERSION_9200_POWER == (byte) 0x15) {// 15-25
                    PowerMinValue = 15;
                    PowerMaxValue = 25;
                }
                if (CommandType.MODULE_VERSION_9200_POWER == (byte) 0x25) {// 5-25
                    PowerMinValue = 5;
                    PowerMaxValue = 25;
                }
                if (CommandType.MODULE_VERSION_9200_POWER == (byte) 0x30) {// 20-30
                    PowerMinValue = 20;
                    PowerMaxValue = 30;
                }
            } else {
                IsMoudle9200 = false;
                PowerMinValue = 5;
                PowerMaxValue = 30;
            }

            Message msg = new Message();
            String[] StrMsg = new String[] { "SetPowerRange" };
            msg.obj = StrMsg;
            mHandler.sendMessage(msg);// ��Handler������Ϣ��

            msg = new Message();
            StrMsg = new String[] { "GetPower" };
            msg.obj = StrMsg;
            mHandler.sendMessage(msg);// ��Handler������Ϣ��
        } catch (Exception ex) {

        }
    }

    public void LoadDevPowerToViewMessage() {
        Message msg = new Message();
        String[] StrMsg = new String[] { "LoadDevPower" };
        msg.obj = StrMsg;
        mHandler.sendMessage(msg);// ��Handler������Ϣ��
    }

    public void LoadViewHandle() {
        HandlerThread handlerThread = new HandlerThread("MyHandlerThread");
        handlerThread.start();
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                // ��������
                String[] StrMsg = (String[]) msg.obj;
                if (StrMsg[0].equals("GetPower"))
                    handleGetPower();
                if (StrMsg[0].equals("SetPowerRange"))
                    SetPowerRange();
                if (StrMsg[0].equals("LoadDevPower")) {

                    if (Language_English)
                        ShowToast("Initializing...");
                }
                super.handleMessage(msg);
            }
        };
    }

    public void SetPowerRange() {
        st_power = new String[PowerMaxValue - PowerMinValue + 1];
        int AddStartIndex = PowerMinValue;
        for (int i = 0; i < st_power.length; i++) {
            st_power[i] = AddStartIndex + "";
            AddStartIndex++;
        }

    }

    public void LoadDevModeMethod() {
        if (Str_DevMode.equals("P6070") || true) {
            try {
                Module.getInstance().ioctl_gpio(66, true);
            } catch (ConfigurationException e) {
                // TODO �Զ����ɵ� catch ��
                e.printStackTrace();
            }

            PMStateThread_start();
        }

    }

    private PMStateThread PMStateThread;

    private class PMStateThread extends Thread {
        public boolean stop = false;

        public void run() {
            while (!stop) {

                try {
                    PowerManager pm = (PowerManager) reactContext.getSystemService(Context.POWER_SERVICE);

                    boolean isScreenOn = pm.isScreenOn();

                    if (isScreenOn == false) {
                        Module.getInstance().ioctl_gpio(66, true);
                    } else {
                        Thread.sleep(20);
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    // e.printStackTrace();
                }
            }
        }

    }

    private void PMStateThread_start() {
        if (PMStateThread == null) {
            PMStateThread = new PMStateThread();
            PMStateThread.start();
        }
    }

    private void PMStateThread_stop() {
        if (PMStateThread != null) {
            PMStateThread.stop = true;
            PMStateThread = null;
        }
    }

    public void LoadStrActionAndView() {
        if (Language_English) {
            Str_Action = "Scan";
            Str_MuiltEPC = "Scan";
            Str_StopMuiltEPC = "Stop";
            GetVersionSuccess = Str_DevMode + " V3 Version ";
            GetVersionFail = "Connection failed, please restart the device";
            Str_ReadTags = "Reading...";
            Str_Prompt = "Prompt";
            Str_Confirm = "Confirm";
            Str_Cancel = "Cancel";
            Str_Get = "Get";
            Str_Set = "Set";
            Str_Power = "Power";
            Str_Success = "Success";
            Str_Fail = "Fail";
            Str_TagsNum = "TagsNum:";
            Str_ReadTagsTimeClick = "Please do not do any other operation because it is currently in the state of mark reading.";
            Str_IsExitDemo = "Exit the system or not?";
            Str_SoundMode0 = "no alarm when reading any tags";
            Str_SoundMode1 = "alarm when reading any tags";
            Str_SoundMode2 = "alarm when reading specific tags";
            StrSelectSoundMode = "Select the sound mode you want to set";
            StrInputData = "Input filter epc or tid";

            Select_Array = new String[] { Str_SoundMode0, Str_SoundMode1, Str_SoundMode2 };
        }
    }

    // ==================================================================
    public void setTitle(String message) {
        // Toast.makeText(reactContext, message, Toast.LENGTH_SHORT).show();
        // WritableMap params = Arguments.createMap();
        // params.putString("message", message);
        //
        // getReactApplicationContext()
        // .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
        // .emit("onMessage", params);
    }

    public void LoadMainView() {

        Ware mWare = new Ware(CommandType.GET_FIRMWARE_VERSION, 0, 0, 0);
        int count = 0;
        while (true) {
            Boolean rett = ReaderController.UHF_CMD(CommandType.GET_FIRMWARE_VERSION, mWare);
            if (rett) {
                Log.e("TAG", "Ver." + mWare.major_version + "." + mWare.minor_version + "."
                        + mWare.revision_version);
                String Str_Ver = "Ver." + mWare.major_version + "." + mWare.minor_version + "."
                        + mWare.revision_version;
                setTitle(GetVersionSuccess + Str_Ver);
                break;
            }

            count++;
            if (count > 5) {
                setTitle(GetVersionFail);

                break;
            }
        }

    }

    public void LoadInventorView() {
       
        if(mSound==null){
          mSound = new Sound(reactContext);
          soundPool = new SoundPool(10, AudioManager.STREAM_SYSTEM, 0);
          soundPool.load(reactContext, R.raw.duka3, 1);
          SoundThread_start();// �̶߳�ʱ����Ƿ񷢶�����
        }
        

      

    }

    private SoundThread Soundthread;

    private class SoundThread extends Thread {
        public boolean stop;

        public void run() {
            while (!stop) {

                try {
                    Thread.sleep(40);
                    if (Scanner.newtagFlag > 25)
                        Scanner.newtagFlag = 25;// һ�������25�Σ������������󣬶�������ʱ
                    if (Scanner.newtagFlag > 0) {
                        Scanner.newtagFlag--;
                        // if(mCb_sound.isChecked())
                        if (SoundState) {
                            soundPool.play(1, 1, 1, 1, 0, 1);
                        }
                    }
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        ;
    }

    private void SoundThread_start() {
        if (Soundthread == null) {
            Soundthread = new SoundThread();
            Soundthread.start();
        }
    }

    

    Toast toast = null;

    public void ShowToast(String Msg) {

        if (toast != null) {
            toast.cancel();
            toast = null;
        }
        toast = Toast.makeText(reactContext, Msg, Toast.LENGTH_SHORT);// .show();
        toast.setGravity(Gravity.TOP | Gravity.CENTER, 3, 1350);

        toast.show();
    }

    public void Test() {
        int count = 0;
        list.clear();
        list = new ArrayList();
        if (Str_Action.equals(Str_MuiltEPC)) {
            scanner.epclist_Thread_start();
            // MULTI_QUERY_TAGS_EPC
            Multi_query_epc mMulti_query_epc = new Multi_query_epc();
            mMulti_query_epc.query_total = 0;

            curDate = new Date(System.currentTimeMillis());
            ReaderController.UHF_CMD(CommandType.MULTI_QUERY_TAGS_EPC, mMulti_query_epc);

            isStart = true;
            setTitle(Str_ReadTags);

            // Str_Action = Str_MuiltEPC;
        } else {
            Boolean ret = ReaderController.UHF_CMD(CommandType.STOP_MULTI_QUERY_TAGS_EPC, null);
            if (ret) {
                if (SoundState)
                    mSound.callAlarm(true, 100);
                setTitle("Stop Ok");
                isStart = false;

                Str_Action = Str_MuiltEPC;
            } else {
                if (count == 0) {
                    ReaderController.UHF_CMD(CommandType.STOP_MULTI_QUERY_TAGS_EPC, null);
                    count++;
                } else {
                    if (SoundState)
                        mSound.callAlarm(false, 100);
                    setTitle("Stop Fail");
                }
            }
        }
    }

    @Override
    public void finish() {
        // TODO Auto-generated method stub
        try {
            int count = 0;
            ReaderController.UHF_CMD(CommandType.Read_TagTemp_Stop, null);

            Boolean ret = ReaderController.UHF_CMD(CommandType.STOP_MULTI_QUERY_TAGS_EPC, null);
            if (ret) {
                if (SoundState)
                    mSound.callAlarm(true, 100);
                setTitle("Stop Ok");

                isStart = false;

                Str_Action = Str_MuiltEPC;
            } else {
                if (count == 0) {
                    ReaderController.UHF_CMD(CommandType.STOP_MULTI_QUERY_TAGS_EPC, null);
                    count++;
                } else {
                    if (SoundState)
                        mSound.callAlarm(false, 100);
                    setTitle("Stop Fail");
                }
            }
        } catch (Exception e) {
            Log.i(TAG, "Finish ma hai error\n" + e.toString());
        }
    }

    public void SoundHandle(String epc, String tid) {
        if (SoundState == true && SoundData.equals("") == true)// ����
        {
            Scanner.newtagFlag++;
        } else if (SoundState == false && SoundData.equals("") == true)// ������
        {

        } else if (SoundState == true && SoundData.equals("") != true)// ָ�����ݲ���
        {
            if (SoundMode == EPC_Filter_Mode) {
                if (SoundData.equals(epc)) {
                    Scanner.newtagFlag++;
                }
            }

            if (SoundMode == TID_Filter_Mode) {
                if (SoundData.equals(tid)) {
                    Scanner.newtagFlag++;
                }
            }

        }
    }

    int count = 0;

    private void handleGetPower() {

        // TODO Auto-generated method stub
        Power mPower = new Power();
        mPower.com_type = CommandType.GET_POWER;
        mPower.loop = 0;
        mPower.read = 0;
        mPower.write = 0;

        Boolean ret = ReaderController.UHF_CMD(CommandType.GET_POWER, mPower);
        if (ret) {
            int read = mPower.read;

            ShowToast(Str_Get + Str_Power + Str_Success + ":" + mPower.read + "dBm");
            mSound.callAlarm(true, 100);
        } else {
            if (count < 3) {
                count++;
                handleGetPower();
            } else {
                this.setTitle("Get Power Fail");

                ShowToast(Str_Get + Str_Power + Str_Fail);
                mSound.callAlarm(false, 100);
                count = 0;
            }
        }
    }

    private void handleSetPower(int ReadPower) {
        int count = 0;
        // TODO Auto-generated method stub
        Power mPower = new Power();
        mPower.com_type = CommandType.SET_POWER;
        mPower.loop = 0;
        mPower.read = ReadPower;
        mPower.write = ReadPower;
        // mPower.write = 25;

        Boolean ret = ReaderController.UHF_CMD(CommandType.SET_POWER, mPower);
        if (ret) {
            this.setTitle(Str_Set + Str_Power + Str_Success + ":" + ReadPower + "dBm");
            // this.setTitle(Str_Set + Str_Power + Str_Success+":"+ ReadPower+"dBm" +
            // "[д����Ϊ25dBm]");
            // mText_threshold_value.setText(int_power_read + "dBm");
            // mSb_threshold_value.setProgress(int_power_read);
           // mSound.callAlarm(true, 100);
        } else {
            this.setTitle(Str_Set + Str_Power + Str_Fail);
           // mSound.callAlarm(false, 100);
        }
    }

    // ===========================================================
    String[] Select_Array = new String[] { Str_SoundMode0, Str_SoundMode1, Str_SoundMode2 };

    public int RadioIndex = 0;

    @Override
    public void BlueToothBtn() {
        // TODO �Զ����ɵķ������

    }

    @Override
    public void BlueToothBtnNew(int i) {

    }

    @Override
    public void BlueToothVoltageNew(int i, byte b, byte b1, int i1, int i2, int i3) {

    }

    @Override
    public void BlueToothVoltage(int arg0) {
        // TODO �Զ����ɵķ������

    }

    @Override
    public void RecvActiveTag(ActiveTag arg0) {
        // TODO �Զ����ɵķ������

    }

    long currenttime = 0;

    @Override
    public void method(byte[] data) {
        // TODO �Զ����ɵķ������
        if (data.length <= 0) {
            return;
        }
        Scanner.tagsTimes_count++;

        byte msb = data[0];
        byte lsb = data[1];
        int pc = (msb & 0x00ff) << 8 | (lsb & 0x00ff);
        pc = (pc & 0xf800) >> 11;

        byte[] tmp = new byte[pc * 2];
        System.arraycopy(data, 2, tmp, 0, tmp.length);
        String str_tmp = ShareData.CharToString(tmp, tmp.length);
        str_tmp = str_tmp.replace(" ", "");

        byte[] tid = new byte[data.length - 2 - (pc * 2) - 3];
        System.arraycopy(data, 2 + (pc * 2), tid, 0, tid.length);
        String str_tid = ShareData.CharToString(tid, tid.length);
        str_tid = str_tid.replace(" ", "");

        // String str_rssi = "" + rssi_calculate(data[2 + pc * 2 + tid.length], data[2 +
        // pc * 2 + 1 + tid.length]);
        String str_rssi = "" + (~((short) (((data[2 + pc * 2 + tid.length] & 0xFF) << 8)
                | (data[2 + pc * 2 + 1 + tid.length] & 0xFF) - 1)) / -10.0);

        Scanner.tag_str_tmp[Scanner.tag_str_tmp_write] = str_tmp;
        Scanner.tag_str_rssi[Scanner.tag_str_tmp_write] = str_rssi;
        if (!Language_English)
            Scanner.tag_str_temp[Scanner.tag_str_tmp_write] = "��";
        if (Language_English)
            Scanner.tag_str_temp[Scanner.tag_str_tmp_write] = "No";

        if (str_tid.length() == 0) {
            Scanner.tag_str_tid[Scanner.tag_str_tmp_write] = "[TID]";
        } else {
            Scanner.tag_str_tid[Scanner.tag_str_tmp_write] = str_tid;
        }

        Scanner.tag_str_tmp_write++;
        if (Scanner.tag_str_tmp_write >= Scanner.Max_Tags_TempBuff)
            Scanner.tag_str_tmp_write = 0;
        // showMessage(str_tmp,str_rssi,false);//��ֹ�ڴ��ڽ�����ֱ������ʾ���ᵼ�´����������ݶ�ʧ20190322���һ�
        SoundHandle(str_tmp, str_tid);
    }

    @Override
    public void CmdRespond(String[] result) {
        // TODO �Զ����ɵķ������
        callCmdRespond(result);
    }

    public static void callCmdRespond(String[] result) {
        if (result[1].equals("F8"))// LTU31���¶�
        {
            Scanner.tagsTimes_count++;
            Log.i(TAG, result.toString());
            Scanner.tag_str_tmp[Scanner.tag_str_tmp_write] = result[2];
            Scanner.tag_str_rssi[Scanner.tag_str_tmp_write] = result[3];
            Scanner.tag_str_temp[Scanner.tag_str_tmp_write] = result[4];
            Scanner.tag_str_tid[Scanner.tag_str_tmp_write] = "[TID]";

            Scanner.tag_str_tmp_write++;
            if (Scanner.tag_str_tmp_write >= Scanner.Max_Tags_TempBuff)
                Scanner.tag_str_tmp_write = 0;

            // SoundHandle(result[2], "");
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

    }
}
