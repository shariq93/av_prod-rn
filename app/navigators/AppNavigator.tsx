/**
 * The app navigator (formerly "AppNavigator" and "MainNavigator") is used for the primary
 * navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow which the user will use once logged in.
 */
import {
  DarkTheme,
  DefaultTheme,
  NavigationContainer,
} from "@react-navigation/native"
import { createNativeStackNavigator, NativeStackScreenProps } from "@react-navigation/native-stack"
import { observer } from "mobx-react-lite"
import React from "react"
import { useColorScheme } from "react-native"
import * as Screens from "app/screens"
import Config from "../config"
import { navigationRef, useBackButtonHandler } from "./navigationUtilities"
import { colors } from "app/theme"
import { useStores } from "app/models"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * If no params are allowed, pass through `undefined`. Generally speaking, we
 * recommend using your MobX-State-Tree store(s) to keep application state
 * rather than passing state through navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 *   https://reactnavigation.org/docs/typescript/#organizing-types
 */
export type AppStackParamList = {
  Welcome: undefined
  // 🔥 Your screens go here
  Login: undefined
  Home: undefined
  ContainerSelection: undefined
  FlowOneHome: undefined
  ContainerDetail: undefined
  AssignToDriver: undefined


  StockOutBarcode: undefined
  FlowTwoHome: undefined
  StockInCustomer: undefined
  StockOutCustomer: undefined
  DriverHome: undefined
  DriverDeliveries: undefined
	DeliveryDetails: undefined
	SubmitDelivery: undefined
	TestRfid: undefined
	MaintainanceAssets: undefined
	CheckStatus: undefined
	BundleDetails: undefined
	ProjectDetails: undefined
	LoanOut: undefined
	Scanner: undefined
	Component: undefined
	ScannerLoanReturn: undefined
	LoanReturn: undefined
	ScannerLoanOut: undefined
	BundleScanner: undefined
	ProjectScanner: undefined
	AssetScanner: undefined
	AssetsLoanSummary: undefined
	ProjectBundleScanner: undefined
	ForceLoanSummary: undefined
	// IGNITE_GENERATOR_ANCHOR_APP_STACK_PARAM_LIST
}

/**
 * This is a list of all the route names that will exit the app if the back button
 * is pressed while in that screen. Only affects Android.
 */
const exitRoutes = Config.exitRoutes

export type AppStackScreenProps<T extends keyof AppStackParamList> = NativeStackScreenProps<
  AppStackParamList,
  T
>

// Documentation: https://reactnavigation.org/docs/stack-navigator/
const Stack = createNativeStackNavigator<AppStackParamList>()

const AppStack = observer(function AppStack() {
  const { auth } = useStores()
  let screenname: "Login" | "Home" | "DriverHome" = "Login"

  // screenname = 'Home'
  if (auth?.isLogin) screenname = 'Home'
  else   screenname = 'Login'
  
  return (
    <Stack.Navigator
      initialRouteName={screenname}
      screenOptions={{ headerShown: false, navigationBarColor: colors.background }}
    >
      {/** 🔥 Your screens go here */}
      <Stack.Screen name="Login" component={Screens.LoginScreen} />
      <Stack.Screen name="Home" component={Screens.HomeScreen} />
      
			<Stack.Screen name="TestRfid" component={Screens.TestRfidScreen} />
			<Stack.Screen name="MaintainanceAssets" component={Screens.MaintainanceAssetsScreen} />
			<Stack.Screen name="CheckStatus" component={Screens.CheckStatusScreen} />
			<Stack.Screen name="BundleDetails" component={Screens.BundleDetailsScreen} />
			<Stack.Screen name="ProjectDetails" component={Screens.ProjectDetailsScreen} />
			<Stack.Screen name="LoanOut" component={Screens.LoanOutScreen} />
			<Stack.Screen name="Scanner" component={Screens.ScannerScreen} />

			<Stack.Screen name="ScannerLoanReturn" component={Screens.ScannerLoanReturnScreen} />

			<Stack.Screen name="ScannerLoanOut" component={Screens.ScannerLoanOutScreen} />
			<Stack.Screen name="BundleScanner" component={Screens.BundleScannerScreen} />
			<Stack.Screen name="ProjectScanner" component={Screens.ProjectScannerScreen} />
			<Stack.Screen name="AssetScanner" component={Screens.AssetScannerScreen} />
			<Stack.Screen name="AssetsLoanSummary" component={Screens.AssetsLoanSummaryScreen} />
			<Stack.Screen name="ForceLoanSummary" component={Screens.ForceLoanSummaryScreen} />
			<Stack.Screen name="ProjectBundleScanner" component={Screens.ProjectBundleScannerScreen} />
			{/* IGNITE_GENERATOR_ANCHOR_APP_STACK_SCREENS */}

    </Stack.Navigator>
  )
})

export interface NavigationProps
  extends Partial<React.ComponentProps<typeof NavigationContainer>> { }

export const AppNavigator = observer(function AppNavigator(props: NavigationProps) {
  const colorScheme = useColorScheme()

  useBackButtonHandler((routeName) => exitRoutes.includes(routeName))

  return (
    <NavigationContainer
      ref={navigationRef}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
      {...props}
    >
      <AppStack />
    </NavigationContainer>
  )
})
