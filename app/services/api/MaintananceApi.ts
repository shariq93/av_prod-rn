import { ApiResponse } from "apisauce"
import { UserSnapshotIn, User, MainDb, Asset, Loans, MaintainanceAsset } from "app/models"
import { api } from "./api"
import {
  AllLoansResponse,
  GeneralResponse,
  LoanedAssetsResponse,
  LoginResponse,
  MainDBResponse,
  MaintainanceResponse,
} from "./api.types"
import { GeneralApiProblem, getGeneralApiProblem } from "./apiProblem"
import { loadString } from "app/utils/storage"

export class MaintananceApi {
  async fetchAll(): Promise<{ kind: "ok"; data: MaintainanceAsset[] } | GeneralApiProblem> {
    const token = await loadString("token")
    const response: ApiResponse<MaintainanceResponse> = await api.apisauce.get(
      `/getMaintainanceAssets`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return { ...problem, message: response.data.message }
    }

    try {
      const data: MaintainanceAsset[] = response.data.data
      if (response.data.status) {
        return { kind: "ok", data }
      } else {
        return { kind: "unauthorized", message: response.data.message }
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
  async removeAsset(id): Promise<{ kind: "ok" } | GeneralApiProblem> {
    const token = await loadString("token")
    const response: ApiResponse<GeneralResponse> = await api.apisauce.delete(
      `/removeMaintainance/${id}`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return { ...problem, message: response.data.message }
    }

    try {
      if (response.ok) {
        return { kind: "ok" }
      } else {
        return { kind: "unauthorized", message: response.data.message }
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
  async addAsset(id): Promise<{ kind: "ok" } | GeneralApiProblem> {
    const token = await loadString("token")
    var formdata = new FormData()
    formdata.append("asset_id", `[${id}]`)
    const response: ApiResponse<GeneralResponse> = await api.apisauce.post(
      `/addMaintainance`,
      formdata,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return { ...problem, message: response.data.message }
    }

    try {
      if (response.ok) {
        return { kind: "ok" }
      } else {
        return { kind: "unauthorized", message: response.data.message }
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
}
