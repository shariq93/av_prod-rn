import { Asset, Bundles, Loans, MainDb, MaintainanceAsset, Projects, User } from "app/models"

/**
 * These types indicate the shape of the data you expect to receive from your
 * API endpoint, assuming it's a JSON object like we have.
 */
export interface LoginResponse {
  status:boolean,
  data:User,
  message:string
}





export interface GeneralResponse {
  status:boolean,
  data:any,
  message:string
}
export interface MainDBResponse {
  status:string,
  data:MainDb,
  message:string
}
export interface MaintainanceResponse {
  status:string,
  data:MaintainanceAsset[],
  message:string
}

export interface LoanedAssetsResponse {
  status:string,
  data:Asset[],
  message:string
}
export interface AllBundleResponse {
  status:string,
  data:Bundles[],
  message:string
}
export interface AllProjectsResponse {
  status:string,
  data:Projects[],
  message:string
}
export interface AllUsersResponse {
  status:string,
  data:User[],
  message:string
}
export interface AllLoansResponse {
  status:string,
  data:Loans
  message:string
}



/**
 * The options used to configure apisauce.
 */
export interface ApiConfig {
  /**
   * The URL of the api.
   */
  url: string

  /**
   * Milliseconds before we timeout the request.
   */
  timeout: number
}
