import { ApiResponse } from "apisauce";
import { UserSnapshotIn, User, MainDb, Asset, Loans, Bundles, Projects } from "app/models";
import { api } from "./api";
import { AllBundleResponse, AllLoansResponse, AllProjectsResponse, AllUsersResponse, GeneralResponse, LoanedAssetsResponse, LoginResponse, MainDBResponse, } from "./api.types";
import { GeneralApiProblem, getGeneralApiProblem } from "./apiProblem";
import { loadString } from "app/utils/storage";

export class MainDbApi{

  async fetchAll(): Promise<{ kind: "ok"; data: MainDb[]; } | GeneralApiProblem> {
    // const token = await loadString('token')
    const response: ApiResponse<MainDBResponse> = await api.apisauce.get(`/sync/data`, {},
    {
      // headers: {
      //   Authorization: `Bearer ${token}`,
      // }
    },)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return {...problem,message:response.data.message}
    }

    try {
      const data: MainDb = response.data.data
      if (response.data.status) {
        return { kind: "ok", data }
      } else {
        return { kind: "unauthorized",message: response.data.message}
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
  async fetchAllAssets(): Promise<{ kind: "ok"; data: Asset[]; } | GeneralApiProblem> {
    const token = await loadString('token')
    const response: ApiResponse<LoanedAssetsResponse> = await api.apisauce.get(`/assets`, {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      }
    },)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return {...problem,message:response.data.message}
    }

    try {
      const data: Asset[] = response.data.data
      if (response.data.status) {
        return { kind: "ok", data }
      } else {
        return { kind: "unauthorized",message: response.data.message}
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
  async fetchAllBundles(): Promise<{ kind: "ok"; data: Bundles[]; } | GeneralApiProblem> {
    const token = await loadString('token')
    const response: ApiResponse<AllBundleResponse> = await api.apisauce.get(`/bundles`, {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      }
    },)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return {...problem,message:response.data.message}
    }

    try {
      const data: Bundles[] = response.data.data
      if (response.data.status) {
        return { kind: "ok", data }
      } else {
        return { kind: "unauthorized",message: response.data.message}
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
  async fetchAllProjects(): Promise<{ kind: "ok"; data: Projects[]; } | GeneralApiProblem> {
    const token = await loadString('token')
    const response: ApiResponse<AllProjectsResponse> = await api.apisauce.get(`/projects`, {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      }
    },)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return {...problem,message:response.data.message}
    }

    try {
      const data: Projects[] = response.data.data
      if (response.data.status) {
        return { kind: "ok", data }
      } else {
        return { kind: "unauthorized",message: response.data.message}
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
  async fetchAllUsers(): Promise<{ kind: "ok"; data: User[]; } | GeneralApiProblem> {
    const token = await loadString('token')
    const response: ApiResponse<AllUsersResponse> = await api.apisauce.get(`/users`, {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      }
    },)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return {...problem,message:response.data.message}
    }

    try {
      const data: User[] = response.data.data
      if (response.data.status) {
        return { kind: "ok", data }
      } else {
        return { kind: "unauthorized",message: response.data.message}
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
  async getAllLoans(): Promise<{ kind: "ok"; data: Loans; } | GeneralApiProblem> {
    const token = await loadString('token')
    const response: ApiResponse<AllLoansResponse> = await api.apisauce.get(`/available/loans`, {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      }
    },)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return {...problem,message:response.data.message}
    }

    try {
      const data: Loans = response.data.data
      if (response.data.status) {
        return { kind: "ok", data }
      } else {
        return { kind: "unauthorized",message: response.data.message}
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
}