import { ApiResponse } from "apisauce";
import { UserSnapshotIn, User } from "app/models";
import { api } from "./api";
import { LoginResponse } from "./api.types";
import { GeneralApiProblem, getGeneralApiProblem } from "./apiProblem";
import { loadString } from "app/utils/storage";

export class AuthApi{
  async login(
    params,
  ): Promise<{ kind: "ok"; user: UserSnapshotIn; token: string } | GeneralApiProblem> {
    const response: ApiResponse<LoginResponse> = await api.apisauce.post(`/login`, params)
   
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return {...problem,message:response.data.message}
    }

    try {
      const user: User = response.data.data
      if (response.data.status) {
        return { kind: "ok", user, token: user.token }
      } else {
        return { kind: "unauthorized",message: response.data.message}
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data",message: "Opps! something went wrong!"}
    }
  }
  async refreshToken(): Promise<{ kind: "ok"; user: UserSnapshotIn; token: string } | GeneralApiProblem> {
    const token = await loadString('token')
    const response: ApiResponse<LoginResponse> = await api.apisauce.get(`/refresh`, {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      }
    },)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return {...problem,message:response.data.message}
    }

    try {
      const user: User = response.data.data
      if (response.data.status) {
        return { kind: "ok", user, token: user.token }
      } else {
        return { kind: "unauthorized",message: response.data.message}
      }
    } catch (e) {
      if (__DEV__) {
        console.tron.error(`Bad data: ${e.message}\n${response.data}`, e.stack)
      }
      return { kind: "bad-data" }
    }
  }
}