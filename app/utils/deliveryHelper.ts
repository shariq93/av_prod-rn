import { Barcode, Customer, Driver, Product } from "app/models"

/**
 * Generates a message based on the provided unscanned barcode information.
 *
 * @param {Array} unScannedCodes - An array of objects representing unscanned barcodes.
 * @returns {string} - A message indicating the status of the barcodes and whether to continue.
 */
export const generateBarcodeMessage = (unScannedCodes) => {
  if (unScannedCodes.length > 0) {
    // If there are unscanned barcodes, construct a message listing them
    const barcodes = unScannedCodes
      .map((items) => {
        return items.barcode_number
      })
      .join(", ")

    // Return a message indicating the number of unscanned barcodes
    return `You have not scanned ${unScannedCodes.length} barcodes.\n${barcodes} will not be part of this DO.\nAre you sure you want to continue?`
  } else {
    // If there are no unscanned barcodes, return a default message
    // asking if the user wants to create a DO with the provided barcodes.
    return "Are you sure you want to create a DO with these barcodes?"
  }
}

/**
 * Generates a payload object based on the provided scanned barcode information.
 *
 * @param {Array} scannedCodes - An array of objects representing scanned barcodes.
 * @returns {Object} - A payload object containing container number and barcode numbers.
 */
export const generatePayload = (scannedCodes: Barcode[]) => {
  const container_number = scannedCodes.length > 0 ? scannedCodes[0].container_number : ""

  const barcodes = []
  scannedCodes.forEach((item, index) => {
    barcodes.push(item)
  })

  // Return a payload object containing container number and barcode numbers.
  return { container_number, barcodes }
}
export const generateStockoutPayload = (scannedCodes: Barcode[], driver: Driver, date: string) => {
  const barcodes = []
  scannedCodes.forEach((item, index) => {
    barcodes.push(item)
  })


  return { barcodes, driver_id: driver.id+"", delivery_date: date }
}
export const generateStockInByCustomerPayload = (scannedCodes: Product[]) => {
  const barcodes = []
  scannedCodes.forEach((item, index) => {
    barcodes.push(item)
  })


  return { barcodes }
}
export const generateStockOutByCustomerPayload = (scannedCodes: Product[], delivery_date: string,) => {
  const barcodes = []
  scannedCodes.forEach((item, index) => {
    barcodes.push(item)
  })


  return { barcodes, delivery_date }
}

export const generatStockoutMessage = (count: number, driver: Driver, date: string) => {
  return `Are you sure want to assign ${count} barcode(s) to Driver "${driver?.first_name?.toUpperCase()} ${driver?.last_name?.toUpperCase()}"\n on Date ${date}`
}
export const generatStockouCustomertMessage = (count: number) => {
  return `Are you sure want to stock in ${count} barcode(s)"`
}
