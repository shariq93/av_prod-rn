import { useCallback } from "react"

export const useDebouncedCallback = (callback: (text: string) => void, delay: number) => {
    let timerId: NodeJS.Timeout
    return useCallback(
      (text: string, ...args: any[]) => {
        if (timerId) {
          clearTimeout(timerId)
        }
        timerId = setTimeout(() => {
          callback(text, ...args)
        }, delay)
      },
      [callback, delay],
    )
  }

export const getValueOfKeys = (obj, keys) => {
  if (!keys) return ""
  return keys.map((key) => obj[key]).join(" ")
}

export function difference(arr1, arr2) {
  return arr1.filter(element => !arr2.includes(element));
}