import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { AssetModel } from "./Asset"
import { MaintainanceAssetModel } from "./MaintainanceAsset"
import { api } from "app/services/api"

/**
 * Model description here for TypeScript hints.
 */
export const MaintainanceAssetsModel = types
  .model("MaintainanceAssets")
  .props({
    assets: types.optional(types.array(MaintainanceAssetModel), []),
    isLoading: types.optional(types.boolean, false),
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    async fetchAll(){
      self.setProp("isLoading", true)
      const resp = await api.maintanance.fetchAll()
      if (resp.kind == "ok") {
        self.setProp("assets", resp.data)
      }
      self.setProp("isLoading", false)
    },
    async removeAsset(id){
      self.setProp("isLoading", true)
      const resp = await api.maintanance.removeAsset(id)
      if (resp.kind == "ok") {
       this.fetchAll()
      }
    },
    async addAsset(assetId){
      self.setProp("isLoading", true)
      const resp = await api.maintanance.addAsset(assetId)
      if (resp.kind == "ok") {
       this.fetchAll()
      }
    }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface MaintainanceAssets extends Instance<typeof MaintainanceAssetsModel> {}
export interface MaintainanceAssetsSnapshotOut
  extends SnapshotOut<typeof MaintainanceAssetsModel> {}
export interface MaintainanceAssetsSnapshotIn extends SnapshotIn<typeof MaintainanceAssetsModel> {}
export const createMaintainanceAssetsDefaultModel = () =>
  types.optional(MaintainanceAssetsModel, {})
