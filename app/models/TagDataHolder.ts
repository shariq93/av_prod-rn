import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { TagDataModel } from "./TagData"
import { User, UserModel } from "./User"

/**
 * Model description here for TypeScript hints.
 */
export const TagDataHolderModel = types
  .model("TagDataHolder")
  .props({
    tagData: types.optional(types.array(TagDataModel), []),
    forLoanOut: types.optional(types.boolean, false),
    loanee: types.optional(UserModel, {}),
    bundleId: types.optional(types.string, ""),
    projectId: types.optional(types.string, ""),
    projectName: types.optional(types.string, ""),
    bundleName: types.optional(types.string, ""),
    selectedTags: types.optional(types.array(TagDataModel), []),
    projectAsset: types.optional(types.array(TagDataModel), []),
    scannedTags: types.optional(types.array(types.string), []),
    notScannedTags: types.optional(types.array(TagDataModel), []),
    returnData: types.optional(types.string, ""),
    showSummaryMessage: types.optional(types.boolean, true),
    remarks: types.optional(types.string, ""),

    //only using 'scannableTags' in Projects fot tracking internal assets on bundles

    scannableTags: types.optional(types.array(types.string), []),
  })
  .actions(withSetPropAction)
  .views((self) => ({
    getAllTagsIds(): string[] {
      let allTags: string[] = []
      self.tagData.forEach((tagData) => {
        allTags = allTags.concat(tagData.getAllTagsIds())
      })
      return allTags
    },
    getAllTags(): any[] {
      let allTags: any[] = []
      self.tagData.forEach((tagData) => {
        allTags = allTags.concat(tagData.getAllTags())
      })
      return allTags
    },
    getAllTagsForProject(): any[] {
      let allTags: any[] = []
      self.tagData.forEach((tagData) => {
        allTags = allTags.concat(tagData.getAllTagsForProjects())
      })
      return allTags
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    addTagData(tagData: any) {
      self.tagData.push(tagData)
    },
    addScannableTags(tagData: any) {
      self.scannableTags.push(tagData)
    },
    addMutiScannableTags(tags: any) {
      // self.scannableTags.push(tagData)
      self.setProp("scannableTags", tags)
    },
    
    addScannedTags(tags: any[]) {
      self.setProp("scannedTags", tags)
    },
    setProjectAssets(tags: any[]) {
      self.setProp("projectAsset", tags)
    },
    setBundleId(id: any) {
      self.setProp("bundleId", id + "")
    },
    setShowSummaryMessage(show: boolean) {
      self.setProp("showSummaryMessage", show)
    },
    setBundleName(name: any) {
      self.setProp("bundleName", name + "")
    },
    setProjectName(name: any) {
      self.setProp("projectName", name + "")
    },
    setSelectedTags(tags: any[]) {
      self.setProp("selectedTags", tags)
    }, 
    setRemarks(remarks: string) {
      self.setProp('remarks', remarks)
    },
    setNotScannedTags(tags: any[]) {
      self.setProp('notScannedTags', tags)
    },
    setProjectId(id: any) {
      self.setProp("projectId", id + "")
    },
    setReturnData(data: any) {
      self.setProp("returnData", data + "")
    },
    setLoanee(user: User) {
      self.setProp("loanee", { ...user })
    },
    isForLoanout(flag: boolean) {
      self.setProp("forLoanOut", flag)
    },

    clearTagDataArray() {
      self.tagData.clear()
      self.selectedTags.clear()
      self.scannedTags.clear()
      this.setLoanee(undefined)
      this.setBundleId("")
      this.setProjectId("")
      this.setReturnData("")
      this.setRemarks("")
      this.isForLoanout(false)
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface TagDataHolder extends Instance<typeof TagDataHolderModel> {}
export interface TagDataHolderSnapshotOut extends SnapshotOut<typeof TagDataHolderModel> {}
export interface TagDataHolderSnapshotIn extends SnapshotIn<typeof TagDataHolderModel> {}
export const createTagDataHolderDefaultModel = () => types.optional(TagDataHolderModel, {})
