import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { BundlesModel } from "./Bundles"
import { AssetModel } from "./Asset"

/**
 * Model description here for TypeScript hints.
 */
export const ProjectsModel = types
  .model("Projects")
  .props({
    id:types.optional(types.number,-1),
    name: types.optional(types.string, ''),
    tagid: types.maybeNull(types.string),
    company_id: types.optional(types.string, '0'),
    // remarks: types.optional(types.string, ""),
    bundles:types.optional(types.array(BundlesModel),[]),
    assets:types.optional( types.array(AssetModel), []),
    loan_status: types.optional(types.string, '0'),

  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Projects extends Instance<typeof ProjectsModel> {}
export interface ProjectsSnapshotOut extends SnapshotOut<typeof ProjectsModel> {}
export interface ProjectsSnapshotIn extends SnapshotIn<typeof ProjectsModel> {}
export const createProjectsDefaultModel = () => types.optional(ProjectsModel, {})
