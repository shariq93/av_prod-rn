import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"

/**
 * Model description here for TypeScript hints.
 */
export const LogsModel = types
  .model("Logs")
  .props({})
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Logs extends Instance<typeof LogsModel> {}
export interface LogsSnapshotOut extends SnapshotOut<typeof LogsModel> {}
export interface LogsSnapshotIn extends SnapshotIn<typeof LogsModel> {}
export const createLogsDefaultModel = () => types.optional(LogsModel, {})
