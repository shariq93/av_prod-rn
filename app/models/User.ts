import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { saveUser, setIsLogin } from "app/utils/storage"
import { api } from "app/services/api"

/**
 * Model description here for TypeScript hints.
 */
export const UserModel = types
  .model("User")
  .props({
    id: types.optional(types.number, -1),
    name: types.optional(types.string, ""),
    email: types.optional(types.string, ""),
    // "email_verified_at": types.optional(types.string, ""),
    role: types.optional(types.string, ""),
    tagid: types.maybeNull(types.string),
    company_id: types.optional(types.string, ""),
    token: types.optional(types.string, ""),
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface User extends Instance<typeof UserModel> {}
export interface UserSnapshotOut extends SnapshotOut<typeof UserModel> {}
export interface UserSnapshotIn extends SnapshotIn<typeof UserModel> {}
export const createUserDefaultModel = () => types.optional(UserModel, {})
