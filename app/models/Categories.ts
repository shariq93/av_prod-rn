import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"

/**
 * Model description here for TypeScript hints.
 */
export const CategoriesModel = types
  .model("Categories")
  .props({
    id: types.optional(types.number, 0),
    name: types.optional(types.string, ""),
    brand_id: types.maybeNull(types.string),
    image: types.maybeNull(types.string),
    company_id: types.optional(types.string, ""),
   
  
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Categories extends Instance<typeof CategoriesModel> {}
export interface CategoriesSnapshotOut extends SnapshotOut<typeof CategoriesModel> {}
export interface CategoriesSnapshotIn extends SnapshotIn<typeof CategoriesModel> {}
export const createCategoriesDefaultModel = () => types.optional(CategoriesModel, {})
