import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { TagDataModel } from "./TagData"
import { UserModel } from "./User"

/**
 * Model description here for TypeScript hints.
 */
export const ProjectTagHolderModel = types
  .model("ProjectTagHolder")
  .props({
    project: types.optional(TagDataModel, {}),
    bundle: types.optional(types.array(TagDataModel), []),
    loanee: types.optional(UserModel, {}),
    bundleId: types.optional(types.string, ""),
    projectId: types.optional(types.string, ""),
    projectName: types.optional(types.string, ""),
    bundleName: types.optional(types.string, ""),
    selectedTags: types.optional(types.array(TagDataModel), []),
    scannedTags:types.optional(types.array(types.string), []),
    returnData: types.optional(types.string, ""),
    showSummaryMessage: types.optional(types.boolean, true),
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface ProjectTagHolder extends Instance<typeof ProjectTagHolderModel> {}
export interface ProjectTagHolderSnapshotOut extends SnapshotOut<typeof ProjectTagHolderModel> {}
export interface ProjectTagHolderSnapshotIn extends SnapshotIn<typeof ProjectTagHolderModel> {}
export const createProjectTagHolderDefaultModel = () => types.optional(ProjectTagHolderModel, {})
