import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { AssetModel } from "./Asset"

import { BrandsModel } from "./Brands"
import { CategoriesModel } from "./Categories"
import { BundlesModel } from "./Bundles"
import { LoanedAsset, LoansModel } from "./Loans"
import { ProjectsModel } from "./Projects"
import { UserModel } from "./User"
import { BundleAssetsModel } from "./BundleAssets"
import { LocationsModel } from "./Locations"
import { ProjectBundlesModel } from "./ProjectBundles"
import { api } from "app/services/api"
import { LoanBundle } from "./LoanBundle"
import { LoanAsset } from "./LoanAsset"

/**
 * Model description here for TypeScript hints.
 */
export const MainDbModel = types
  .model("MainDb")
  .props({
    allAssets: types.optional(types.array(AssetModel), []),
    allBundles: types.optional(types.array(BundlesModel), []),
    allProjects: types.optional(types.array(ProjectsModel), []),
    allLoans: types.optional(LoansModel, {}),
    allUsers: types.optional(types.array(UserModel), []),
    loading: types.optional(types.boolean, false),
  })
  .actions(withSetPropAction)
  .views((self) => ({
    get loanedAssets() {
      return self.allAssets.filter((it) => it.loan_status == "1")
    },
    get onlyUsers(){
      return self.allUsers.filter(it=> it.role=='user')
    },
    get availableAssets() {
      const availableassets = self.allAssets.filter((it) => it.in_maintainance == "0")
      return availableassets.filter((it) => it.loan_status == "0")
    },
    get availableBundles() {
      return self.allBundles.filter((it) => it.loan_status == "0")
    },
    get availableProjects() {
      return self.allProjects.filter((it) => it.loan_status == "0")
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    async fetchAll() {
      const resp = await api.mainDb.fetchAll()
    },

    async fetchAllAssets() {
      const resp = await api.mainDb.fetchAllAssets()
      if (resp.kind == "ok") {
        const items = resp.data?.sort((a, b) => {
          // Check if loan_status, in_maintainance, and in_bundle are equal
          if (
            a.loan_status === b.loan_status &&
            a.in_maintainance === b.in_maintainance &&
            a.in_bundle === b.in_bundle
          ) {
            return 0 // Maintain original order
          }
          // Sort based on loan_status, in_maintainance, and in_bundle
          // If loan_status is 0 and in_maintainance and in_bundle are 0, move the item to the top
          if (a.loan_status == "0" && a.in_maintainance == "0" && a.in_bundle == "0") {
            return -1
          }
          // If loan_status, in_maintainance, and in_bundle are not 0, maintain original order
          if (b.loan_status == "0" && b.in_maintainance == "0" && b.in_bundle == "0") {
            return 1
          }
          // For other cases, maintain original order
          return 0
        })
        self.setProp("allAssets", items)
      }
    },
    async fetchAllusers() {
      const resp = await api.mainDb.fetchAllUsers()
      if (resp.kind == "ok") {
        self.setProp("allUsers", resp.data)
      }
    },
    async fetchAllData() {
      self.setProp("loading", true)
      await this.fetchAllAssets()
      await this.fetchAllBundles()
      await this.fetchAllProjects()
      await this.fetchLoans()
      await this.fetchAllusers()
      self.setProp("loading", false)
    },
    async fetchAllBundles() {
      const resp = await api.mainDb.fetchAllBundles()
      if (resp.kind == "ok") {
        self.setProp("allBundles", resp.data)
      }
    },

    async fetchAllProjects() {
      const resp = await api.mainDb.fetchAllProjects()
      if (resp.kind == "ok") {
        self.setProp("allProjects", resp.data)
      }
    },
    async fetchLoans() {
      const resp = await api.mainDb.getAllLoans()
      if (resp.kind == "ok") {
        self.setProp("allLoans", resp.data)
      }
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface MainDb extends Instance<typeof MainDbModel> {}
export interface MainDbSnapshotOut extends SnapshotOut<typeof MainDbModel> {}
export interface MainDbSnapshotIn extends SnapshotIn<typeof MainDbModel> {}
export const createMainDbDefaultModel = () => types.optional(MainDbModel, {})
