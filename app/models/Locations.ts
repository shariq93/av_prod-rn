import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"

/**
 * Model description here for TypeScript hints.
 */
export const LocationsModel = types
  .model("Locations")
  .props({
    id: types.optional(types.number, -1),
    address: types.optional(types.string, ""),
    level: types.optional(types.string, ""),
  
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Locations extends Instance<typeof LocationsModel> {}
export interface LocationsSnapshotOut extends SnapshotOut<typeof LocationsModel> {}
export interface LocationsSnapshotIn extends SnapshotIn<typeof LocationsModel> {}
export const createLocationsDefaultModel = () => types.optional(LocationsModel, {})
