import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { Asset, AssetModel } from "./Asset"
import { LoanAsset, LoanAssetModel } from "./LoanAsset"

/**
 * Model description here for TypeScript hints.
 */
const Type = types.enumeration("Type", ["", "SINGLE_ASSET", "SINGLE_BUNDLE", "SINGLE_PROJECT"])
export const LoanReturnModel = types
  .model("LoanReturn")
  .props({
    processType: types.optional(Type, ""), // ,
    loneeId: types.optional(types.string, ""),
    assetId: types.optional(types.string, ""),
    bundleId: types.optional(types.string, ""),
    projectId: types.optional(types.string, ""),

  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setSingleAsset(loneeId, assetId) {
      self.setProp("processType", Type.create("SINGLE_ASSET"))
      self.setProp("loneeId", loneeId + "")
      self.setProp("assetId", assetId + "")
    },
    setSingleBundle(loneeId, bundleId, assets) {
      self.setProp("processType", Type.create("SINGLE_BUNDLE"))
      self.setProp("loneeId", loneeId + "")
      self.setProp("bundleId", bundleId + "")

    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface LoanReturn extends Instance<typeof LoanReturnModel> {}
export interface LoanReturnSnapshotOut extends SnapshotOut<typeof LoanReturnModel> {}
export interface LoanReturnSnapshotIn extends SnapshotIn<typeof LoanReturnModel> {}
export const createLoanReturnDefaultModel = () => types.optional(LoanReturnModel, {})
