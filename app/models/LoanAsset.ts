import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { BrandsModel } from "./Brands"
import { CategoriesModel } from "./Categories"
import { UserModel } from "./User"

/**
 * Model description here for TypeScript hints.
 */
export const LoanAssetModel = types
  .model("LoanAsset")
  .props({
    id: types.optional(types.number, 0),
    name: types.optional(types.string, ""),
    tagid: types.optional(types.string, ""),
    in_maintainance: types.optional(types.string, ""),
    brand: types.optional(BrandsModel, {}),
    loanee: types.optional(UserModel, {}),
    category: types.optional(CategoriesModel, {}),
    loan_date: types.optional(types.string, ""),
    loan_id: types.optional(types.number, 0),
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface LoanAsset extends Instance<typeof LoanAssetModel> {}
export interface LoanAssetSnapshotOut extends SnapshotOut<typeof LoanAssetModel> {}
export interface LoanAssetSnapshotIn extends SnapshotIn<typeof LoanAssetModel> {}
export const createLoanAssetDefaultModel = () => types.optional(LoanAssetModel, {})
