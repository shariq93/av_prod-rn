import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { AssetModel } from "./Asset"

/**
 * Model description here for TypeScript hints.
 */
export const BundlesModel = types
  .model("Bundles")
  .props({
    id: types.optional(types.number, -1),
    name: types.optional(types.string, ""),
    tagid: types.optional(types.string, ""),
    loan_status: types.optional(types.string, "0"),
    company_id: types.optional(types.string, "0"),
    assets:types.optional( types.array(AssetModel), []),
    in_project: types.optional(types.string, "1"),
   
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Bundles extends Instance<typeof BundlesModel> {}
export interface BundlesSnapshotOut extends SnapshotOut<typeof BundlesModel> {}
export interface BundlesSnapshotIn extends SnapshotIn<typeof BundlesModel> {}
export const createBundlesDefaultModel = () => types.optional(BundlesModel, {})
