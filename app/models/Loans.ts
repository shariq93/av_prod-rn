import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { BrandsModel } from "./Brands"
import { UserModel } from "./User"
import { CategoriesModel } from "./Categories"
import { Asset } from "./Asset"
import { LoanAssetModel } from "./LoanAsset"
import { LoanBundleModel } from "./LoanBundle"
import { LoanProjectModel } from "./LoanProject"

/**
 * Model description here for TypeScript hints.
 */




export interface LoanedAsset {
  name: string
  tagid: string
  bundleName?: string
  bundleId?: string
  projectName?: string
  projectId?: string
  loadDate: string
  loneeName: string
  loneeId: string
}
export const LoansModel = types
  .model("Loans")
  .props({
    assets: types.array(LoanAssetModel),
    bundles: types.array(LoanBundleModel),
    projects: types.array(LoanProjectModel),
  })
  .actions(withSetPropAction)
  .views((self) => ({
    get allAssets() {
      let assets: LoanedAsset[] = []

      for (let index = 0; index < self.assets.length; index++) {
        const item = self.assets[index]
        const asset: LoanedAsset = {
          name: "",
          tagid: "",
          loadDate: "",
          loneeName: "",
          loneeId: "",
        }
        asset.name = item.name
        asset.tagid = item.tagid
        asset.loneeName = item.loanee.name
        asset.loadDate = item.loan_date
        asset.loneeId = item.loanee.id + ""

        assets.push(asset)
      }

      for (let index = 0; index < self.bundles.length; index++) {
        const bundles = self.bundles[index]
        for (let index = 0; index < bundles.assets.length; index++) {
          const item = bundles.assets[index]
          const asset: LoanedAsset = {
            name: "",
            tagid: "",
            loadDate: "",
            loneeName: "",
            loneeId: "",
          }
          asset.name = item.name
          asset.tagid = item.tagid
          asset.loneeName = bundles.loanee.name
          asset.loadDate = bundles.loan_date
          asset.loneeId = bundles.loanee.id + ""
          asset.bundleId = bundles.id + ""
          asset.bundleName = bundles.name + ""
          assets.push(asset)
        }
      }

      for (let index = 0; index < self.projects.length; index++) {
        const projects = self.projects[index]
        for (let index = 0; index < projects.bundles.length; index++) {
          const bundles = projects.bundles[index]
          for (let index = 0; index < bundles.assets.length; index++) {
            const item = bundles.assets[index]
            const asset: LoanedAsset = {
              name: "",
              tagid: "",
              loadDate: "",
              loneeName: "",
              loneeId: "",
            }
            asset.name = item.name
            asset.tagid = item.tagid
            asset.loneeName = projects.loanee.name
            asset.loadDate = projects.loan_date
            asset.loneeId = projects.loanee.id + ""
            asset.bundleId = bundles.id + ""
            asset.bundleName = bundles.name + ""
            asset.projectId= projects.id + ""
            asset.projectName = projects.name + ""
            assets.push(asset)
          }
        }
      }

      return assets
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Loans extends Instance<typeof LoansModel> {}
export interface LoansSnapshotOut extends SnapshotOut<typeof LoansModel> {}
export interface LoansSnapshotIn extends SnapshotIn<typeof LoansModel> {}
export const createLoansDefaultModel = () => types.optional(LoansModel, {})
