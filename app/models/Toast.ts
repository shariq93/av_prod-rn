import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { translate } from "../i18n"

/**
 * Model description here for TypeScript hints.
 */
export const ToastModel = types
  .model("Toast")
  .props({
    isVisible: types.optional(types.boolean, false),
    message: types.optional(types.string, ""),
    type: types.optional(types.string, ""),
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    showSuccess(message?: string, icon?: string) {
      self.setProp("message", message ?? "")
      self.setProp("isVisible", true)
      self.setProp("type", "success")
    },
    showError(message?: string) {
      self.setProp("message", message || "Oops! something went wrong.")
      self.setProp("isVisible", true)
      self.setProp("type", "error")
    },
    showToast(message) {
      self.setProp("message", message)
      self.setProp("isVisible", true)
    },
    hideToast() {
      self.setProp("isVisible", false)
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Toast extends Instance<typeof ToastModel> {}
export interface ToastSnapshotOut extends SnapshotOut<typeof ToastModel> {}
export interface ToastSnapshotIn extends SnapshotIn<typeof ToastModel> {}
export const createToastDefaultModel = () => types.optional(ToastModel, {})
