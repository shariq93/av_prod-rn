import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { LoanAssetModel } from "./LoanAsset"
import { UserModel } from "./User"

/**
 * Model description here for TypeScript hints.
 */
export const LoanBundleModel = types
  .model("LoanBundle")
  .props({
    id: types.optional(types.number, 0),
    name: types.optional(types.string, ""),
    tagId: types.optional(types.string, ""),
    tagid: types.optional(types.string, ""),
    loan_id: types.optional(types.number, 0),
    assets: types.optional(types.array(LoanAssetModel), []),
    loanee: types.optional(UserModel, {}),
    loan_date: types.optional(types.string, ""),
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface LoanBundle extends Instance<typeof LoanBundleModel> {}
export interface LoanBundleSnapshotOut extends SnapshotOut<typeof LoanBundleModel> {}
export interface LoanBundleSnapshotIn extends SnapshotIn<typeof LoanBundleModel> {}
export const createLoanBundleDefaultModel = () => types.optional(LoanBundleModel, {})
