import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { AssetModel } from "./Asset"

/**
 * Model description here for TypeScript hints.
 */
export const MaintainanceAssetModel = types
  .model("MaintainanceAsset")
  .props({
    id: types.optional(types.identifierNumber, 0),
    asset_id: types.optional(types.string, ''),
    user_id: types.optional(types.string, ''),
    
    asset: types.maybeNull(AssetModel), // Reference to the Asset model
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface MaintainanceAsset extends Instance<typeof MaintainanceAssetModel> {}
export interface MaintainanceAssetSnapshotOut extends SnapshotOut<typeof MaintainanceAssetModel> {}
export interface MaintainanceAssetSnapshotIn extends SnapshotIn<typeof MaintainanceAssetModel> {}
export const createMaintainanceAssetDefaultModel = () => types.optional(MaintainanceAssetModel, {})
