import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { UserModel } from "./User"
import { ToastModel } from "./Toast"
import { AuthModel } from "./Auth"
import { MainDbModel } from "./MainDb"
import { MaintainanceAssetsModel } from "./MaintainanceAssets"
import { LoanReturnModel } from "./LoanReturn"
import { TagDataHolderModel } from "./TagDataHolder"
import { ProjectTagHolderModel } from "./ProjectTagHolder"



/**
 * A RootStore model.
 */
export const RootStoreModel = types.model("RootStore").props({

  auth: types.optional(AuthModel, {
    token: "",
    loading: false,
    isLogin: false,
    user: {},
  }),
 
  mainDb: types.optional(MainDbModel, {
  
  }),
 
  maintainance: types.optional(MaintainanceAssetsModel, {
  
  }),

  loanReturn: types.optional(LoanReturnModel,{}),
  tagHolder: types.optional(TagDataHolderModel,{}),
  projectTagHolder:types.optional(ProjectTagHolderModel,{}),
 
 
 


  toast: types.optional(ToastModel, {
    message: "",
    isVisible: false,
  }),
})

/**
 * The RootStore instance.
 */
export interface RootStore extends Instance<typeof RootStoreModel> {}
/**
 * The data of a RootStore.
 */
export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
