import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"

/**
 * Model description here for TypeScript hints.
 */
export const BundleAssetsModel = types
  .model("BundleAssets")
  .props({
    id: types.optional(types.number, -1),
    asset_id: types.optional(types.string, '0'),
    bundle_id: types.optional(types.string, '0'),
   
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface BundleAssets extends Instance<typeof BundleAssetsModel> {}
export interface BundleAssetsSnapshotOut extends SnapshotOut<typeof BundleAssetsModel> {}
export interface BundleAssetsSnapshotIn extends SnapshotIn<typeof BundleAssetsModel> {}
export const createBundleAssetsDefaultModel = () => types.optional(BundleAssetsModel, {})
