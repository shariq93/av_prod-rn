import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { BrandsModel } from "./Brands"
import { CategoriesModel } from "./Categories"

/**
 * Model description here for TypeScript hints.
 */
export const AssetModel = types
  .model("Asset")
  .props({
    id: types.optional(types.number, 0),
    name: types.optional(types.string, ""),
    tagid: types.optional(types.string, ""),
    image: types.maybeNull(types.string),
    brand_id: types.optional(types.string, ""),
    category_id: types.optional(types.string, ""),
    company_id: types.optional(types.string, ""),
    serial_no: types.optional(types.string, ""),
    model_no: types.optional(types.string, ""),
    remarks: types.maybeNull(types.string),
    price: types.optional(types.string, ""),
    company_name: types.optional(types.string, ""),
    purchase_date: types.maybeNull(types.string),
    expiry_date: types.maybeNull(types.string),
    details: types.maybeNull(types.string),
    loan_status: types.optional(types.string, ""),
    in_bundle: types.optional(types.string, ""),
    in_project: types.optional(types.string, ""),
    in_maintainance: types.optional(types.string, ""),
    location_id: types.optional(types.string, ""),
    shelve: types.optional(types.string, ""),
    deleted_at: types.maybeNull(types.string),
    created_at: types.optional(types.string, ""),
    updated_at: types.optional(types.string, ""),
    brand: types.optional(BrandsModel, {}),
    category: types.optional(CategoriesModel, {})
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Asset extends Instance<typeof AssetModel> {}
export interface AssetSnapshotOut extends SnapshotOut<typeof AssetModel> {}
export interface AssetSnapshotIn extends SnapshotIn<typeof AssetModel> {}
export const createAssetDefaultModel = () => types.optional(AssetModel, {})
