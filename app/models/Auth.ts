import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { User, UserModel } from "./User"
import { api } from "app/services/api"
import { saveString, saveUser, setIsLogin } from "app/utils/storage"

/**
 * Model description here for TypeScript hints.
 */
export const AuthModel = types
  .model("Auth")
  .props({
    user: types.optional(UserModel, {}),
    token: types.optional(types.string, ""),
    loading: types.optional(types.boolean, false),
    isLogin: types.optional(types.boolean, false),
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setUser(user: User) {
      self.setProp("user", user)
      saveUser(user as User)
      self.setProp("isLogin", true)
      setIsLogin("true")
    },
    setToken(token: string) {
      self.setProp("token", token)
      saveString("token", token)
    },
    async loginUser(email: string, password: string, onSucess, onFailure) {
      self.setProp("loading", true)
      const resp = await api.auth.login({ email, password })
      if (resp.kind == "ok") {
        const user = resp.user
        
        const token = resp.token

        this.setUser(user as User)
        this.setToken(token)
        

        onSucess()
      } else {
        onFailure(resp?.message)
      }
      self.setProp("loading", false)
    },
    async refresh() {
   
      const resp = await api.auth.refreshToken()
      if (resp.kind == "ok") {
        const user = resp.user
        const token = resp.token

        // this.setUser(user as User)
        this.setToken(token)
      }
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Auth extends Instance<typeof AuthModel> {}
export interface AuthSnapshotOut extends SnapshotOut<typeof AuthModel> {}
export interface AuthSnapshotIn extends SnapshotIn<typeof AuthModel> {}
export const createAuthDefaultModel = () => types.optional(AuthModel, {})
