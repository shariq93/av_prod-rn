import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"

/**
 * Model description here for TypeScript hints.
 */
export const BrandsModel = types
  .model("Brands")
  .props({
    id: types.optional(types.number, 0),
    name: types.optional(types.string, ""),
    image: types.maybeNull(types.string),
    company_id: types.optional(types.string, ""),

  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface Brands extends Instance<typeof BrandsModel> {}
export interface BrandsSnapshotOut extends SnapshotOut<typeof BrandsModel> {}
export interface BrandsSnapshotIn extends SnapshotIn<typeof BrandsModel> {}
export const createBrandsDefaultModel = () => types.optional(BrandsModel, {})
