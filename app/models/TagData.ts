import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"

/**
 * Model description here for TypeScript hints.
 */
export const TagDataModel = types
  .model("TagData")
  .props({
    id: types.optional(types.string, ""),
    tagId: types.optional(types.string, ""),
    type: types.optional(types.string, ""),
    name: types.optional(types.string, ""),
    projectId: types.optional(types.string, ""),
    bundleId: types.optional(types.string, ""),
    maintainance:types.optional(types.boolean, false),
    tags: types.optional(types.array(types.late((): any => TagDataModel)), []),
    // only of projects scanning flow
    scannable: types.optional(types.boolean, false),
  })
  .actions(withSetPropAction)
  .views((self) => ({
    getAllTagsIds(): string[] {
      let allTags: string[] = [self.tagId] // Include the current tagId
      // Recursively collect tag IDs from nested tags
      self.tags.forEach((tag) => {
        allTags = allTags.concat(tag.getAllTagsIds())
      })
      return allTags
    },
    getAllTags(): any[] {
      let allTags: any[] = [{ tagId: self.tagId, name: self.name, id: self.id, type: self.type }] // Include the current tagId
      // Recursively collect tag IDs from nested tags
      self.tags.forEach((tag) => {
        allTags = allTags.concat(tag.getAllTags())
      })
      return allTags
    },
    getAllTagsForProjects(): any[] {
      let allTags: any[] = [{ tagId: self.tagId, name: self.name, id: self.id, type: self.type }] // Include the current tagId
      // Recursively collect tag IDs from nested tags
      self.tags.forEach((tag) => {
        if (tag.scannable) allTags = allTags.concat(tag.getAllTagsForProjects())
      })
      return allTags
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setScannable(isScannable: boolean) {
      self.setProp("scannable", isScannable)
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface TagData extends Instance<typeof TagDataModel> {}
export interface TagDataSnapshotOut extends SnapshotOut<typeof TagDataModel> {}
export interface TagDataSnapshotIn extends SnapshotIn<typeof TagDataModel> {}
export const createTagDataDefaultModel = () => types.optional(TagDataModel, {})
