import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"

/**
 * Model description here for TypeScript hints.
 */
export const ProjectBundlesModel = types
  .model("ProjectBundles")
  .props({
    id:types.optional(types.number,-1),
    bundle_id: types.optional(types.string,""),
    project_id: types.optional(types.string,""),
   
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface ProjectBundles extends Instance<typeof ProjectBundlesModel> {}
export interface ProjectBundlesSnapshotOut extends SnapshotOut<typeof ProjectBundlesModel> {}
export interface ProjectBundlesSnapshotIn extends SnapshotIn<typeof ProjectBundlesModel> {}
export const createProjectBundlesDefaultModel = () => types.optional(ProjectBundlesModel, {})
