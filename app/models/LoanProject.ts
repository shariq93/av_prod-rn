import { Instance, SnapshotIn, SnapshotOut, types } from "mobx-state-tree"
import { withSetPropAction } from "./helpers/withSetPropAction"
import { LoanBundleModel } from "./LoanBundle"
import { UserModel } from "./User"
import { LoanAssetModel } from "./LoanAsset"

/**
 * Model description here for TypeScript hints.
 */
export const LoanProjectModel = types
  .model("LoanProject")
  .props({
    id: types.optional(types.number, 0),
    name: types.optional(types.string, ""),
    tagid: types.optional(types.string, ""),
    bundles: types.array(LoanBundleModel),
    remarks: types.optional(types.string, ""),
    assets:types.optional( types.array(LoanAssetModel), []),
    loan_id: types.optional(types.number, 0),
    loanee: types.optional(UserModel, {}),
    loan_date: types.optional(types.string, ""),
  })
  .actions(withSetPropAction)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

export interface LoanProject extends Instance<typeof LoanProjectModel> {}
export interface LoanProjectSnapshotOut extends SnapshotOut<typeof LoanProjectModel> {}
export interface LoanProjectSnapshotIn extends SnapshotIn<typeof LoanProjectModel> {}
export const createLoanProjectDefaultModel = () => types.optional(LoanProjectModel, {})
