import { colors } from "./colors"
import { mesures } from "./mesures"

/**
  Use these spacings for margins/paddings and other whitespace throughout your app.
 */
export const ShadowStyle = {
  shadowColor: colors.palette.neutral300,
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowRadius: mesures.borderRadius,
  shadowOpacity: .3,
  elevation: 4
  
} as const

export type ShadowStyle = keyof typeof ShadowStyle
