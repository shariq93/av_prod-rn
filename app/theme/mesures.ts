/**
  Use these spacings for margins/paddings and other whitespace throughout your app.
 */
export const mesures = {
  borderRadius:10,
  borderWidthNormal:1,
  borderWidthBolder:2,
  
} as const

export type mesures = keyof typeof mesures
