import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { View, ViewStyle, Dimensions, Pressable, DeviceEventEmitter, Image, TouchableOpacity, FlatList, ActivityIndicator } from "react-native"
import { AppStackScreenProps, navigate } from "app/navigators"
import { AssetListItem, Button, LoanedAssetsListItem, LoanedBundleListItem, LoanedProjectListItem, Screen, SquareButton, Text } from "app/components"
import { CommonActions, useNavigation } from "@react-navigation/native"
import { TagData, useStores } from "app/models"
import { useHeader } from "app/utils/useHeader"
import { colors, spacing, typography } from "app/theme"
import { MaterialIcons } from '@expo/vector-icons';
import { Dialogs } from "app/components/Dialogs"
import { logout } from "app/utils/storage"
const { width } = Dimensions.get('window')
import { Feather } from '@expo/vector-icons';
import { NativeModules } from 'react-native';


interface HomeScreenProps extends AppStackScreenProps<"Home"> { }

export const HomeScreen: FC<HomeScreenProps> = observer(function HomeScreen() {
  // Pull in one of our MST stores
  const { mainDb, auth, loanReturn, tagHolder } = useStores()
  const { user } = auth
  const { allLoans, loading } = mainDb
  const { allAssets, assets, bundles, projects } = allLoans

  useEffect(() => {
    mainDb.fetchAllData()

  }, [])
  // Pull in navigation via hook
  const navigation = useNavigation()
  const [showLogoutDialog, setShowLogoutDialog] = useState(false)
  const [selectedIndex, setSelectedIndex] = useState(0)
  useHeader({
    title: 'Welcome', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    RightActionComponent: <View style={{ marginRight: 10 }}>
      <Pressable
        onPress={() => {
          setShowLogoutDialog(true)
        }}
      >
        <MaterialIcons name="logout" size={24} color="white" />
      </Pressable>
    </View>
  })


  return (
    <Screen style={$root} preset="scroll">
      <Dialogs show={showLogoutDialog} showIcon
        title="Logout"
        type={"info"} discription={"Are you sure want to logout?"}

        onPossitivePress={async () => {
          await logout()
          setShowLogoutDialog(false)
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [{ name: "Login" }],
            }),
          )
        }}
        onNegativePress={function (): void {
          setShowLogoutDialog(false)
        }}
        negativeText="No"
        possitiveText={"Yes"}>

      </Dialogs>
      <View style={$box}>

        <View style={{ flexDirection: 'row', }}>
          <Image source={{ uri: "https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png" }}
            style={{ width: 60, height: 60, borderRadius: 60 / 2 }}
          />
          <View style={{ marginLeft: 10 }}>

            <Text size="lg" style={{ color: '#fff', fontFamily: typography.primary.medium }}>{user.name}</Text>
            <Text style={{ color: '#fff', fontFamily: typography.primary.medium }}>{user.role}</Text>
          </View>
        </View>

        {/* <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 5 }}>
          <TouchableOpacity onPress={() => {
            
          }}>
            <View style={{ flexDirection: 'row' }}>
              <Feather name="download-cloud" size={24} color="white" />
              <Text style={{ color: '#fff', marginLeft: 10 }}>Sync Down</Text>
            </View>
          </TouchableOpacity>

          <View style={{ width: 20 }} />
          <TouchableOpacity>
            <View style={{ flexDirection: 'row' }}>
              <Feather name="upload-cloud" size={24} color="white" />
              <Text style={{ color: '#fff', marginLeft: 10 }}>Sync Up</Text>
            </View>
          </TouchableOpacity>
        </View> */}


      </View>

      <View style={{ padding: spacing.sm }}>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <SquareButton onPress={() => {
            navigation.navigate('LoanOut')
          }} icon='handshake-o' lable='Loan Out' />
          <SquareButton onPress={() => {
            navigation.navigate('MaintainanceAssets')
          }} icon='tools' lable='Maintenance' />
          <SquareButton onPress={() => {
            navigation.navigate('CheckStatus')

          }} icon='text-box-check-outline' lable='Check Status' />

        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 5, }}>
          <TouchableOpacity
            onPress={() => {
              setSelectedIndex(0)
            }}
            style={{ ...$tabs, backgroundColor: selectedIndex == 0 ? colors.palette.primary600 : colors.separator }}>
            <Text style={{ color: selectedIndex == 0 ? '#fff' : colors.palette.primary600 }}>Assets </Text>
          </TouchableOpacity>
          <View style={{ width: 5 }} />
          <TouchableOpacity
            onPress={() => {
              setSelectedIndex(1)
            }}
            style={{ ...$tabs, backgroundColor: selectedIndex == 1 ? colors.palette.primary600 : colors.separator }}>
            <Text style={{ color: selectedIndex == 1 ? '#fff' : colors.palette.primary600 }}>Bundle</Text>
          </TouchableOpacity>
          <View style={{ width: 5 }} />
          <TouchableOpacity
            onPress={() => {
              setSelectedIndex(2)
            }}
            style={{ ...$tabs, backgroundColor: selectedIndex == 2 ? colors.palette.primary600 : colors.separator }}>
            <Text style={{ color: selectedIndex == 2 ? '#fff' : colors.palette.primary600 }}>Projects</Text>
          </TouchableOpacity>
          <View style={{ width: 5 }} />
          {/* <TouchableOpacity
            onPress={() => {
              setSelectedIndex(3)
            }}
            style={{ ...$tabs, backgroundColor: selectedIndex == 3 ? colors.palette.primary600 : colors.separator }}>
            <Text style={{ color: selectedIndex == 3 ? '#fff' : colors.palette.primary600 }}>Projects</Text>
          </TouchableOpacity> */}
        </View>

        <Text style={{ fontFamily: typography.primary.semiBold }}>Loaned Items</Text>
        {/* {selectedIndex == 0 && allAssets.map((item) => {
          return <View>
            <AssetListItem item={item} />
            <View style={{ height: 8 }} />
          </View>
        })} */}

        {loading && <ActivityIndicator color={colors.palette.primary600}/>}
        {selectedIndex == 0 && <FlatList
          data={assets}
          ListEmptyComponent={() => <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <Text>No Asset available to return</Text>
          </View>}
          renderItem={({ item }) => <LoanedAssetsListItem

            item={item}

            onPress={() => {

              const emptyTags: TagData[] = []
              const tempAsset = {
                tagId: (item.tagid),
                name: item.name,
                type: 'ASSET',
                id: (item.id + ""),
                projectId: "-1",
                bundleId: "-1",
                tags: emptyTags
              }
              tagHolder.setBundleId(-1)
              tagHolder.setLoanee(item.loanee)
              tagHolder.addTagData(tempAsset)
              tagHolder.setShowSummaryMessage(false)
              // loanReturn.setSingleBundle(item.loanee.id, item.id, item.assets)
              // navigation.navigate('ScannerLoanReturn',
              //   {
              //     bundle: { name: item.name, tagId: (item.tagid || item.tagId), },
              //     bundleAssets: item.assets,
              //   })

              navigation.navigate('AssetScanner')
            }}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />}


        {selectedIndex == 1 && <FlatList
          data={bundles}
          ListEmptyComponent={() => <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <Text>No bundle available to return</Text>
          </View>}
          renderItem={({ item }) => <LoanedBundleListItem
            onPress={() => {

              const childTags = item.assets.map((it) => {
                const emptyTags: TagData[] = []
                let temp = {
                  tagId: it.tagid,
                  id: (it.id + ''),
                  name: it.name,
                  type: 'ASSET',
                  projectId: "-1",
                  bundleId: (item.id + ''),
                  tags: emptyTags
                }
                return temp
              })

              const tempBundle = {
                tagId: (item.tagid || item.tagId),
                name: item.name,
                type: 'BUNDLE',
                id: (item.id + ""),
                projectId: "-1",
                bundleId: "-1",
                tags: childTags
              }
              tagHolder.setBundleId(item.id)
              tagHolder.setLoanee(item.loanee)
              tagHolder.addTagData(tempBundle)
              // loanReturn.setSingleBundle(item.loanee.id, item.id, item.assets)
              // navigation.navigate('ScannerLoanReturn',
              //   {
              //     bundle: { name: item.name, tagId: (item.tagid || item.tagId), },
              //     bundleAssets: item.assets,
              //   })

              navigation.navigate('BundleScanner')
            }}
            item={item}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />}
        {selectedIndex == 2 && <FlatList
          data={projects}
          ListEmptyComponent={() => <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <Text>No project available to return</Text>
          </View>}
          renderItem={({ item }) => <LoanedProjectListItem
            item={item}
            onPress={() => {
              const bundles = item.bundles.map((it) => {


                const bundleAssets = it.assets.map((asset) => {

                  const emptyTags: TagData[] = []
                  let temp = {
                    tagId: asset.tagid,
                    id: (asset.id + ''),
                    name: asset.name,
                    type: 'ASSET',
                    maintainance:(asset.in_maintainance=='1'),
                    projectId: (item.id + ''),
                    bundleId: (it.id + ''),
                    tags: emptyTags
                  }
                  return temp
                })

                let temp = {
                  tagId: it.tagid,
                  id: (it.id + ''),
                  name: it.name,
                  type: 'BUNDLE',
                  projectId: (item.id + ''),
                  bundleId: (it.id + ''),
                  tags: bundleAssets
                }
                return temp
              })
              const assets = item.assets.map((it) => {
                const emptyTags: TagData[] = []
                let temp = {
                  tagId: it.tagid,
                  id: (it.id + ''),
                  name: it.name,
                  type: 'ASSET',
                  maintainance:(it.in_maintainance=='1'),
                  projectId: "-1",
                  bundleId: (item.id + ''),
                  tags: emptyTags
                }
                return temp
              })
              const childTags = [...assets, ...bundles]
              const tempBundle = {
                tagId: (item.name),
                name: item.name,
                type: 'PROJECT',
                id: (item.id + ""),
                projectId: "-1",
                bundleId: "-1",
                tags: childTags
              }
              tagHolder.setProjectId(item.id)
              tagHolder.setLoanee(item.loanee)
              tagHolder.addTagData(tempBundle)
              tagHolder.setRemarks(item.remarks)


              navigation.navigate('ProjectScanner')
              // loanReturn.setSingleBundle(item.loanee.id, item.id, item.assets)
              // navigation.navigate('ScannerLoanReturn',
              //   {
              //     bundle: { name: item.name, tagId: (item.tagid || item.tagId), },
              //     bundleAssets: item.assets,
              //   })

              // navigation.navigate('ProjectDetails', { project: item })
            }}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />}
      </View>
    </Screen>
  )
})

const $root: ViewStyle = {
  flex: 1,

}
const $box: ViewStyle = {
  backgroundColor: colors.palette.primary600,
  height: 75, width,
  paddingLeft: spacing.sm

}
const $tabs: ViewStyle = {
  flex: 1,
  backgroundColor: colors.palette.primary600,
  paddingVertical: spacing.sm,
  justifyContent: 'center', alignItems: 'center',
  borderRadius: 7

}
