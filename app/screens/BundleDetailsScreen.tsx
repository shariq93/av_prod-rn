import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, TouchableOpacity, View, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { AssetListItem, Screen, Text } from "app/components"
import { useNavigation, useRoute } from "@react-navigation/native"
import { Asset, LoanedAsset, useStores } from "app/models"
import { useHeader } from "app/utils/useHeader"
import { colors, spacing, typography } from "app/theme"
import { MaterialCommunityIcons } from '@expo/vector-icons';
interface BundleDetailsScreenProps extends AppStackScreenProps<"BundleDetails"> { }

export const BundleDetailsScreen: FC<BundleDetailsScreenProps> = observer(function BundleDetailsScreen() {
  // Pull in one of our MST stores
  const { } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()
  useHeader({
    title: 'Bundle Details', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })
  const [assets, setAssets] = useState<LoanedAsset[]>([])
  const route = useRoute()
  const params = route?.params
  useEffect(() => {
    const {assets} = params?.bundle
    console.tron.log(assets)
    if (assets) {

      
      setAssets(assets)
    }

  }, [])

  return (
    <Screen style={$root} preset="scroll">
      <FlatList
       ListHeaderComponent={<TouchableOpacity onPress={() => {
        const items = assets.map(item => { return { name: item.name, tagId: item.tagid } })
        navigation.navigate('Scanner', { items: items,type:'ASSETS' })
      }}>
        {<View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 10 }}>
          <MaterialCommunityIcons name="cellphone-nfc" size={24} color={colors.palette.primary500} />
          <Text style={{ fontFamily: typography.primary.semiBold, color: colors.palette.primary500 }}>{"Scan Assets"}</Text>
        </View>}
      </TouchableOpacity>
      }
        data={assets}
        renderItem={({ item }) => <AssetListItem
          item={item}
        />}
        ListFooterComponent={()=><View style={{height:50}}/>}
        ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
      />
    </Screen>
  )
})

const $root: ViewStyle = {
  flex: 1,
  padding:spacing.sm
}
