import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { DeviceEventEmitter, FlatList, NativeModules, View, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { Button, ScannableItemForLoan, Screen, Text } from "app/components"
import { useNavigation, useRoute } from "@react-navigation/native"
import { TagData, User, useStores } from "app/models"
import { colors, spacing } from "app/theme"
import { useHeader } from "app/utils/useHeader"
import { loadString } from "app/utils/storage"
import { DateTimePickerField } from "app/components/DateTimePickerField"
import moment from "moment"
import { ListModal } from "app/components/ListModal"
import { Dialogs, useDialog } from "app/components/Dialogs"
const { RFIDReaderModule } = NativeModules;

interface BundleScannerScreenProps extends AppStackScreenProps<"BundleScanner"> { }

export const BundleScannerScreen: FC<BundleScannerScreenProps> = observer(function BundleScannerScreen() {
  // Pull in one of our MST stores
  const { tagHolder, mainDb, toast } = useStores()
  const { onlyUsers:allUsers } = mainDb
  const { tagData, bundleId, getAllTags, loanee } = tagHolder
  // Pull in navigation via hook
  useEffect(() => {

    return () => {
      tagHolder.clearTagDataArray()
    }
  }, [

  ])
  const navigation = useNavigation()
  useHeader({
    title: 'Bundle ', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })



  const [scannedTags, setScannedTags] = useState([])
  const [isScanning, setIsScanning] = useState(false)

  const [selectedUser, setSelectedUser] = useState<User>(undefined)
  const [expectedReturnDate, setExpectedReturnDate] = useState(moment().format('YYYY-MM-DD'))

  const [showModal, setShowModal] = useState(false)
  const [showProcessDialog, setShowProcessDialog] = useState(false)
  const { isVisible, meta, setMeta, show, hide } = useDialog()
  const route = useRoute()
  const {params} = route


  function pingScannerForTags() {
    if (RFIDReaderModule) {
      RFIDReaderModule.getScannedTags((tags) => {
        processScannedTags(tags);
      });
    }

  }

  const returnBundle = async (bundleId, user_id) => {
    const token = await loadString('token')
    const myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("bundle_id", bundleId)
    formdata.append("user_id", user_id)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/return/bundle", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == 'success') {
          mainDb.fetchAllData()
          toast.showSuccess('Bundle returned successfully!')
          navigation.goBack()
        }
      })
      .catch((error) => console.error(error));
  }



  const loanOut = async () => {
    const token = await loadString('token')
    const myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("bundle_id", bundleId)
    formdata.append("user_id", selectedUser.id + '')
    // formdata.append("tagid", JSON.stringify(scannedTags))

    formdata.append("expected_return_date", expectedReturnDate)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/loan/bundle", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == 'success') {
          mainDb.fetchAllData()
          toast.showSuccess('Bundle loaned out successfully!')
          navigation.goBack()
        }
      })
      .catch((error) => console.error(error));
  }

  const recievedTags = React.useRef([]).current
  const [tempCount, setTempCount] = useState(0)
  if (RFIDReaderModule) {
    DeviceEventEmitter.addListener('onTagFound', (event) => {
      if (event?.tag) {
        if (!recievedTags.includes(event?.tag))
          recievedTags.push(event?.tag)
        setTempCount(recievedTags.length)
      }

    })
  }
  useEffect(() => {
    processScannedTags()
  }, [tempCount])
  useEffect(() => {

    if (RFIDReaderModule) RFIDReaderModule.initDevice()
    return () => {
      if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
    }
  }, [])


  const processScannedTags = () => {



    setScannedTags([...recievedTags])
  }




  function findMatchingTags(totalTags, scannedTags): any[] {
    const totalTagsSet = new Set(totalTags);
    const matchingTags = scannedTags.filter(tag => totalTagsSet.has(tag));
    return matchingTags;
  }

  const prepareForSummary = () => {
    const onlyScannedAssets = getAllTags().filter(it => scannedTags.includes(it.tagId) && it.type == 'ASSET');

    tagHolder.setSelectedTags(onlyScannedAssets)

    const childTags = onlyScannedAssets.map((it) => {
      const emptyTags: TagData[] = []
      let temp = {
        tagId: (it.tagid || it.tagId),
        id: (it.id + ''),
        name: it.name,
        type: 'ASSET',
        projectId: tagHolder.projectId,
        bundleId: tagHolder.bundleId,
        tags: emptyTags
      }
      return temp
    })


    tagHolder.setBundleId(tagHolder.projectId)
    tagHolder.setLoanee(tagHolder.forLoanOut ? selectedUser : tagHolder.loanee)
    tagHolder.setSelectedTags(childTags)
    tagHolder.setReturnData(expectedReturnDate)
    navigation.navigate('AssetsLoanSummary')
  }

  const matchedTags = findMatchingTags(tagHolder.getAllTagsIds(), scannedTags)
  const totalTags = tagHolder.getAllTagsIds()
  let bundleFound = false
  if (tagData.length > 0) {
    const mainBundle = tagData[0]
    if (matchedTags.includes(mainBundle.tagId)) {
      bundleFound = true

    }

  }
  return (
    <View style={$root} >
      <ListModal show={showModal}
        data={allUsers}
        title="Select User"
        titleKeys={["name",]}
        subTitleKeys={['email']}
        onItemSelected={function (item: User): void {
          setSelectedUser(item)
          setShowModal(false)
          if (matchedTags.length < totalTags.length) {
            setTimeout(() => setShowProcessDialog(true), 100);
          } else {
            setTimeout(() => show(), 100);
          }
          // setTimeout(() => show(), 100);
        }} onDismiss={function (): void {
          setShowModal(false)
        }}
      />
      <Dialogs
        show={isVisible}
        type={'warning'}
        showIcon
        title={"Confirm Loan Out"}
        discription={"Are you sure want to loan out selected items to " + selectedUser?.name + "?"}
        onPossitivePress={async () => {

          loanOut();
          hide()
        }}
        onNegativePress={function (): void {
          hide()
        }}

        possitiveText={"Yes"}
        negativeText={"No"}
      ></Dialogs>

      <Dialogs
        show={showProcessDialog}
        type={'warning'}
        showIcon
        title={"Partial Scan"}
        discription={"All tags are not scanned do you want to continue? "}
        onPossitivePress={async () => {

          // loanOut();
          setShowProcessDialog(false)

          prepareForSummary()

        }}
        onNegativePress={function (): void {
          setShowProcessDialog(false)
        }}

        possitiveText={"Yes"}
        negativeText={"No"}
      ></Dialogs>
      <Text>Scanned: {scannedTags.length}    Matched: {matchedTags.length}</Text>
      <FlatList
        data={tagData}
        ListFooterComponent={<View style={{ height: 100 }} />}

        renderItem={({ item }) => {
          return <ScannableItemForLoan
            item={item}
            childs={item.tags}
            scannedTags={scannedTags}
            showChilds={item.tags.length > 0 && scannedTags.includes(item.tagId)}
            scanned={scannedTags.includes(item.tagId)}
          />
        }}

        ListEmptyComponent={() => <View>
          <Text size="xs">No data available!</Text>
        </View>}
      />
      <View style={{ position: 'absolute', bottom: 30, width: '100%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around' }}>
        {!isScanning && <Button
          style={{ width: '80%' }}
          text={isScanning ? 'Stop Scanning' : "Start Scanning"}
          onPress={() => {
            if (!isScanning) {
              if (RFIDReaderModule) {

                RFIDReaderModule.onScanPressed()
              }
            } else {
              if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
            }
            setIsScanning(!isScanning)
          }}
        />}
        {isScanning && !tagHolder.forLoanOut &&!params?.forCheck && <Button
          disabled={matchedTags.length < 1}
          style={{ width: '80%', backgroundColor: matchedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400 }}
          textStyle={{ color: '#fff' }}
          text={'Return'}
          onPress={() => {
            if (matchedTags.length < totalTags.length) {
              alert('Please scan All Tags')
            } else {
              returnBundle(bundleId, loanee.id + '')
            }
          }}
        />}

        {isScanning && params?.forCheck && <Button

          style={{ width: '80%', backgroundColor: colors.palette.primary600 }}
          textStyle={{ color: '#fff' }}
          text={'Go Back'}
          onPress={() => {
            // if (matchedTags.length < totalTags.length) {
            navigation.goBack()
            // } else {
            //   returnProject(projectId, loaneId)
            // }
            // returnProject(projectId, loanee.id)
          }}
        />}

        {isScanning && tagHolder.forLoanOut && <View style={{ flexDirection: 'row', flex: 1, }}>
          <DateTimePickerField
            lable="Delivery Date"
            // style={{width:20}}
            containerStyle={{ width: 160 }}
            placeholderText="Select date"
            defaultDate={expectedReturnDate}
            onDateTimeSelect={function (date: string): void {
              setExpectedReturnDate(date)
            }}
          />
          <Button
            disabled={(matchedTags.length < 1 || !bundleFound)}
            textStyle={{ color: colors.palette.neutral100, }}
            style={{ backgroundColor: (matchedTags.length < 1 || !bundleFound) ? colors.palette.neutral400 : colors.palette.primary600, borderRadius: 7, marginTop: spacing.md, flex: 1 }}
            onPress={() => {
              setShowModal(true)
            }}
            text="Assign User" /></View>}
      </View>
    </View>
  )
})

const $root: ViewStyle = {
  flex: 1,
  padding: spacing.sm,
  backgroundColor: colors.background
}
