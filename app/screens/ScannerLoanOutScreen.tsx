import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { DeviceEventEmitter, FlatList, NativeModules, TextStyle, View, ViewStyle, useWindowDimensions } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { Button, ScannableItemForLoan, Screen, Text } from "app/components"
import { useNavigation, useRoute } from "@react-navigation/native"
import { User, useStores } from "app/models"
import { useHeader } from "app/utils/useHeader"
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { colors, mesures, spacing, typography } from "app/theme"
import { ta } from "date-fns/locale"
import { color } from "react-native-reanimated"
import { loadString } from "app/utils/storage"
import moment from "moment"
import { DateTimePickerField } from "app/components/DateTimePickerField"
import { ListModal } from "app/components/ListModal"
import { Dialogs, useDialog } from "app/components/Dialogs"
const { RFIDReaderModule } = NativeModules;

interface ScannerLoanOutScreenProps extends AppStackScreenProps<"ScannerLoanOut"> { }

export const ScannerLoanOutScreen: FC<ScannerLoanOutScreenProps> = observer(function ScannerLoanOutScreen() {
  // Pull in one of our MST stores
  const { loanReturn, mainDb } = useStores()
  const { allUsers } = mainDb
  const [scannedTags, setScannedTags] = useState([])
  const [matchedTags, setMatchedTags] = useState([])
  const [bundleAssets, setBundleAssets] = useState([])
  const [selectedUser, setSelectedUser] = useState<User>(undefined)
  const [expectedReturnDate, setExpectedReturnDate] = useState(moment().format('YYYY-MM-DD'))

  const [isScanning, setIsScanning] = useState(false)
  const recievedTags = React.useRef([]).current

  // Pull in navigation via hook
  const navigation = useNavigation()
  const { height } = useWindowDimensions()
  useHeader({
    title: 'Loan Out', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })
  const route = useRoute();
  const params = route?.params;

  const [items, setItems] = useState([]);
  const [singleAsset, setSingleAsset] = useState<{ name, tagId }>(params?.asset)
  const [singleBundle, setSingleBundle] = useState<{ name, tagId }>(params?.bundle)
  const [showModal, setShowModal] = useState(false)

  const { isVisible, meta, setMeta, show, hide } = useDialog()
  useEffect(() => {
    if (loanReturn.processType == 'SINGLE_ASSET') {
      setSingleAsset(params?.asset)
      const { name, tagId, } = params?.asset
      setItems([{ name, tagId }])
    }
    if (loanReturn.processType == 'SINGLE_BUNDLE') {
      setSingleBundle(params?.bundle)
      const { name, tagId, } = params?.bundle
      setItems([{ name, tagId }])
      setBundleAssets(params?.bundleAssets)
      console.log(JSON.stringify(params?.bundleAssets));

    }


  }, [params])



  useEffect(() => {
    setTimeout(() => {
      if (isScanning)
        setScannedTags(['E28068940000400B1FD5F512',])
      // setTimeout(() => {
      //   setScannedTags(['111239440944', 'E28068940000400B1FD5F522',])

      // }, 1000);
    }, 2000);
  }, [isScanning])

  function pingScannerForTags() {
    if (RFIDReaderModule) {
      RFIDReaderModule.getScannedTags((tags) => {
          processScannedTags(tags);
      });
  }
      
  }
  
  
    useEffect(() => {
  
      if (RFIDReaderModule) RFIDReaderModule.initDevice()
    
      const intId = setInterval(pingScannerForTags, 2000);
      return () => {
       
        clearInterval(intId)
        if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
  
      }
    }, [])

  useEffect(() => {
    // checkforBundle()
    console.log({ type: loanReturn.processType, scannedTags, singleAsset, matchedTags });
    if (loanReturn.processType == 'SINGLE_ASSET') {
      performMatchForSingleAsset(scannedTags)
    }
    if (loanReturn.processType == 'SINGLE_BUNDLE') {
      performMatchForSingleBundle(scannedTags)
    }
  }, [scannedTags])


  const processScannedTags = (tags: any[]) => {
    const tempTags = tags.map(tag => tag?.tag)
    setScannedTags(tempTags)
  }

  const performMatchForSingleAsset = (scannedTags) => {
    if (scannedTags.includes(singleAsset?.tagId)) {
      matchedTags.push(singleAsset?.tagId)
      setMatchedTags([...matchedTags])
    }
  }
  const performMatchForSingleBundle = (scannedTags) => {
    if (scannedTags.includes(singleBundle?.tagId)) {
      matchedTags.push(singleBundle?.tagId)
      setMatchedTags([...matchedTags])
    }
  }



  const loanOut = async () => {
    const token = await loadString('token')
    const myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("asset_id", loanReturn.assetId)
    formdata.append("user_id", selectedUser.id + '')
    formdata.append("tagid", singleAsset.tagId)
    formdata.append("expected_return_date", expectedReturnDate)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/loan/asset", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        alert("Success")
        navigation.goBack()
      })
      .catch((error) => console.error(error));
  }
  return (

    <View style={$root}>
      <ListModal show={showModal}
        data={allUsers}
        title="Select User"
        titleKeys={["name",]}
        subTitleKeys={['email']}
        onItemSelected={function (item: User): void {
          setSelectedUser(item)
          setShowModal(false)
          setTimeout(() => show(), 100);
        }} onDismiss={function (): void {
          setShowModal(false)
        }}
      />
      <Dialogs
        show={isVisible}
        type={'warning'}
        showIcon
        title={"Confirm Loan Out"}
        discription={"Are you sure want to loan out selected items to " + selectedUser?.name + "?"}
        onPossitivePress={async () => {

          loanOut();
          hide()
        }}
        onNegativePress={function (): void {
          hide()
        }}

        possitiveText={"Yes"}
        negativeText={"No"}
      ></Dialogs>
      <FlatList
        data={items}
        ListFooterComponent={<View style={{ height: 100 }} />}

        renderItem={({ item }) => {
          return <ScannableItemForLoan
            item={item}
            childs={bundleAssets}
            scannedTags={scannedTags}
            showChilds={bundleAssets.length > 0 && scannedTags.includes(item.tagId)}
            scanned={scannedTags.includes(item.tagId)}
          />
        }}

        ListEmptyComponent={() => <View>
          <Text size="xs">No data available!</Text>
        </View>}
      />
      <View style={{ position: 'absolute', bottom: 30, width: '100%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around' }}>
        {!isScanning && <Button
          style={{ width: '80%' }}
          text={isScanning ? 'Stop Scanning' : "Start Scanning"}
          onPress={() => {
            if (!isScanning) {
              if (RFIDReaderModule) {

                RFIDReaderModule.onScanPressed()
              }
            } else {
              if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
            }
            setIsScanning(!isScanning)
          }}
        />}

        {isScanning && <View style={{ flexDirection: 'row', flex: 1, }}>
          <DateTimePickerField
            lable="Delivery Date"
            // style={{width:20}}
            containerStyle={{ width: 160 }}
            placeholderText="Select date"
            defaultDate={expectedReturnDate}
            onDateTimeSelect={function (date: string): void {
              setExpectedReturnDate(date)
            }}
          />
          <Button

            textStyle={{ color: colors.palette.neutral100, }}
            style={{ backgroundColor: colors.palette.primary600, borderRadius: 7, marginTop: spacing.md, flex: 1 }}
            onPress={() => {
              setShowModal(true)
            }}
            text="Assign User" /></View>}
        {/* {posting && <View style={{ height: 45 }}>
          <ActivityIndicator color={colors.palette.primary600} />
        </View>} */}
        {/* {isScanning && loanReturn.processType == 'SINGLE_ASSET' && <Button
          disabled={matchedTags.length < 1}
          style={{ width: '80%', backgroundColor: matchedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400 }}
          textStyle={{ color: '#fff' }}
          text={'Return'}
          onPress={() => {
            returnAsset()
          }}
        />}
        
        {isScanning && loanReturn.processType == 'SINGLE_BUNDLE' && <Button
          disabled={matchedTags.length < 1}
          style={{ width: '80%', backgroundColor: matchedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400 }}
          textStyle={{ color: '#fff' }}
          text={'Scan Bundle Assets'}
          onPress={() => {
            if (bundleAssets.length > 0) {
              navigation.goBack()
              navigation.navigate('BundleDetails', { bundle: { assets: bundleAssets } })
            } else {
              alert('There are no assets in the bundle')
            }

          }}
        />} */}
      </View>



    </View>

  )
})

const $root: ViewStyle = {

  flex: 1,
  padding: spacing.sm,
  backgroundColor: colors.background
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
const $title: TextStyle = {
  fontFamily: typography.primary.medium,
  color: colors.palette.primary600
}
const $card: ViewStyle = {
  padding: spacing.xs,
  backgroundColor: colors.palette.neutral100,
  marginTop: spacing.sm,
  borderRadius: mesures.borderRadius,

}
const $listTitle: TextStyle = {
  fontFamily: typography.primary.semiBold,
  fontSize: 12,
  color: colors.palette.primary500,
}

const $generalText: TextStyle = {
  fontFamily: typography.primary.light,
  fontSize: 13,
  color: colors.palette.neutral900,
}