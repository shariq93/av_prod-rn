import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { ActivityIndicator, FlatList, View, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { Button, MaintanceAssetListItem, Screen, Text } from "app/components"
import { useNavigation } from "@react-navigation/native"
import { colors, spacing } from "app/theme"
import { useHeader } from "app/utils/useHeader"
import { useStores } from "app/models"
import { ListModal } from "app/components/ListModal"
// 
interface MaintainanceAssetsScreenProps extends AppStackScreenProps<"MaintainanceAssets"> { }

export const MaintainanceAssetsScreen: FC<MaintainanceAssetsScreenProps> = observer(function MaintainanceAssetsScreen() {
  // Pull in one of our MST stores
  const { maintainance, mainDb } = useStores()
  const { assets, isLoading } = maintainance
  const { availableAssets } = mainDb
  // Pull in navigation via hook
  const navigation = useNavigation()
  useEffect(() => {
    maintainance.fetchAll()
    mainDb.fetchAllAssets()
  }, [])
  useHeader({
    title: 'Maintenance', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff',

  })

  const [showAssetList, setShowAssetList] = useState(false)
  return (
    <Screen style={$root} preset="scroll">

      <Button
        onPress={() => {
          setShowAssetList(true)

        }}
        textStyle={{ color: colors.palette.neutral100 }}
        style={{ backgroundColor: colors.palette.primary600, borderRadius: 7, marginBottom: spacing.sm }}
        text='Add Item'
      />
      {isLoading && <View style={{ padding: spacing.sm }}>
        <ActivityIndicator />
      </View>}

      <ListModal

        show={showAssetList}
        data={availableAssets}
        title="Add Assets"
        subTitleKeys={['tagid']}
        titleKeys={["name"]}
        onItemSelected={function (item: any): void {
          maintainance.addAsset(item.id).then(() => {
            mainDb.fetchAllData()
          })
          setShowAssetList(false)
        }} onDismiss={function (): void {
          setShowAssetList(false)
        }}
      />
      <FlatList
        data={assets}
        renderItem={({ item }) => <MaintanceAssetListItem
          item={item}
        />}
        ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        ListFooterComponent={() => <View style={{ height: 50 }} />}
      />
    </Screen>
  )
})

const $root: ViewStyle = {
  flex: 1,
  padding: spacing.sm
}
