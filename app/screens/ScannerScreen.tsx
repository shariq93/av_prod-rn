import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { DeviceEventEmitter, FlatList, NativeModules, TextStyle, View, ViewStyle, useWindowDimensions } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { Button, Screen, Text } from "app/components"
import { useNavigation, useRoute } from "@react-navigation/native"
import { useStores } from "app/models"
import { useHeader } from "app/utils/useHeader"
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { colors, mesures, spacing, typography } from "app/theme"
import { ta } from "date-fns/locale"
const { RFIDReaderModule } = NativeModules;
interface ScannerScreenProps extends AppStackScreenProps<"Scanner"> { }

export const ScannerScreen: FC<ScannerScreenProps> = observer(function ScannerScreen() {
  // Pull in one of our MST stores
  const { } = useStores()
  const [scannedTags, setScannedTags] = useState([])
  const [intervalId, setIntervalId] = useState<any>(-1)
  const [isScanning, setIsScanning] = useState(false)
  const recievedTags = React.useRef([]).current

  // Pull in navigation via hook
  const navigation = useNavigation()
  const { height } = useWindowDimensions()
  useHeader({
    title: 'Scanner', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })
  const route = useRoute();
  const params = route?.params;
  const [items, setItems] = useState([]);
  const [type, setType] = useState('');
  const [bundleAssets, setBundleAssets] = useState([]);
  useEffect(() => {

    if (params.items) {
      setItems(params.items)
    }
    if (params.type) {
      setType(params.type)
    }
    if (params.bundleAssets) {
      setBundleAssets(params.bundleAssets)
    }

  }, [params])



function pingScannerForTags() {
  if (RFIDReaderModule) {
    RFIDReaderModule.getScannedTags((tags) => {
        processScannedTags(tags);
    });
}
    
}


  useEffect(() => {

    if (RFIDReaderModule) RFIDReaderModule.initDevice()
  
    const intId = setInterval(pingScannerForTags, 2000);
    return () => {
     
      clearInterval(intId)
      if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()

    }
  }, [])

  useEffect(() => {
    if (type == 'BUNDLE') {
    checkforBundle()
    }
  }, [scannedTags])

  const checkforBundle = () => {
    console.log({ type });
    if (type == 'BUNDLE') {
      const bundleTags = items[0].tagId

      const tagFound = recievedTags.includes(bundleTags)
      console.log({ bundleTags, tagFound });
      if (tagFound) {
        navigation.goBack()
        navigation.navigate('BundleDetails', { bundle: bundleAssets })
      }

    }
  }

  const processScannedTags = (tags: any[]) => {
    const tempTags = tags.map(tag => tag?.tag)
    console.log({ tempTags });

    setScannedTags(tempTags)
  }
  return (

    <View style={$root}>

      <FlatList
        data={items}
        ListFooterComponent={<View style={{ height: 100 }} />}

        renderItem={({ item }) => {
          return <View style={$card}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
              <View>
                <Text >{item.name}</Text>
                <Text style={$generalText}>{item.tagId}</Text>
              </View>
              {scannedTags.includes(item.tagId) && <View>
                <AntDesign name="checkcircle" size={18} color={colors.success} />
              </View>}
            </View>

          </View>
        }}

        ListEmptyComponent={() => <View>
          <Text size="xs">No data available!</Text>
        </View>}
      />
      <Button
        style={{ position: 'absolute', bottom: 30, width: '80%', alignSelf: 'center' }}
        text={isScanning ? 'Stop Scanning' : "Start Scanning"}
        onPress={() => {
          if (!isScanning) {
            if (RFIDReaderModule) {

              RFIDReaderModule.onScanPressed()
            }
          } else {
            if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
          }
          setIsScanning(!isScanning)
        }}
      />



    </View>

  )
})

const $root: ViewStyle = {

  flex: 1,
  padding: spacing.sm,
  backgroundColor: colors.background
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
const $title: TextStyle = {
  fontFamily: typography.primary.medium,
  color: colors.palette.primary600
}
const $card: ViewStyle = {
  padding: spacing.xs,
  backgroundColor: colors.palette.neutral100,
  marginTop: spacing.sm,
  borderRadius: mesures.borderRadius,

}
const $listTitle: TextStyle = {
  fontFamily: typography.primary.semiBold,
  fontSize: 12,
  color: colors.palette.primary500,
}

const $generalText: TextStyle = {
  fontFamily: typography.primary.light,
  fontSize: 13,
  color: colors.palette.neutral900,
}