import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { DeviceEventEmitter, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { Button, Screen, Text } from "app/components"
import { NativeModules } from 'react-native';
import { colors } from "app/theme"
import { useHeader } from "app/utils/useHeader"


const { RFIDReaderModule } = NativeModules;
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "app/models"

interface TestRfidScreenProps extends AppStackScreenProps<"TestRfid"> { }

export const TestRfidScreen: FC<TestRfidScreenProps> = observer(function TestRfidScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  useHeader({
    title: 'Scann', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },

  })
  const [msg, setMsg] = useState('')
  const [data, setData] = useState(["EMPTY"])
  if (RFIDReaderModule) {
    // Listen for scanned data event
    DeviceEventEmitter.addListener('onTagFound', (event) => {

      if (event?.tag) {
        if (!data.includes(event?.tag)) {
          data.push(event?.tag)
          setData(data)
        }
      }

    });

    DeviceEventEmitter.addListener('onMessage', (event) => {
      setMsg(msg + '\n' + (JSON.stringify(event)));
    });
  }

  useEffect(() => {
    return () => {
      RFIDReaderModule.stopRFScanner()
    }
  }, [])
  return (
    <Screen style={$root} preset="scroll">
      <Button onPress={() => {
        RFIDReaderModule.onScanPressed()
      }
      }>Scan</Button>
      <Button onPress={() => {
        RFIDReaderModule.initDevice()
      }
      }>init</Button>
      <Button onPress={() => {
        RFIDReaderModule.stopRFScanner()
      }
      }>stop</Button>
      <Text>{msg}</Text>
      {data.map((data) => {
        return <Text>{data}</Text>
      })}
    </Screen>
  )
})

const $root: ViewStyle = {
  flex: 1,
}
