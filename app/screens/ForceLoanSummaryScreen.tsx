import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { ScrollView, View, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { Button, ScannableItemForLoan, Screen, Text, TextField } from "app/components"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "app/models"
import { colors, spacing, typography } from "app/theme"
import { useHeader } from "app/utils/useHeader"
import { loadString } from "app/utils/storage"

interface ForceLoanSummaryScreenProps extends AppStackScreenProps<"AssetsLoanSummary"> { }

export const ForceLoanSummaryScreen: FC<ForceLoanSummaryScreenProps> = observer(function ForceLoanSummaryScreen() {
  // Pull in one of our MST stores
  const { mainDb, tagHolder, toast } = useStores()
  const { selectedTags, notScannedTags, projectId } = tagHolder
  const [loading, setLoading] = useState(false)
  // Pull in navigation via hook
  const navigation = useNavigation()
  useHeader({
    title: 'Transaction Summary', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })
  const message = 'This transection will only effect the following Asset not complete bundle/project. You can individually loanout / return remaining assets.'
  // const returnAssets = async () => {
  //   const token = await loadString('token')
  //   const myHeaders = new Headers();
  //   myHeaders.append("Accept", "application/json");
  //   myHeaders.append("Authorization", "Bearer " + token);

  //   const ids = selectedTags.map((it) => it.id)
  //   const formdata = new FormData();
  //   formdata.append("asset_ids", ids.join(','))
  //   formdata.append("user_id", tagHolder.loanee.id + '')


  //   const requestOptions = {
  //     method: "POST",
  //     headers: myHeaders,
  //     body: formdata,
  //     redirect: "follow"
  //   };

  //   fetch("https://rfid.avproduction.sg/api/return/asset", requestOptions)
  //     .then((response) => response.json())
  //     .then((result) => {
  //       if (result.status == 'success') {
  //         mainDb.fetchAllData()
  //         toast.showSuccess('Returned successfully!')
  //         navigation.goBack()
  //         navigation.goBack()
  //       } else {
  //         toast.showError()
  //       }
  //     })
  //     .catch((error) => console.error(error));
  // }


  // const loanoutAsset = async () => {
  //   const token = await loadString('token')
  //   const myHeaders = new Headers();
  //   myHeaders.append("Accept", "application/json");
  //   myHeaders.append("Authorization", "Bearer " + token);

  //   const ids = selectedTags.map((it) => it.tagId)
  //   const formdata = new FormData();
  //   formdata.append("tagids", ids.join(','))
  //   formdata.append("user_id", tagHolder.loanee.id + '')
  //   formdata.append("expected_return_date", tagHolder.returnData)

  //   const requestOptions = {
  //     method: "POST",
  //     headers: myHeaders,
  //     body: formdata,
  //     redirect: "follow"
  //   };

  //   fetch("https://rfid.avproduction.sg/api/loan/asset", requestOptions)
  //     .then((response) => response.json())
  //     .then((result) => {
  //       if (result.status == 'success') {
  //         mainDb.fetchAllData()
  //         toast.showSuccess('Loaned out successful!')
  //         navigation.goBack()
  //         navigation.goBack()
  //       } else {
  //         toast.showError()
  //       }
  //     })
  //     .catch((error) => console.error(error));
  // }


  const returnProject = async (projectId, user_id) => {
    const token = await loadString('token')
    setLoading(true)
    const myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("project_id", projectId)
    formdata.append("user_id", user_id + '')

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/return/project", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == 'success') {
          mainDb.fetchAllData()
          toast.showSuccess('Project returned successfully!')
          navigation.goBack()
          navigation.goBack()
        } else {
          toast.showError()
        }
        setLoading(false)
      })
      .catch((error) => console.error(error));
  }



  const loanOut = async () => {
    setLoading(true)
    const token = await loadString('token')
    const myHeaders = new Headers();

    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("project_id", projectId)
    formdata.append("user_id", tagHolder.loanee.id + '')
    formdata.append("remarks", remarks)
    // formdata.append("tagid", JSON.stringify(scannedTags))

    formdata.append("expected_return_date", tagHolder.returnData)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/loan/project", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == 'success') {
          mainDb.fetchAllData()
          toast.showSuccess('Project Loaned out successfully!')
          navigation.goBack()
          navigation.goBack()
        } else {
          toast.showError()
        }
        setLoading(false)
      })
      .catch((error) => console.error(error));
  }

  const [remarks, setRemarks] = useState('')

  useEffect(() => {
    let tempMessage = 'Not Scanned Tags:'
    notScannedTags.map((item) => {
      tempMessage = (tempMessage + '\n' +item.name+ "("+ item.tagId+")" + ": ")
    })
    setRemarks(tempMessage)
  }, [notScannedTags])
  return (
    <View style={$root} >
      <View>

        <Text size="xs">
          {tagHolder.forLoanOut ? "Assigning To:" : 'Returning From:'}
        </Text>
        <View style={{ flexDirection: 'row' }}>
          <Text style={{ fontFamily: typography.primary.semiBold }}>
            {tagHolder.loanee.name}
          </Text>
          <Text style={{ marginLeft: 5 }}>
            ({tagHolder.loanee.role})
          </Text>
        </View>
        <View>

        </View>
      </View>
      <ScrollView>
        <View style={{ height: 300 }}>
          {selectedTags.map((item) => <ScannableItemForLoan
            item={item}
            childs={item.tags}
            // scannedTags={scannedTags}
            // showChilds={item.tags.length > 0 && scannedTags.includes(item.tagId)}
            scanned={true}
          />
          )}


        </View>
        <View style={{ height: 10 }} />
        <TextField
          value={remarks}
          label="Remarks"
          onChangeText={(text) => {
            setRemarks(text)
          }}
          style={{ fontSize: 13 }}
          multiline
        />
        <View style={{ height: 100 }} />
      </ScrollView>

      <View style={{ position: 'absolute', bottom: 30, width: '90%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around' }}>
        <Button
          disabled={selectedTags.length < 1}
          style={{ width: '95%', backgroundColor: selectedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400, }}
          textStyle={{ color: '#fff' }}
          text={tagHolder.forLoanOut ? "Process Loan out" : 'Process Return'}
          onPress={() => {
            if (tagHolder.forLoanOut) {
              loanOut()
            } else {
              returnProject(projectId, tagHolder.loanee.id)
            }
          }}
        />
      </View>

    </View>
  )
})

const $root: ViewStyle = {
  flex: 1,

  backgroundColor: colors.background,
  padding: spacing.sm
}
