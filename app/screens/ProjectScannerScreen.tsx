import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { DeviceEventEmitter, FlatList, NativeModules, ScrollView, View, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { Button, ScannableItemForLoan, Screen, Text } from "app/components"
import { useNavigation, useRoute } from "@react-navigation/native"
import { TagData, TagDataHolder, User, useStores } from "app/models"
import { colors, spacing, typography } from "app/theme"
import { useHeader } from "app/utils/useHeader"
import { loadString } from "app/utils/storage"
import { DateTimePickerField } from "app/components/DateTimePickerField"
import moment from "moment"
import { ListModal } from "app/components/ListModal"
import { Dialogs, useDialog } from "app/components/Dialogs"
const { RFIDReaderModule } = NativeModules;

interface ProjectScannerScreenProps extends AppStackScreenProps<"BundleScanner"> { }

export const ProjectScannerScreen: FC<ProjectScannerScreenProps> = observer(function ProjectScannerScreen() {
  // Pull in one of our MST stores
  const { tagHolder, mainDb, toast } = useStores()
  const { onlyUsers: allUsers } = mainDb
  const { tagData, loanee, projectId, getAllTags, getAllTagsForProject, scannedTags } = tagHolder
  // const { project, bundle, loanee, getAllTags } = projectTagHolder
  // Pull in navigation via hook
  useEffect(() => {

    return () => {
      tagHolder.clearTagDataArray()
    }
  }, [

  ])
  const navigation = useNavigation()
  useHeader({
    title: 'Project ', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })



  // const [scannedTags, setScannedTags] = useState([])
  const [isScanning, setIsScanning] = useState(false)

  const [selectedUser, setSelectedUser] = useState<User>(undefined)
  const [expectedReturnDate, setExpectedReturnDate] = useState(moment().format('YYYY-MM-DD'))

  const [showModal, setShowModal] = useState(false)
  const [showProcessDialog, setShowProcessDialog] = useState(false)

  const { isVisible, meta, setMeta, show, hide } = useDialog()
  const [loading, setLoading] = useState(false)

  const route = useRoute()
  const { params } = route

  const returnProject = async (projectId, user_id) => {
    const token = await loadString('token')
    setLoading(true)
    const myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("project_id", projectId)
    formdata.append("user_id", user_id + '')

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/return/project", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == 'success') {
          mainDb.fetchAllData()
          toast.showSuccess('Project returned successfully!')
          navigation.goBack()
        } else {
          toast.showError()
        }
        setLoading(false)
      })
      .catch((error) => console.error(error));
  }



  const loanOut = async () => {
    setLoading(true)
    const token = await loadString('token')
    const myHeaders = new Headers();

    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("project_id", projectId)
    formdata.append("user_id", selectedUser.id + '')
    // formdata.append("tagid", JSON.stringify(scannedTags))

    formdata.append("expected_return_date", expectedReturnDate)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/loan/project", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == 'success') {
          mainDb.fetchAllData()
          toast.showSuccess('Project Loaned out successfully!')
          navigation.goBack()
        } else {
          toast.showError()
        }
        setLoading(false)
      })
      .catch((error) => console.error(error));
  }


  const recievedTags = React.useRef([]).current
  const [tempCount, setTempCount] = useState(0)
  if (RFIDReaderModule) {
    DeviceEventEmitter.addListener('onTagFound', (event) => {
      if (event?.tag) {
        if (!recievedTags.includes(event?.tag))
          recievedTags.push(event?.tag)
        setTempCount(recievedTags.length)
      }

    })
  }
  useEffect(() => {
    processScannedTags()
  }, [tempCount])
  useEffect(() => {

    if (RFIDReaderModule) RFIDReaderModule.initDevice()
    return () => {
      if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
    }
  }, [])


  const processScannedTags = () => {


    tagHolder.addScannedTags([...recievedTags])

    // setScannedTags([...recievedTags])
  }


  useEffect(() => {
    setTimeout(() => {
      if (isScanning)

        tagHolder.addScannedTags(['SH - Out door Filming', 'E29000000000000B11038021', 'E29000000000000B11038015', 'E29000000000000B11038018'
          , 'E29000000000000B11038016', 'E29000000000000B11038017', 'E29000000000000B11038020', 'E29000000000000B11038019---'])
      // setScannedTags(['New test', 'E280116060000213235E08B0', 'E2801160600002132394EC2D', 'E280116060000213235E08A0'])

    }, 2000);
  }, [isScanning])

  function findMatchingTags(totalTags, scannedTags): any[] {
    const totalTagsSet = new Set(totalTags);
    const matchingTags = scannedTags.filter(tag => totalTagsSet.has(tag));
    return matchingTags;
  }
  const matchedTags = findMatchingTags(tagHolder.getAllTagsIds(), scannedTags)
  const totalTags = tagHolder.getAllTagsIds()

  // const onlyAssets = tagData.filter(it => it.type == 'ASSET')
  let onlyAssets = []
  let onlyBundles = []
  let projectData: TagData = undefined
  if (tagData.length > 0) {
    onlyAssets = tagData[0].tags.filter(it => it.type == 'ASSET')
    onlyBundles = tagData[0].tags.filter(it => it.type == 'BUNDLE')
    projectData = tagData[0]
    if (projectData) {
      projectData.setScannable(true)
    }
  }


  useEffect(() => {
    enableBundles(onlyBundles)
    if (projectData) {
      recievedTags.push(projectData.tagId)
    }
  }, [])

  const enableBundles = (tags: TagData[]) => {
    tags?.forEach(it => it.setScannable(true))
  }
  const enableProjectAssets = (tags: TagData[]) => {
    tags?.forEach(it => it.setScannable(true))
  }

  const prepareForSummary = () => {
    const onlyScannedAssets = getAllTagsForProject().filter(it => scannedTags.includes(it.tagId) && it.type == 'ASSET');
    const notScannedAssets = getAllTagsForProject().filter(it => (!scannedTags.includes(it.tagId)) && it.type == 'ASSET');

    tagHolder.setSelectedTags(onlyScannedAssets)

    const childTags = onlyScannedAssets.map((it) => {
      const emptyTags: TagData[] = []
      let temp = {
        tagId: (it.tagid || it.tagId),
        id: (it.id + ''),
        name: it.name,
        type: 'ASSET',
        projectId: tagHolder.projectId,
        bundleId: tagHolder.bundleId,
        tags: emptyTags
      }
      return temp
    })


    tagHolder.setBundleId(tagHolder.projectId)
    tagHolder.setLoanee(tagHolder.forLoanOut ? selectedUser : tagHolder.loanee)
    tagHolder.setSelectedTags(childTags)
    tagHolder.setNotScannedTags(notScannedAssets)
    tagHolder.setReturnData(expectedReturnDate)
    navigation.navigate('ForceLoanSummary')
  }

  const renderDialogs = () => {
    return <>
      <ListModal show={showModal}
        data={allUsers}
        title="Select User"
        titleKeys={["name",]}
        subTitleKeys={['email']}
        onItemSelected={function (item: User): void {
          setSelectedUser(item)
          setShowModal(false)
          if (matchedTags.length < totalTags.length) {
            setTimeout(() => setShowProcessDialog(true), 100);
          } else {
            setTimeout(() => show(), 100);
          }
          // setTimeout(() => show(), 100);

        }} onDismiss={function (): void {
          setShowModal(false)
        }}
      />
      <Dialogs
        show={isVisible}
        type={'error'}
        showIcon
        title={"Confirm Loan Out"}
        discription={"Are you sure want to loan out selected items to " + selectedUser?.name + "?"}
        onPossitivePress={async () => {

          loanOut();
          hide()
        }}
        onNegativePress={function (): void {
          hide()
        }}

        possitiveText={"Yes"}
        negativeText={"No"}
      ></Dialogs>
      <Dialogs
        show={showProcessDialog}
        type={'warning'}

        showIcon
        title={"Partial Scan"}
        discription={"All tags are not scanned do you want to continue? "}
        onPossitivePress={async () => {
          setShowProcessDialog(false)
          // loanOut();
          prepareForSummary()

        }}
        onNegativePress={function (): void {
          setShowProcessDialog(false)
        }}

        possitiveText={"Force Fulfill"}
        negativeText={"No"}
      ></Dialogs>
    </>
  }
  return (
    <View style={$root} >
      {renderDialogs()}
      <Text>Scanned: {scannedTags.length > 0 ? scannedTags.length - 1 : scannedTags.length}    Matched: {matchedTags.length > 0 ? matchedTags.length - 1 : matchedTags.length}</Text>
      <View style={{backgroundColor:'#fff',borderRadius:7,paddingHorizontal:10,paddingVertical:5,marginTop:10}}>
        <Text style={{fontFamily:typography.primary.medium}}>Remarks</Text>
        <Text style={{fontSize:14,color:colors.palette.neutral600}}>{tagHolder.remarks}</Text>
      </View>
      <ScrollView>
        <View>
          {projectData && <ScannableItemForLoan
            item={projectData}
            // childs={item.tags}
            hideTagId
            scanned={false}
          />
          }
          <>

            {onlyAssets.length > 0 && <Text>Assets</Text>}
            {enableProjectAssets(onlyAssets)}
            {onlyAssets.map((item) => <ScannableItemForLoan
              item={item}
              childs={item.tags}
              // scannedTags={scannedTags}
              // showChilds={item.tags.length > 0 && scannedTags.includes(item.tagId)}
              scanned={scannedTags.includes(item.tagId)}
            />
            )}

            {onlyBundles.length > 0 && <Text>Bundle</Text>}
            {
              //to enable bundle scan if project is scanned successfully
              enableBundles(onlyBundles)
            }
            {onlyBundles.map((item) => <ScannableItemForLoan
              item={item}
              onPress={() => {
                const tempTags = []
                item.tags.forEach((e: TagData) => {
                  tempTags.push({ ...e, tags: [] })
                  e.setScannable(true)
                });
                tagHolder.setProjectAssets(tempTags)
                navigation.navigate('ProjectBundleScanner')
                // alert('hello')
              }}
              showScanDetails
              childs={item.tags}
              scannedTags={scannedTags}
              // showChilds={scannedTags.includes(item.tagId)}   v
              scanned={scannedTags.includes(item.tagId)}
            />
            )}
          </>
        </View>
        <View style={{ height: 100 }} />
      </ScrollView>
      <View style={{ position: 'absolute', bottom: 30, width: '100%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around' }}>
        {!isScanning && <Button
          style={{ width: '80%' }}
          text={isScanning ? 'Stop Scanning' : "Start Scanning"}
          onPress={() => {
            if (!isScanning) {
              if (RFIDReaderModule) {

                RFIDReaderModule.onScanPressed()
              }
            } else {
              if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
            }
            setIsScanning(!isScanning)
          }}
        />}
        {isScanning && !tagHolder.forLoanOut && !params?.forCheck && <Button
          disabled={matchedTags.length < 1 || loading}
          style={{ width: '80%', backgroundColor: matchedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400 }}
          textStyle={{ color: '#fff' }}
          text={loading ? "Processing..." : 'Return'}
          onPress={() => {
            if (matchedTags.length < totalTags.length) {
              setShowProcessDialog(true)
            } else {
              returnProject(projectId, loanee.id + '')
            }
            // returnProject(projectId, loanee.id)
          }}
        />}
        {isScanning && params?.forCheck && <Button

          style={{ width: '80%', backgroundColor: colors.palette.primary600 }}
          textStyle={{ color: '#fff' }}
          text={'Go Back'}
          onPress={() => {
            // if (matchedTags.length < totalTags.length) {
            navigation.goBack()
            // } else {
            //   returnProject(projectId, loaneId)
            // }
            // returnProject(projectId, loanee.id)
          }}
        />}
        {isScanning && tagHolder.forLoanOut && <View style={{ flexDirection: 'row', flex: 1, }}>
          <DateTimePickerField
            lable="Delivery Date"
            // style={{width:20}}
            containerStyle={{ width: 160 }}
            placeholderText="Select date"
            defaultDate={expectedReturnDate}
            onDateTimeSelect={function (date: string): void {
              setExpectedReturnDate(date)
            }}
          />
          <Button
            disabled={matchedTags.length < 1}
            textStyle={{ color: colors.palette.neutral100, }}
            style={{ backgroundColor: matchedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400, borderRadius: 7, marginTop: spacing.md, flex: 1 }}
            onPress={() => {
              setShowModal(true)
            }}
            text="Assign User" /></View>}
      </View>
    </View>
  )
})

const $root: ViewStyle = {
  flex: 1,
  padding: spacing.sm,
  backgroundColor: colors.background
}
