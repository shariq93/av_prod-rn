import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { DeviceEventEmitter, FlatList, NativeModules, TextStyle, View, ViewStyle, useWindowDimensions } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { Button, ScannableItemForLoan, Screen, Text } from "app/components"
import { useNavigation, useRoute } from "@react-navigation/native"
import { useStores } from "app/models"
import { useHeader } from "app/utils/useHeader"
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { colors, mesures, spacing, typography } from "app/theme"
import { ta } from "date-fns/locale"
import { color } from "react-native-reanimated"
import { loadString } from "app/utils/storage"
const { RFIDReaderModule } = NativeModules;

interface ScannerLoanReturnScreenProps extends AppStackScreenProps<"ScannerLoanReturn"> { }

export const ScannerLoanReturnScreen: FC<ScannerLoanReturnScreenProps> = observer(function ScannerLoanReturnScreen() {
  // Pull in one of our MST stores
  const { loanReturn } = useStores()
  const [scannedTags, setScannedTags] = useState([])
  const [matchedTags, setMatchedTags] = useState([])
  const [bundleAssets, setBundleAssets] = useState([])

  const [isScanning, setIsScanning] = useState(false)
  const recievedTags = React.useRef([]).current

  // Pull in navigation via hook
  const navigation = useNavigation()
  const { height } = useWindowDimensions()
  useHeader({
    title: 'Loan Return', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })
  const route = useRoute();
  const params = route?.params;

  const [items, setItems] = useState([]);
  const [singleAsset, setSingleAsset] = useState<{ name, tagId }>(params?.asset)
  const [singleBundle, setSingleBundle] = useState<{ name, tagId }>(params?.bundle)
  useEffect(() => {
    if (loanReturn.processType == 'SINGLE_ASSET') {
      setSingleAsset(params?.asset)
      const { name, tagId, } = params?.asset
      setItems([{ name, tagId }])
    }
    if (loanReturn.processType == 'SINGLE_BUNDLE') {
      setSingleBundle(params?.bundle)
      const { name, tagId, } = params?.bundle
      setItems([{ name, tagId }])
      setBundleAssets(params?.bundleAssets)
      console.log(JSON.stringify(params?.bundleAssets));

    }


  }, [params])



  // useEffect(() => {
  //   setTimeout(() => {
  //     if (isScanning)
  //       setScannedTags(['111239440944',])
      setTimeout(() => {
        setScannedTags(['111239440944','E29000000000000B11038020',])

      }, 1000);
  //   }, 2000);
  // }, [isScanning])

  function pingScannerForTags() {
    if (RFIDReaderModule) {
      RFIDReaderModule.getScannedTags((tags) => {
          processScannedTags(tags);
      });
  }
      
  }
  
  
    useEffect(() => {
  
      if (RFIDReaderModule) RFIDReaderModule.initDevice()
    
      const intId = setInterval(pingScannerForTags, 2000);
      return () => {
       
        clearInterval(intId)
        if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
  
      }
    }, [])
  useEffect(() => {
    // checkforBundle()
    console.log({ type: loanReturn.processType, scannedTags, singleAsset, matchedTags });
    if (loanReturn.processType == 'SINGLE_ASSET') {
      performMatchForSingleAsset(scannedTags)
    }
    if (loanReturn.processType == 'SINGLE_BUNDLE') {
      performMatchForSingleBundle(scannedTags)
    }
  }, [scannedTags])


  const processScannedTags = (tags: any[]) => {
    const tempTags = tags.map(tag => tag?.tag)
    setScannedTags(tempTags)
  }

  const performMatchForSingleAsset = (scannedTags) => {
    if (scannedTags.includes(singleAsset?.tagId)) {
      matchedTags.push(singleAsset?.tagId)
      setMatchedTags([...matchedTags])
    }
  }
  const performMatchForSingleBundle = (scannedTags) => {
    if (scannedTags.includes(singleBundle?.tagId)) {
      matchedTags.push(singleBundle?.tagId)
      setMatchedTags([...matchedTags])
    }
  }


  
  const returnAsset = async () => {
    const token = await loadString('token')
    const myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("asset_ids", loanReturn.assetId)
    formdata.append("user_id", loanReturn.loneeId)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/return/asset", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        alert("Success")
        navigation.goBack()
      })
      .catch((error) => console.error(error));
  }
  return (

    <View style={$root}>

      <FlatList
        data={items}
        ListFooterComponent={<View style={{ height: 100 }} />}

        renderItem={({ item }) => {
          return <ScannableItemForLoan
            item={item}
            childs={bundleAssets}
            scannedTags={scannedTags}
            showChilds={bundleAssets.length > 0 && scannedTags.includes(item.tagId)}
            scanned={scannedTags.includes(item.tagId)}
          />
        }}

        ListEmptyComponent={() => <View>
          <Text size="xs">No data available!</Text>
        </View>}
      />
      <View style={{ position: 'absolute', bottom: 30, width: '90%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around' }}>
        {!isScanning && <Button
          style={{ width: '80%' }}
          text={isScanning ? 'Stop Scanning' : "Start Scanning"}
          onPress={() => {
            if (!isScanning) {
              if (RFIDReaderModule) {

                RFIDReaderModule.onScanPressed()
              }
            } else {
              if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
            }
            setIsScanning(!isScanning)
          }}
        />}
        {isScanning && loanReturn.processType == 'SINGLE_ASSET' && <Button
          disabled={matchedTags.length < 1}
          style={{ width: '80%', backgroundColor: matchedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400 }}
          textStyle={{ color: '#fff' }}
          text={'Return'}
          onPress={() => {
            returnAsset()
          }}
        />}
        {isScanning && loanReturn.processType == 'SINGLE_BUNDLE' && <Button
          disabled={matchedTags.length < 1}
          style={{ width: '80%', backgroundColor: matchedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400 }}
          textStyle={{ color: '#fff' }}
          text={'Scan Bundle Assets'}
          onPress={() => {
            if (bundleAssets.length > 0) {
              navigation.goBack()
              navigation.navigate('BundleDetails', { bundle: { assets: bundleAssets } })
            } else {
              alert('There are no assets in the bundle')
            }

          }}
        />}
      </View>



    </View>

  )
})

const $root: ViewStyle = {

  flex: 1,
  padding: spacing.sm,
  backgroundColor: colors.background
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
const $title: TextStyle = {
  fontFamily: typography.primary.medium,
  color: colors.palette.primary600
}
const $card: ViewStyle = {
  padding: spacing.xs,
  backgroundColor: colors.palette.neutral100,
  marginTop: spacing.sm,
  borderRadius: mesures.borderRadius,

}
const $listTitle: TextStyle = {
  fontFamily: typography.primary.semiBold,
  fontSize: 12,
  color: colors.palette.primary500,
}

const $generalText: TextStyle = {
  fontFamily: typography.primary.light,
  fontSize: 13,
  color: colors.palette.neutral900,
}