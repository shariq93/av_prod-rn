import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { DeviceEventEmitter, FlatList, NativeModules, ScrollView, View, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { Button, ScannableItemForLoan, Screen, Text } from "app/components"
import { useNavigation, useRoute } from "@react-navigation/native"
import { TagData, User, useStores } from "app/models"
import { colors, spacing } from "app/theme"
import { useHeader } from "app/utils/useHeader"
import { loadString } from "app/utils/storage"
import { DateTimePickerField } from "app/components/DateTimePickerField"
import moment from "moment"
import { ListModal } from "app/components/ListModal"
import { Dialogs, useDialog } from "app/components/Dialogs"
const { RFIDReaderModule } = NativeModules;

interface AssetScannerScreenProps extends AppStackScreenProps<"BundleScanner"> { }

export const AssetScannerScreen: FC<AssetScannerScreenProps> = observer(function AssetScannerScreen() {
  // Pull in one of our MST stores
  const { tagHolder, mainDb, toast } = useStores()
  const { onlyUsers:allUsers } = mainDb
  const { tagData, loanee, projectId, getAllTags } = tagHolder


  const route = useRoute()
  const {params} = route

  useEffect(() => {

    return () => {
      tagHolder.clearTagDataArray()
    }
  }, [

  ])
  const navigation = useNavigation()
  useHeader({
    title: 'Asset', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })



  const [scannedTags, setScannedTags] = useState([])
  const [isScanning, setIsScanning] = useState(false)

  const [selectedUser, setSelectedUser] = useState<User>(undefined)
  const [expectedReturnDate, setExpectedReturnDate] = useState(moment().format('YYYY-MM-DD'))

  const [showModal, setShowModal] = useState(false)
  const [showProcessDialog, setShowProcessDialog] = useState(false)

  const { isVisible, meta, setMeta, show, hide } = useDialog()

  const recievedTags = React.useRef([]).current

  const pingScannerForTags = ()=> {
    if (RFIDReaderModule) {
      RFIDReaderModule.getScannedTags((tags) => {
        console.log(tags);
        
        // processScannedTags(tags);
      });
    }

  }

  const returnProject = async (projectId, user_id) => {
    const token = await loadString('token')
    const myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("project_id", projectId)
    formdata.append("user_id", user_id + '')

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/return/project", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == 'success') {
          mainDb.fetchAllData()
          toast.showSuccess('Project returned successfully!')
          navigation.goBack()
        } else {
          toast.showError()
        }
      })
      .catch((error) => console.error(error));
  }



  const loanOut = async () => {
    const token = await loadString('token')
    const myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token);

    const formdata = new FormData();
    formdata.append("project_id", projectId)
    formdata.append("user_id", selectedUser.id + '')
    // formdata.append("tagid", JSON.stringify(scannedTags))

    formdata.append("expected_return_date", expectedReturnDate)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch("https://rfid.avproduction.sg/api/loan/project", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == 'success') {
          mainDb.fetchAllData()
          toast.showSuccess('Project Loaned out successfully!')
          navigation.goBack()
        } else {
          toast.showError()
        }

      })
      .catch((error) => console.error(error));
  }

  const [tempCount,setTempCount] = useState(0)
  if (RFIDReaderModule) {
  DeviceEventEmitter.addListener('onTagFound', (event) => {
    if (event?.tag) {
      if (!recievedTags.includes(event?.tag))
        recievedTags.push(event?.tag)
        setTempCount(recievedTags.length)
    }

  })
}
 useEffect(() => {
    processScannedTags()
  }, [tempCount])
  useEffect(() => {

    if (RFIDReaderModule) RFIDReaderModule.initDevice()
    return () => {
      if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
    }
  }, [])

  const processScannedTags = () => {



    setScannedTags([...recievedTags])
  }


  // useEffect(() => {
  //   setTimeout(() => {
  //     if (isScanning)
  //       setScannedTags(['sh123', 'E280116060000213235E08C0', 'E29000000000000B11038019', 'E280116060000213235E0880','E29000000000000B11038010', 'E29000000000000B11038012','E29000000000000B11038020'])
  //     // setScannedTags(['New test', 'E280116060000213235E08B0', 'E2801160600002132394EC2D', 'E280116060000213235E08A0'])

  //   }, 2000);
  // }, [isScanning])

  function findMatchingTags(totalTags, scannedTags): any[] {
    const totalTagsSet = new Set(totalTags);
    const matchingTags = scannedTags.filter(tag => totalTagsSet.has(tag));
    return matchingTags;
  }
  const matchedTags = findMatchingTags(tagHolder.getAllTagsIds(), scannedTags)
  const totalTags = tagHolder.getAllTagsIds()

 

  const prepareForSummary = () => {
    const onlyScannedAssets = getAllTags().filter(it => scannedTags.includes(it.tagId) && it.type == 'ASSET');

    tagHolder.setSelectedTags(onlyScannedAssets)

    const childTags = onlyScannedAssets.map((it) => {
      const emptyTags: TagData[] = []
      let temp = {
        tagId: (it.tagid || it.tagId),
        id: (it.id + ''),
        name: it.name,
        type: 'ASSET',
        projectId: tagHolder.projectId,
        bundleId: tagHolder.bundleId,
        tags: emptyTags
      }
      return temp
    })


    tagHolder.setBundleId(tagHolder.projectId)
    tagHolder.setLoanee(tagHolder.forLoanOut ? selectedUser : tagHolder.loanee)
    tagHolder.setSelectedTags(childTags)
    tagHolder.setReturnData(expectedReturnDate)
    navigation.navigate('AssetsLoanSummary')
  }
  return (
    <View style={$root} >
      <ListModal show={showModal}
        data={allUsers}
        title="Select User"
        titleKeys={["name",]}
        subTitleKeys={['email']}
        onItemSelected={function (item: User): void {
          setSelectedUser(item)
          setShowModal(false)
          // if (matchedTags.length < totalTags.length) {
          //   setTimeout(() => setShowProcessDialog(true), 100);
          // } else {
          //   setTimeout(() => show(), 100);
          // }
          setTimeout(() => setShowProcessDialog(true), 100);

        }} onDismiss={function (): void {
          setShowModal(false)
        }}
      />
      <Dialogs
        show={isVisible}
        type={'error'}
        showIcon
        title={"Confirm Loan Out"}
        discription={"Are you sure want to loan out selected items to " + selectedUser?.name + "?"}
        onPossitivePress={async () => {

          loanOut();
          hide()
        }}
        onNegativePress={function (): void {
          hide()
        }}

        possitiveText={"Yes"}
        negativeText={"No"}
      ></Dialogs>
      <Dialogs
        show={showProcessDialog}
        type={'warning'}
        showIcon
        title={"Scanned"}
        discription={"Asset scanned! Do you want to contine?"}
        onPossitivePress={async () => {

          // loanOut();
          prepareForSummary()
          setShowProcessDialog(false)
        }}
        onNegativePress={function (): void {
          setShowProcessDialog(false)
        }}

        possitiveText={"Yes"}
        negativeText={"No"}
      ></Dialogs>
     <Text>Scanned: {scannedTags.length}    Matched: {matchedTags.length}</Text>
      <ScrollView>
        <View>
          {tagData.map((item) => <ScannableItemForLoan
            item={item}
            childs={item.tags}
            // scannedTags={scannedTags}
            // showChilds={item.tags.length > 0 && scannedTags.includes(item.tagId)}
            scanned={scannedTags.includes(item.tagId)}
          />
          )}

         
        </View>
        <View style={{ height: 100 }} />
      </ScrollView>
      <View style={{ position: 'absolute', bottom: 30, width: '100%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around' }}>
        {!isScanning && <Button
          style={{ width: '80%' }}
          text={isScanning ? 'Stop Scanning' : "Start Scanning"}
          onPress={() => {
            if (!isScanning) {
              if (RFIDReaderModule) {

                RFIDReaderModule.onScanPressed()
              }
            } else {
              if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
            }
            setIsScanning(!isScanning)
          }}
        />}
        {isScanning && !tagHolder.forLoanOut && !params?.forCheck && <Button
          disabled={matchedTags.length < 1}
          style={{ width: '80%', backgroundColor: matchedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400 }}
          textStyle={{ color: '#fff' }}
          text={'Return'}
          onPress={() => {
            // if (matchedTags.length < totalTags.length) {
              setShowProcessDialog(true)
            // } else {
            //   returnProject(projectId, loaneId)
            // }
            // returnProject(projectId, loanee.id)
          }}
        />}
         {isScanning && params?.forCheck && <Button

          style={{ width: '80%', backgroundColor: colors.palette.primary600 }}
          textStyle={{ color: '#fff' }}
          text={'Go Back'}
          onPress={() => {
            // if (matchedTags.length < totalTags.length) {
             navigation.goBack()
            // } else {
            //   returnProject(projectId, loaneId)
            // }
            // returnProject(projectId, loanee.id)
          }}
        />}

        {isScanning && tagHolder.forLoanOut && <View style={{ flexDirection: 'row', flex: 1, }}>
          <DateTimePickerField
            lable="Delivery Date"
            // style={{width:20}}
            containerStyle={{ width: 160 }}
            placeholderText="Select date"
            defaultDate={expectedReturnDate}
            onDateTimeSelect={function (date: string): void {
              setExpectedReturnDate(date)
            }}
          />
          <Button
            disabled={matchedTags.length < 1}
            textStyle={{ color: colors.palette.neutral100, }}
            style={{ backgroundColor: matchedTags.length > 0 ? colors.palette.primary600 : colors.palette.neutral400, borderRadius: 7, marginTop: spacing.md, flex: 1 }}
            onPress={() => {
              setShowModal(true)
            }}
            text="Assign User" /></View>}
      </View>
    </View>
  )
})

const $root: ViewStyle = {
  flex: 1,
  padding: spacing.sm,
  backgroundColor: colors.background
}
