import React, { FC, useEffect, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import { ImageBackground, View, ViewStyle, ImageStyle, Animated, Dimensions, Easing } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { AppStackScreenProps, navigate } from "../navigators"
import { RoundedCornerBox } from "../components"

import { CommonActions, useNavigation } from "@react-navigation/native"
import { useStores } from "../models"
import { LoginForm } from "./LoginForm"

import { spacing } from "../theme"


import { translate } from "../i18n"
const { height,width } = Dimensions.get('window')

// @ts-ignore
export const LoginScreen: FC<StackScreenProps<AppStackScreenProps, "Login">> = observer(function LoginScreen() {
  // Pull in one of our MST stores
  const { auth, toast } = useStores()
  const [currentForm, setCurrenForm] = useState<'' | 'LOGIN'>('')
  // Pull in navigation via hook
  const navigation = useNavigation()


  const $height = useRef(new Animated.Value((height / 4))).current;

  const resizeForLogin = () => {
    Animated.timing($height, {
      toValue: height / 2.3,
      easing: Easing.elastic(.9),
      duration: 300,
      useNativeDriver: false,
    }).start()
  }

  useEffect(() => {
    if (currentForm == 'LOGIN') resizeForLogin()


  }, [currentForm])
  useEffect(() => {
    setCurrenForm('LOGIN')
  }, [])
  const onLoginSuceess = () => {
    toast.showSuccess(`Welcome ${auth.user.name}`)

  
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{ name: 'Home' }],
      }),
    );

  }

  const onLoginError = (error) => {
    toast.showError(error)
  }
  const onSignError = (error) => {
    toast.showError()
  }
  return (
    <View style={$root} >

      <ImageBackground
        resizeMode='cover'
        source={$images.background}
        style={{ flex: 1 }}
      >
        <View
          style={{
            height,width,backgroundColor:'#000',opacity:.7
          }}
        />
        <RoundedCornerBox height={$height} childrens={
          <View style={{ marginTop: spacing.md }}>

            {currentForm == 'LOGIN' && <LoginForm
             
              onLoginPressed={(email, password) => {

              // navigate('Home')
                auth.loginUser(email, password, onLoginSuceess, onLoginError)
              }}
              onForgetPassClicked={() => { setCurrenForm('FORGETPASSWORD') }} />}

          </View>}
        />


      </ImageBackground>


    </View>
  )
})

const $root: ViewStyle = {
  flex: 1,
}
const $logo_image: ImageStyle = {
  resizeMode: 'contain',
  maxWidth: 250,
  alignSelf: 'center',

}
const $images = {
  background: require('../../assets/images/bg.jpg'),
  logo: require('../../assets/images/app-icon-all.png')
}
