import React, { FC, useState } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, TouchableOpacity, View, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { AvailableAssetListItem, BundleListItem, ProjectListItem, Screen, Text } from "app/components"
import { useNavigation } from "@react-navigation/native"
import { TagData, useStores } from "app/models"
import { useHeader } from "app/utils/useHeader"
import { colors, spacing, typography } from "app/theme"

interface LoanOutScreenProps extends AppStackScreenProps<"LoanOut"> { }

export const LoanOutScreen: FC<LoanOutScreenProps> = observer(function LoanOutScreen() {
  // Pull in one of our MST stores
  const { mainDb, auth, loanReturn, tagHolder } = useStores()
  const { user } = auth
  const { allAssets, availableAssets, availableBundles, availableProjects } = mainDb


  // Pull in navigation via hook
  const navigation = useNavigation()

  useHeader({
    title: 'Loan Out', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff',

  })

  const [selectedIndex, setSelectedIndex] = useState(0)
  return (
    <Screen style={$root} preset="scroll">
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10, }}>
        {/* <TouchableOpacity
          onPress={() => {
            setSelectedIndex(0)
          }}
          style={{ ...$tabs, backgroundColor: selectedIndex == 0 ? colors.palette.primary600 : colors.separator }}>
          <Text style={{ color: selectedIndex == 0 ? '#fff' : colors.palette.primary600 }}>All </Text>
        </TouchableOpacity> */}
        <View style={{ width: 5 }} />
        <TouchableOpacity
          onPress={() => {
            setSelectedIndex(0)
          }}
          style={{ ...$tabs, backgroundColor: selectedIndex == 0 ? colors.palette.primary600 : colors.separator }}>
          <Text style={{ color: selectedIndex == 0 ? '#fff' : colors.palette.primary600 }}>Assets</Text>
        </TouchableOpacity>
        <View style={{ width: 5 }} />
        <TouchableOpacity
          onPress={() => {
            setSelectedIndex(1)
          }}
          style={{ ...$tabs, backgroundColor: selectedIndex == 1 ? colors.palette.primary600 : colors.separator }}>
          <Text style={{ color: selectedIndex == 1 ? '#fff' : colors.palette.primary600 }}>Bundle</Text>
        </TouchableOpacity>
        <View style={{ width: 5 }} />
        <TouchableOpacity
          onPress={() => {
            setSelectedIndex(2)
          }}
          style={{ ...$tabs, backgroundColor: selectedIndex == 2 ? colors.palette.primary600 : colors.separator }}>
          <Text style={{ color: selectedIndex == 2 ? '#fff' : colors.palette.primary600 }}>Projects</Text>
        </TouchableOpacity>
      </View>

      {/* {selectedIndex == 0 && <View>
        <Text style={{ fontFamily: typography.primary.semiBold }}>Assets</Text>
        <FlatList
          data={allAssets}
          renderItem={({ item }) => <AvailableAssetListItem
            onPress={() => {
              // navigation.navigate('BundleDetails', { bundle: item })
            }}
            item={item}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />
      </View>} */}
      {selectedIndex == 0 && <View>
        <Text style={{ fontFamily: typography.primary.semiBold }}>Assets</Text>
        <FlatList
          data={availableAssets}
          renderItem={({ item }) => <AvailableAssetListItem
            onPress={() => {
              
              const emptyTags: TagData[] = []
              const tempAsset = {
                tagId: (item.tagid),
                name: item.name,
                type: 'ASSET',
                id: (item.id + ""),
                projectId: "-1",
                bundleId: "-1",
                tags: emptyTags
              }
              tagHolder.setBundleId(-1)
              tagHolder.isForLoanout(true)
              tagHolder.addTagData(tempAsset)
              tagHolder.setShowSummaryMessage(false)
              // loanReturn.setSingleBundle(item.loanee.id, item.id, item.assets)
              // navigation.navigate('ScannerLoanReturn',
              //   {
              //     bundle: { name: item.name, tagId: (item.tagid || item.tagId), },
              //     bundleAssets: item.assets,
              //   })

              navigation.navigate('AssetScanner')
            }}

            item={item}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />
      </View>}
      {selectedIndex == 1 && <View>
        <Text style={{ fontFamily: typography.primary.semiBold }}>Bundles</Text>
        <FlatList
          data={availableBundles}
          renderItem={({ item }) => <BundleListItem
            onPress={() => {

              const childTags = item.assets.filter(it=>it.loan_status=='0').map((it) => {
                const emptyTags: TagData[] = []
                let temp = {
                  tagId: it.tagid,
                  id: (it.id + ''),
                  name: it.name,
                  type: 'ASSET',
                  projectId: "-1",
                  bundleId: (item.id + ''),
                  tags: emptyTags
                }
                return temp
              })

              const tempBundle = {
                tagId: (item.tagid || item.tagId),
                name: item.name,
                type: 'BUNDLE',
                id: (item.id + ""),
                projectId: "-1",
                bundleId: "-1",
                tags: childTags
              }
              tagHolder.setBundleId(item.id)
              tagHolder.isForLoanout(true)
              tagHolder.addTagData(tempBundle)
              navigation.navigate('BundleScanner')
            }}
            item={item}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />
      </View>}
      {selectedIndex == 2 && <View>
        <Text style={{ fontFamily: typography.primary.semiBold }}>Projects</Text>
        <FlatList
          data={availableProjects}
          renderItem={({ item }) => <ProjectListItem
            onPress={() => {
              const bundles = item.bundles.filter(it=>it.loan_status=='0').map((it) => {


                const bundleAssets = it.assets.filter(it=>it.loan_status=='0').map((asset) => {
                  const emptyTags: TagData[] = []
                  let temp = {
                    tagId: asset.tagid,
                    id: (asset.id + ''),
                    name: asset.name,
                    type: 'ASSET',
                    projectId: (item.id + ''),
                    bundleId: (it.id + ''),
                    tags: emptyTags
                  }
                  return temp
                })

                let temp = {
                  tagId: it.tagid,
                  id: (it.id + ''),
                  name: it.name,
                  type: 'BUNDLE',
                  projectId: (item.id + ''),
                  bundleId: (it.id + ''),
                  tags: bundleAssets
                }
                return temp
              })
              const assets = item.assets.filter(it=>it.loan_status=='0').map((it) => {
                const emptyTags: TagData[] = []
                let temp = {
                  tagId: it.tagid,
                  id: (it.id + ''),
                  name: it.name,
                  type: 'ASSET',
                  projectId: "-1",
                  bundleId: (item.id + ''),
                  tags: emptyTags
                }
                return temp
              })
              const childTags = [...assets, ...bundles]
              const tempBundle = {
                tagId: (item.name),
                name: item.name,
                type: 'PROJECT',
                id: (item.id + ""),
                projectId: "-1",
                bundleId: "-1",
                tags: childTags
              }
              tagHolder.setProjectId(item.id)
              tagHolder.addTagData(tempBundle)
              tagHolder.isForLoanout(true)
              navigation.navigate('ProjectScanner')
            }}
            item={item}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />
      </View>}
    </Screen>
  )
})

const $root: ViewStyle = {
  flex: 1,
  padding: spacing.sm
}
const $tabs: ViewStyle = {
  flex: 1,
  backgroundColor: colors.palette.primary600,
  paddingVertical: spacing.sm,
  justifyContent: 'center', alignItems: 'center',
  borderRadius: 7

}
