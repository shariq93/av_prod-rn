import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, View, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { LoanedBundleListItem, Screen, Text } from "app/components"
import { useNavigation, useRoute } from "@react-navigation/native"
import { LoanBundle, useStores } from "app/models"
import { colors, spacing } from "app/theme"
import { useHeader } from "app/utils/useHeader"

interface ProjectDetailsScreenProps extends AppStackScreenProps<"ProjectDetails"> { }

export const ProjectDetailsScreen: FC<ProjectDetailsScreenProps> = observer(function ProjectDetailsScreen() {
  // Pull in one of our MST stores
  const { } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()
  useHeader({
    title: 'Project Details', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })
  const [bundle, setBundle] = useState<LoanBundle[]>([])
  const route = useRoute()
  const params = route?.params
  useEffect(() => {
    const { bundles } = params?.project

    if (bundles) {
      setBundle(bundles)
    }

  }, [])

  return (
    <Screen style={$root} preset="scroll">
      <FlatList
        data={bundle}
        renderItem={({ item }) => <LoanedBundleListItem
          onPress={() => {
            navigation.navigate('BundleDetails', { bundle: item })
          }}
          item={item}
        />}
        ListFooterComponent={()=><View style={{height:50}}/>}
        ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
      />
    </Screen>
  )
})

const $root: ViewStyle = {
  flex: 1,
  padding: spacing.sm
}
