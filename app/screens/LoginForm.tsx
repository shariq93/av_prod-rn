import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { View, ViewStyle, ImageStyle, Pressable, StyleProp, ActivityIndicator } from "react-native"
import { Button, HeadingWithUnderline, Text, TextField, Toggle } from "../components"

import { Feather } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { colors, spacing, typography } from "../theme"
import { useFormik } from 'formik';
import * as yup from 'yup'
import { useStores } from "../models";
import { AntDesign } from '@expo/vector-icons';
import { color } from "react-native-reanimated";

export interface LoginFormProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>

  onLoginPressed: (email: string, password: string) => void
  onForgetPassClicked?: () => void

  onClose?: () => void
  forDialog?: boolean

}

export const LoginForm = observer(function LoginScreen(props: LoginFormProps) {
  const { onClose, onLoginPressed, forDialog } = props
  const [loginType, setLoginType] = useState("0")
  const { auth } = useStores()
  const { loading } = auth
  const $validationSchema = yup.object({

    email: yup.string()
      .required('Email is required'),
    password: yup.string()
      .min(6, 'Password can not be less then 6 character')
      .required('Password is required'),
  })
  const { handleChange, handleSubmit, handleBlur, values, errors, touched } = useFormik({
    validationSchema: $validationSchema,
    initialValues: {
      email: 'admin@admin.com',
      password: 'password',
    },
    onSubmit: values => {

      onLoginPressed(values.email, values.password)
    },
  });
  return (
    <>
      <HeadingWithUnderline label='Sign In' ></HeadingWithUnderline>
      <TextField
        placeholder="Email"

        value={values.email}
        status={(errors.email && touched.email) ? 'error' : undefined}
        helper={touched.email && errors.email}
        onBlur={handleBlur('email')}
        onChangeText={handleChange('email')}
        inputWrapperStyle={{ justifyContent: 'center', alignItems: 'center', }}
        LeftAccessory={() => <View style={{ marginLeft: 10 }}>
          <Feather name='user' size={20} color={colors.palette.secondary500} />
        </View>}

        label="email"
      />
      <TextField
        containerStyle={{ marginTop: spacing.sm }}
        placeholder="Password"
        value={values.password}
        secureTextEntry={true}
        status={(errors.password && touched.password) ? 'error' : undefined}
        helper={touched.password && errors.password}
        onBlur={handleBlur('password')}
        onChangeText={handleChange('password')}
        inputWrapperStyle={{ justifyContent: 'center', alignItems: 'center', }}
        LeftAccessory={() => <View style={{ marginLeft: 10 }}>
          <Feather name="lock" size={20} color={colors.palette.secondary500} />
        </View>}
        label='Password'
      />



      {!loading && <Button
        onPress={() => {

          handleSubmit()
        }}
        textStyle={{ color: colors.palette.neutral100 }}
        style={{ backgroundColor: colors.palette.primary600, borderRadius: 7, marginTop: spacing.md }}
        text='Login'
      />}

      {loading && <ActivityIndicator style={{ marginTop: spacing.lg }} color={colors.palette.primary600} />}


    </>
  )
})
