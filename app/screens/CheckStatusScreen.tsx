import React, { FC, useState } from "react"
import { observer } from "mobx-react-lite"
import { ActivityIndicator, FlatList, TouchableOpacity, View, ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { LoanedAssetsListItem, LoanedBundleListItem, LoanedProjectListItem, ScannerDialog, Screen, Text } from "app/components"
import { useNavigation } from "@react-navigation/native"
import { TagData, useStores } from "app/models"
import { colors, spacing, typography } from "app/theme"
import { useHeader } from "app/utils/useHeader"
import { MaterialCommunityIcons } from '@expo/vector-icons';
interface CheckStatusScreenProps extends AppStackScreenProps<"CheckStatus"> { }

export const CheckStatusScreen: FC<CheckStatusScreenProps> = observer(function CheckStatusScreen() {
  // Pull in one of our MST stores
  const { mainDb, auth, loanReturn, tagHolder } = useStores()
  const { allLoans, loading } = mainDb

  const { allAssets, assets, bundles, projects } = allLoans
  // Pull in navigation via hook
  const navigation = useNavigation()
  useHeader({
    title: 'Check Status', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff',

  })

  const [selectedIndex, setSelectedIndex] = useState(0)
  return (
    <Screen style={$root} preset="fixed">
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 5, }}>
          <TouchableOpacity
            onPress={() => {
              setSelectedIndex(0)
            }}
            style={{ ...$tabs, backgroundColor: selectedIndex == 0 ? colors.palette.primary600 : colors.separator }}>
            <Text style={{ color: selectedIndex == 0 ? '#fff' : colors.palette.primary600 }}>Assets </Text>
          </TouchableOpacity>
          <View style={{ width: 5 }} />
          <TouchableOpacity
            onPress={() => {
              setSelectedIndex(1)
            }}
            style={{ ...$tabs, backgroundColor: selectedIndex == 1 ? colors.palette.primary600 : colors.separator }}>
            <Text style={{ color: selectedIndex == 1 ? '#fff' : colors.palette.primary600 }}>Bundle</Text>
          </TouchableOpacity>
          <View style={{ width: 5 }} />
          <TouchableOpacity
            onPress={() => {
              setSelectedIndex(2)
            }}
            style={{ ...$tabs, backgroundColor: selectedIndex == 2 ? colors.palette.primary600 : colors.separator }}>
            <Text style={{ color: selectedIndex == 2 ? '#fff' : colors.palette.primary600 }}>Projects</Text>
          </TouchableOpacity>
          <View style={{ width: 5 }} />
          {/* <TouchableOpacity
            onPress={() => {
              setSelectedIndex(3)
            }}
            style={{ ...$tabs, backgroundColor: selectedIndex == 3 ? colors.palette.primary600 : colors.separator }}>
            <Text style={{ color: selectedIndex == 3 ? '#fff' : colors.palette.primary600 }}>Projects</Text>
          </TouchableOpacity> */}
        </View>

        <Text style={{ fontFamily: typography.primary.semiBold }}>Loaned Items</Text>
        {/* {selectedIndex == 0 && allAssets.map((item) => {
          return <View>
            <AssetListItem item={item} />
            <View style={{ height: 8 }} />
          </View>
        })} */}

        {loading && <ActivityIndicator color={colors.palette.primary600}/>}
        {selectedIndex == 0 && <FlatList
          data={assets}
          ListEmptyComponent={() => <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <Text>No Asset available to return</Text>
          </View>}
          renderItem={({ item }) => <LoanedAssetsListItem

            item={item}

            onPress={() => {

              const emptyTags: TagData[] = []
              const tempAsset = {
                tagId: (item.tagid),
                name: item.name,
                type: 'ASSET',
                id: (item.id + ""),
                projectId: "-1",
                bundleId: "-1",
                tags: emptyTags
              }
              tagHolder.setBundleId(-1)
              tagHolder.setLoanee(item.loanee)
              tagHolder.addTagData(tempAsset)
              tagHolder.setShowSummaryMessage(false)
              // loanReturn.setSingleBundle(item.loanee.id, item.id, item.assets)
              // navigation.navigate('ScannerLoanReturn',
              //   {
              //     bundle: { name: item.name, tagId: (item.tagid || item.tagId), },
              //     bundleAssets: item.assets,
              //   })

              navigation.navigate('AssetScanner',{forCheck:true})
            }}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />}


        {selectedIndex == 1 && <FlatList
          data={bundles}
          ListEmptyComponent={() => <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <Text>No bundle available to return</Text>
          </View>}
          renderItem={({ item }) => <LoanedBundleListItem
            onPress={() => {

              const childTags = item.assets.map((it) => {
                const emptyTags: TagData[] = []
                let temp = {
                  tagId: it.tagid,
                  id: (it.id + ''),
                  name: it.name,
                  type: 'ASSET',
                  projectId: "-1",
                  bundleId: (item.id + ''),
                  tags: emptyTags
                }
                return temp
              })

              const tempBundle = {
                tagId: (item.tagid || item.tagId),
                name: item.name,
                type: 'BUNDLE',
                id: (item.id + ""),
                projectId: "-1",
                bundleId: "-1",
                tags: childTags
              }
              tagHolder.setBundleId(item.id)
              tagHolder.setLoanee(item.loanee)
              tagHolder.addTagData(tempBundle)
              // loanReturn.setSingleBundle(item.loanee.id, item.id, item.assets)
              // navigation.navigate('ScannerLoanReturn',
              //   {
              //     bundle: { name: item.name, tagId: (item.tagid || item.tagId), },
              //     bundleAssets: item.assets,
              //   })

              navigation.navigate('BundleScanner',{forCheck:true})
            }}
            item={item}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />}
        {selectedIndex == 2 && <FlatList
          data={projects}
          ListEmptyComponent={() => <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <Text>No project available to return</Text>
          </View>}
          renderItem={({ item }) => <LoanedProjectListItem
            item={item}
            onPress={() => {
              const bundles = item.bundles.map((it) => {


                const bundleAssets = it.assets.map((asset) => {

                  const emptyTags: TagData[] = []
                  let temp = {
                    tagId: asset.tagid,
                    id: (asset.id + ''),
                    name: asset.name,
                    type: 'ASSET',
                    maintainance:(asset.in_maintainance=='1'),
                    projectId: (item.id + ''),
                    bundleId: (it.id + ''),
                    tags: emptyTags
                  }
                  return temp
                })

                let temp = {
                  tagId: it.tagid,
                  id: (it.id + ''),
                  name: it.name,
                  type: 'BUNDLE',
                  projectId: (item.id + ''),
                  bundleId: (it.id + ''),
                  tags: bundleAssets
                }
                return temp
              })
              const assets = item.assets.map((it) => {
                const emptyTags: TagData[] = []
                let temp = {
                  tagId: it.tagid,
                  id: (it.id + ''),
                  name: it.name,
                  type: 'ASSET',
                  projectId: "-1",
                  maintainance:(it.in_maintainance=='1'),
                  bundleId: (item.id + ''),
                  tags: emptyTags
                }
                return temp
              })
              const childTags = [...assets, ...bundles]
              const tempBundle = {
                tagId: (item.name),
                name: item.name,
                type: 'PROJECT',
                id: (item.id + ""),
                projectId: "-1",
                bundleId: "-1",
                tags: childTags
              }
              tagHolder.setProjectId(item.id)
              tagHolder.setLoanee(item.loanee)
              tagHolder.addTagData(tempBundle)

              navigation.navigate('ProjectScanner',{forCheck:true})
              // loanReturn.setSingleBundle(item.loanee.id, item.id, item.assets)
              // navigation.navigate('ScannerLoanReturn',
              //   {
              //     bundle: { name: item.name, tagId: (item.tagid || item.tagId), },
              //     bundleAssets: item.assets,
              //   })

              // navigation.navigate('ProjectDetails', { project: item })
            }}
          />}
          ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
        />}
    </Screen >
  )
})

const $root: ViewStyle = {
  flex: 1,
  padding: spacing.sm,

}
const $box: ViewStyle = {
  flex: 1,
  backgroundColor: colors.palette.primary600,
  paddingVertical: spacing.sm,
  justifyContent: 'center', alignItems: 'center',
  borderRadius: 7

}
const $tabs: ViewStyle = {
  flex: 1,
  backgroundColor: colors.palette.primary600,
  paddingVertical: spacing.sm,
  justifyContent: 'center', alignItems: 'center',
  borderRadius: 7

}