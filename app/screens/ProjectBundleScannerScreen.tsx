import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle } from "react-native"
import { AppStackScreenProps } from "app/navigators"
import { ScannableItemForLoan, Screen, Text } from "app/components"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "app/models"
import { useHeader } from "app/utils/useHeader"
import { colors, spacing } from "app/theme"

interface ProjectBundleScannerScreenProps extends AppStackScreenProps<"ProjectBundleScanner"> { }

export const ProjectBundleScannerScreen: FC<ProjectBundleScannerScreenProps> = observer(function ProjectBundleScannerScreen() {
  // Pull in one of our MST stores
  const { tagHolder } = useStores()
  const { tagData, loanee, projectId, getAllTags, scannedTags, projectAsset } = tagHolder
  // Pull in navigation via hook
  const navigation = useNavigation()
  useHeader({
    title: 'Bundle Assets', backgroundColor: colors.palette.primary600, titleStyle: { color: '#FFF' },
    leftIcon: 'back', onLeftPress: () => { navigation.goBack() }, leftIconColor: '#fff'
  })

  return (
    <Screen style={$root} preset="scroll">
      {projectAsset.map((item) => <ScannableItemForLoan
        item={item}

        childs={item.tags}
        // scannedTags={scannedTags}
        // showChilds={scannedTags.includes(item.tagId)}   v
        scanned={scannedTags.includes(item.tagId)}
      />
      )}
    </Screen>
  )
})

const $root: ViewStyle = {
  flex: 1,
  padding:spacing.sm
}
