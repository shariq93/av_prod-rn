import * as React from "react"
import { StyleProp, TextStyle, TouchableOpacity, useColorScheme, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, spacing, typography } from "../theme"
import { Text } from "./Text"
import { useEffect, useState } from "react"
import moment from "moment";
import DateTimePickerModal from "react-native-modal-datetime-picker";


export interface DateTimePickerFieldProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  defaultDate: string,
  onDateTimeSelect: (date: string) => void,
  containerStyle?: StyleProp<ViewStyle>,
  placeholderText?: string,
  fomate?: string
  type?: 'date' | 'time'
  lable?: string
  error?: string
  mandetory?: boolean
}

/**
 * Describe your component here
 */
export const DateTimePickerField = observer(function DateTimePickerField(props: DateTimePickerFieldProps) {
  const { style, onDateTimeSelect,error, mandetory, lable, placeholderText, fomate, defaultDate, containerStyle, type } = props

  let dateFormate = ""
  if (type == 'time') dateFormate = fomate ? fomate : 'hh:mm A'
  else dateFormate = fomate ? fomate : 'YYYY-MM-DD'
  const $styles = [$container, style]
  const [selectedDate, setSelelectedDate] = useState("");
  const [showModal, setShowModal] = useState(false);
  const colorScheme = useColorScheme()
  let [isDarkModeEnabled, setIsdarkmoodEnabled] = useState(false);

  useEffect(() => {
    setIsdarkmoodEnabled(colorScheme === 'dark')
  }, [colorScheme])

  let onDateChange = (date) => {
    setShowModal(false);
    setSelelectedDate(date);
    onDateTimeSelect(date);
  };

  useEffect(() => {
    setSelelectedDate(defaultDate);
  }, [defaultDate]);


  return (
    <TouchableOpacity
      style={$styles}
      onPress={() => {
        setShowModal(true);
      }}
    >
      <>


       {lable && <>
          <View style={{ flexDirection: 'row' }}>
            <Text style={$text}>{lable}</Text>
            {mandetory && <Text style={$star} text={'*'} />}
          </View>
        </>}


        <View style={[$action, containerStyle,error && {borderColor:colors.palette.error}]}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignSelf: "center",
            }}
          >
            {/* <FontAwesome name="calendar-o" color={"gray"} size={15} /> */}
            {selectedDate != "" && (
              <Text style={{ fontSize: 14, marginLeft: 15 }}>{selectedDate}</Text>
            )}
            {selectedDate == "" && (
              <Text style={{ fontSize: 14, marginLeft: 15, color: colors.palette.neutral300 }}>
                {placeholderText}
              </Text>
            )}

          </View>

          <DateTimePickerModal
            locale="en_US"
            isDarkModeEnabled={isDarkModeEnabled}
            isVisible={showModal}
           
            mode={type}
            date={
              selectedDate
                ? moment(selectedDate, dateFormate).toDate()
                : new Date()
            }
            onConfirm={(date) => {
              onDateChange(moment(date).format(dateFormate));
            }}

            onCancel={() => {
              setShowModal(false);
            }}
          />
        </View>
        {error && <Text style={{
          color: colors.palette.error, marginTop: spacing.xxxs,
          fontSize: 14
        }}>{error}</Text>}
      </>

    </TouchableOpacity>
  )
})

const $container: ViewStyle = {
  flex: 1,


}



const $action: ViewStyle = {
  flexDirection: "row",
  borderWidth: 1,
  borderColor: colors.palette.neutral300,
  borderRadius: 7,
  paddingLeft: 10,
  paddingRight: 10,
  backgroundColor: colors.palette.neutral100,
  minHeight: 45,
}

const $text: TextStyle = {
  fontFamily: typography.primary.medium,
  fontSize: 14,
  color: colors.palette.neutral700,
}
const $star: TextStyle = {
  color: colors.error,
  fontSize: 14,
  marginTop: -2
}
