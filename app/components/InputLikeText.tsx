import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, spacing, typography } from "../theme"
import { Text } from "./Text"
import { Ionicons } from '@expo/vector-icons'
export interface InputLikeTextProps {

  
  style?: StyleProp<ViewStyle>
  lable?: string
  text?: string
  mandetory?: boolean
  placeholder?: string,
  placeholderTx?: string,
  icon?: boolean
}

/**
 * Describe your component here
 */
export const InputLikeText = observer(function InputLikeText(props: InputLikeTextProps) {
  const { style, lable, placeholder,placeholderTx, mandetory, text, icon } = props
  const $styles = [$container, style]

  return (
    <View style={$styles}>

      {lable && <View style={{ flexDirection: 'row' }}>
        <Text style={$lable}>{lable}</Text>
        {mandetory && <Text style={$star}>*</Text>}
      </View>}
      <View style={$wrapper}>
        {icon && <Ionicons name="ios-search" style={{ marginRight: 10 }} size={24} color={colors.palette.secondary500} />}
        <Text  style={$text}>{text ?? placeholder}</Text>
      </View>

    </View>
  )
})

const $container: ViewStyle = {

}
const $wrapper: ViewStyle = {
  height: 45,
  borderRadius: 7,
  borderColor: colors.palette.neutral300,
  borderWidth: 1,
  flexDirection: 'row',
  alignItems: 'center',
  paddingHorizontal: spacing.sm,
  backgroundColor: colors.palette.neutral100,
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.neutral700,
}
const $lable: TextStyle = {
  fontFamily: typography.primary.medium,
  fontSize: 14,
  color: colors.palette.neutral700,
}

const $star: TextStyle = {
  color: colors.error,
  fontSize: 14,
  marginTop: -2
}
