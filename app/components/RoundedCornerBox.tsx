import * as React from "react"
import { StyleProp, ViewStyle, Dimensions,Animated, Keyboard } from "react-native"
import { observer } from "mobx-react-lite"
import { colors } from "../theme"
import { useEffect } from "react"

const { height } = Dimensions.get('window')
export interface RoundedCornerBoxProps {
  childrens: React.ReactElement
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  height?: Animated.Value

}

/**
 * Describe your component here
 */
export const RoundedCornerBox = observer(function RoundedCornerBox(props: RoundedCornerBoxProps) {
  const { style,height:animatedHeight } = props
  
  const [keyboardOffset, setKeyboardOffset] = React.useState(0);
  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => {
      setKeyboardOffset(e.endCoordinates.height-20);
    });

    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardOffset(0);
    });

    return () => {
      keyboardDidShowListener.remove();
      keyboardDidHideListener.remove();
    };
  }, []);
  const $styles = [$container, style,{height:animatedHeight},{bottom:keyboardOffset}]
  return (
    <Animated.View style={$styles}>
      {props.childrens}
    </Animated.View>
  )
})

const $container: ViewStyle = {
 
  minHeight: height / 2.7,
  backgroundColor: colors.palette.neutral100,
  position: 'absolute',
  bottom: 0, left: 0, right: 0,
  borderTopLeftRadius: 28,
  borderTopRightRadius: 28,
  paddingLeft: 16,
  paddingRight: 16,
  paddingBottom:50


}


