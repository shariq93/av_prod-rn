import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle, Dimensions, Animated, Pressable, Easing } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "../theme"
import { Text } from "./Text"
const { width } = Dimensions.get('window')
export interface AnimatedTabBarProps {
  /**
   * An optional style override useful for padding & margin.
   */
  tabs: any
  onSelectedTab?: (index: number) => void
  style?: StyleProp<ViewStyle>
}

/**
 * Describe your component here
 */
export const AnimatedTabBar = observer(function AnimatedTabBar(props: AnimatedTabBarProps) {
  const { style, tabs, onSelectedTab } = props
 
  const [selectdIndex, setSelectedIndex] = React.useState(0)

  let tabCount = tabs.length
  const selectorWitdh = (width - 35) / tabCount
  const $styles = [$container, style,{width:selectorWitdh*tabCount}]
  const $translateX = React.useRef(new Animated.Value(0)).current
  const animateToIndex = (index) => {
    Animated.timing($translateX, {
      toValue: ((selectorWitdh * index)),
      duration: 200,
      easing: Easing.ease,
      useNativeDriver: true
    }).start()
  }
  return (
    <View style={$styles}>
      <Animated.View style={{
        backgroundColor: colors.palette.primary600, height: 45, width: selectorWitdh,
        position: 'absolute', borderRadius: mesures.borderRadius, transform: [{ translateX: $translateX }]
      }}></Animated.View>
      <View style={{ flexDirection: 'row' }}>
        {tabs.map((item, index) => {
          return <Pressable key={item.name} style={{ width: selectorWitdh }} onPress={() => {
            setSelectedIndex(index)
            animateToIndex(index)
            if(onSelectedTab) onSelectedTab(index)
          }}>
            <View style={{ width: selectorWitdh, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={index == selectdIndex ? $textSelected : $text}>{item.name}</Text>
            </View>
          </Pressable>
        })}


      </View>

    </View>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
  backgroundColor: colors.palette.neutral100,
  minHeight: 45,

  borderRadius: mesures.borderRadius
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 13,
  color: colors.palette.neutral400,
  
}
const $textSelected: TextStyle = {
  fontFamily: typography.primary.semiBold,
  fontSize: 13,
  color: colors.palette.neutral100,
}
