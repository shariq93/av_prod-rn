import * as React from "react"
import { StyleProp, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
import { Asset } from "app/models"
import { TagView } from "./TagView"

export interface AvailableAssetListItemProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  item: Asset
  onPress: () => void
}

/**
 * Describe your component here
 */
export const AvailableAssetListItem = observer(function AvailableAssetListItem(props: AvailableAssetListItemProps) {
  const { style, item, onPress } = props
  const $styles = [$container, style]
  const in_bundle = item.in_bundle == '1' && item.in_project == "0" && item.in_maintainance == '0';
  const in_project =  item.in_project == "1"


  return (
    <TouchableOpacity

      disabled={item.loan_status == '1' || item.in_maintainance == '1'}
      onPress={() => {
        onPress()
      }} style={$styles}>
      <Text style={{ fontFamily: typography.primary.semiBold }}>{item.name}</Text>
      <View style={{ flexDirection: 'row' }}>

        <Text size="xs" style={{}}>{item.tagid}</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        {in_bundle && <TagView width={100} color='#6ab04c' text='In Bundle' />}
        {in_project && <TagView color={'#e67e22'} text='In Project' />}
        {item.loan_status == '1' && <TagView color={colors.error} text='Not Availabe' />}
      </View>




    </TouchableOpacity>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
  backgroundColor: '#fff',
  borderRadius: mesures.borderRadius,
  padding: spacing.sm
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
