import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle, Dimensions } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, typography } from "../theme"
import { Text } from "./Text"
import { Dialog } from 'react-native-simple-dialogs';
import { Ionicons } from '@expo/vector-icons';
import { Button } from "./Button"
const { height, width } = Dimensions.get('window');
const BUTTON_HALF_WIDTH = width / 2 - 60;
const BUTTON_WIDTH = width - 90;
export interface DialogsProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  show: boolean
  type: 'success' | 'error' | 'info' | 'warning'
  showIcon?: boolean
  title?: string
  discription: string
  onPossitivePress: () => void
  onNegativePress: () => void
  possitiveText: string
  negativeText?: string
}

/**
 * Describe your component here
 */
export const Dialogs = observer(function Dialogs(props: DialogsProps) {
  const { style, show, type, showIcon, title, discription, onPossitivePress, onNegativePress, possitiveText, negativeText } = props
  const [visible, setVisile] = React.useState(false)

  React.useEffect(() => {
    setVisile(show)
  }, [show])
  const ICON_SIZE = 50
  const $styles = [$container, style]
  const renderIcon = (type: 'success' | 'error' | 'info' | 'warning'): any => {
    if (type == 'success') {
      return <Ionicons style={$iconStyle} name="checkmark-circle" size={ICON_SIZE} color={colors.palette.success} />
    }
    if (type == 'error') {
      return <Ionicons style={$iconStyle} name="alert-circle" size={ICON_SIZE} color={colors.palette.error} />
    }
    if (type == 'info') {
      return <Ionicons style={$iconStyle} name="information-circle" size={ICON_SIZE} color={colors.palette.info} />
    }
    if (type == 'warning') {
      return <Ionicons style={$iconStyle} name="warning" size={ICON_SIZE} color={colors.palette.warning} />
    }
  }
  return (
    <Dialog
      dialogStyle={{ backgroundColor: '#fff', borderRadius: 7 }}
      visible={visible}
     >
      <View style={$container}>
        {showIcon && renderIcon(type)}
        {title && <Text
          style={$title}
          text={title}
        />}
        <Text
          style={$discription}
          text={discription}
        />
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15, justifyContent: 'center' }}>
          {onNegativePress && <>
            <Button

              textStyle={{ color: colors.palette.neutral100 }}
              onPress={onNegativePress}
              style={$buttonError}
              text={negativeText}

            ></Button>
            <View style={{ width: 15 }} />
          </>}
          <Button

            textStyle={{ color: colors.palette.neutral100 }}
            
            style={[...$buttonSuccess, { width: (onNegativePress == undefined ? BUTTON_WIDTH : BUTTON_HALF_WIDTH) }]}
            onPress={onPossitivePress}
            text={possitiveText}
          ></Button>
        </View>
      </View>
    </Dialog>
  )
})
export function useDialog() {
  const [isVisible, setIsVisible] = React.useState(false);
  const [meta,setMeta ] = React.useState<any>();

  const show = () => {
    setIsVisible(true);
  };

  const hide = () => {
    setIsVisible(false);
  };



  return { isVisible,meta,setMeta, show, hide };
}
const $container: ViewStyle = {
  justifyContent: "center",
  alignItems: 'center'
}

const $iconStyle: ViewStyle = {
  marginBottom: 10
}

const $title: TextStyle = {
  fontFamily: typography.primary.semiBold,
  fontSize: 18,
  textAlign: 'center',
  color: colors.palette.neutral800,
}
const $discription: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  textAlign: 'center',
  color: colors.palette.neutral600,
}
const $button: ViewStyle = {

  width: BUTTON_HALF_WIDTH,
  borderRadius: 7,
}
const $buttonSuccess = [
  $button,
  {
    backgroundColor: colors.palette.primary600,


  }
]
const $buttonError = [
  $button,
  {
    backgroundColor: colors.palette.neutral400,
  }
]

const $buttonText: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  textAlign: 'center',
  color: colors.palette.neutral600,
}

