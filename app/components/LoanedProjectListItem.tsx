import * as React from "react"
import { StyleProp, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
import { LoanProject } from "app/models"
import moment from "moment"

export interface LoanedProjectListItemProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  item: LoanProject
  onPress: () => void
}

/**
 * Describe your component here
 */
export const LoanedProjectListItem = observer(function LoanedProjectListItem(props: LoanedProjectListItemProps) {
  const { style, item,onPress } = props
  const $styles = [$container, style]

  return (
    <TouchableOpacity onPress={() => {
      onPress()
    }} style={$styles}>
      <Text style={{ fontFamily: typography.primary.semiBold }}>{item.name}</Text>
      <View style={{ flexDirection: 'row' }}>
        <Text size="xs">Bundles: </Text>
        <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{item.bundles.length} item(s)</Text>
      </View>

      <View style={{ flexDirection: 'row' }}>
        <Text size="xs">Loanee: </Text>
        <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{item.loanee.name}</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <Text size="xs">Loaned On: </Text>
        <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{moment(item.loan_date).format('MM-DD-YYYY')}</Text>
      </View>


    </TouchableOpacity>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
  backgroundColor: '#fff',
  borderRadius: mesures.borderRadius,
  padding: spacing.sm
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
