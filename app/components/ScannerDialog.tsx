import * as React from "react"
import { FlatList, NativeModules, DeviceEventEmitter, StyleProp, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
import { Dialog } from "react-native-simple-dialogs"
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { useEffect, useState } from "react"
import { Button } from "./Button"
const { RFIDReaderModule } = NativeModules;
export interface ScannerDialogProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  title: string
  items: any[]
  type: string
  hideTitle?: boolean
  onDismiss: () => void
  onScan: (tags: any[]) => void
  openDialog?: boolean
}

/**
 * Describe your component here
 */
export const ScannerDialog = observer(function ScannerDialog(props: ScannerDialogProps) {
  const { style, title, onDismiss, onScan, type, items, hideTitle, openDialog } = props
  const $styles = [$container, style]
  const [visible, setVisile] = useState(false)
  const [scannedTags, setScannedTags] = useState([])
  const [intervalId, setIntervalId] = useState<any>(-1)
  const [isScanning, setIsScanning] = useState(false)
  const recievedTags = React.useRef([]).current

  useEffect(() => {
    setVisile(openDialog)
  }, [openDialog])


  if (RFIDReaderModule) {
    // Listen for scanned data event
    DeviceEventEmitter.addListener('onTagFound', (event) => {
      if (event?.tag) {
        if (!recievedTags.includes(event?.tag))
          recievedTags.push(event?.tag)
      }

    });



  }
  return (
    <View>
      <TouchableOpacity onPress={() => {
        setVisile(true)
      }}>
        {!hideTitle && <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 10 }}>
          <MaterialCommunityIcons name="cellphone-nfc" size={24} color={colors.palette.primary500} />
          <Text style={{ fontFamily: typography.primary.semiBold, color: colors.palette.primary500 }}>{title}</Text>
        </View>}
      </TouchableOpacity>
      <Dialog
        dialogStyle={{ backgroundColor: '#fff', borderRadius: 7, maxHeight: 470 }}
        visible={visible}
        onShow={() => {
          if (RFIDReaderModule) {
            RFIDReaderModule.initDevice()
          }

          const intId = setInterval(() => {
            console.log(recievedTags);

            setScannedTags([...recievedTags])

          }, 2000);
          setIntervalId(intId)
        }}
        onTouchOutside={() => {
          onDismiss()
          clearInterval(intervalId)
          if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
          setVisile(false)
        }} >
        <View style={$container}>
          {title && <Text style={$title} size='md' text={title} />}
          <FlatList
            data={items}
            style={{ maxHeight: 300 }}
            ItemSeparatorComponent={() => <View style={{ height: 1, backgroundColor: '#ccc', marginTop: 2 }} />}
            renderItem={({ item }) => {
              return <View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                  <View>
                    <Text size="xs">{item.name}</Text>
                    <Text style={$generalText}>{item.tagId}</Text>
                  </View>
                  {scannedTags.includes(item.tagId) && <View>
                    <AntDesign name="checkcircle" size={18} color={colors.success} />
                  </View>}
                </View>

              </View>
            }}

            ListEmptyComponent={() => <View>
              <Text size="xs">No data available!</Text>
            </View>}
          />

          <Button


            onPress={() => {
              if (!isScanning) {
                if (RFIDReaderModule) RFIDReaderModule.onScanPressed()
              } else {
                clearInterval(intervalId)
                if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
              }

              setIsScanning(!isScanning)
            }} text={isScanning ? 'StopScanning' : "Start Scanning"} style={{ backgroundColor: colors.palette.primary600, minHeight: 40, marginTop: 10 }} textStyle={{
              color: colors.palette.neutral100, fontSize: 14
            }}></Button>
          <Button
            disabled={scannedTags.length < 1}

            onPress={() => {
              onScan(scannedTags)
              onDismiss()
              if (RFIDReaderModule) RFIDReaderModule.stopRFScanner()
              clearInterval(intervalId)
            }} text="Select" style={{ backgroundColor: scannedTags.length < 1 ? colors.palette.neutral300 : colors.palette.primary600, minHeight: 40, marginTop: 10 }} textStyle={{
              color: colors.palette.neutral100, fontSize: 14
            }}></Button>
        </View>
      </Dialog >
    </View >
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
const $title: TextStyle = {
  fontFamily: typography.primary.medium,
  color: colors.palette.primary600
}
const $card: ViewStyle = {
  padding: spacing.xs,
  backgroundColor: colors.palette.neutral200,
  marginTop: spacing.sm,
  borderRadius: mesures.borderRadius,

}
const $listTitle: TextStyle = {
  fontFamily: typography.primary.semiBold,
  fontSize: 12,
  color: colors.palette.primary500,
}

const $generalText: TextStyle = {
  fontFamily: typography.primary.light,
  fontSize: 12,
  color: colors.palette.neutral900,
}