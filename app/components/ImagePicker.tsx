import * as React from "react"
import {  StyleProp, TextStyle, View, ViewStyle, TouchableOpacity } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, typography } from "../theme"
import { Text } from "./Text"
import { Feather, AntDesign } from '@expo/vector-icons';
import ImageView from "react-native-image-viewing";
import { Asset, Callback, CameraOptions, ImagePickerResponse, launchCamera, launchImageLibrary } from 'react-native-image-picker';
import Image from 'react-native-fast-image'
export interface ImagePickerProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  type: 'gallery' | 'camera'
  options?: CameraOptions
  onImagePick?: (image: any) => void
  onRemove?: (image: any) => void
  showRemove?: boolean
}

/**
 * Describe your component here
 */
export const ImagePicker = observer(function ImagePicker(props: ImagePickerProps) {
  const { style, type, options, onImagePick, onRemove,showRemove } = props
  const $styles = [$container, style]
  const [imageResponse, setimageResponse] = React.useState<Asset>(undefined)
  const [imageSelected, setImageSelected] = React.useState(false)
  const [visible, setIsVisible] = React.useState(false);
  const $options: CameraOptions = {
    mediaType: 'photo',
    maxWidth: 500,
    maxHeight: 500,
    quality: .5,
    includeBase64: true
  }
  const $cameraOptions = { ...$options, ...options }
  const onGetImage = (results: ImagePickerResponse) => {

    if (!results.didCancel) {
      let singleImage = results.assets[0]
      setImageSelected(true)
      setimageResponse(singleImage)
      onImagePick(singleImage)
    }

  }


  return (
    <View style={$styles}>
      <TouchableOpacity

        onPress={() => {
          if (!imageSelected) {
            if (type == 'camera') {
              // launchCamera({ ...$cameraOptions, cameraType: 'back' }).then((result) => {
                launchCamera($cameraOptions, onGetImage)
                setImageSelected(false)
              // })
            }
            if (type == 'gallery') {
              setImageSelected(false)
              launchImageLibrary($cameraOptions, onGetImage)
            }
          } else {
            setIsVisible(true)
          }

        }}>
        {!imageSelected && <View style={$imageContainer}>

          {type == 'gallery' && <Feather name="image" size={50} color={colors.palette.neutral500} />}
          {type == 'camera' && <Feather name="camera" size={50} color={colors.palette.neutral500} />}

        </View>}
        {imageSelected && <View style={$imageContainer}>

          <Image source={{ uri: imageResponse?.uri }} style={{ width: 90, height: 90, borderRadius: 7, }} ></Image>
         { showRemove && <TouchableOpacity onPress={() => {
            onRemove(imageResponse)
            setImageSelected(false)
            setimageResponse(undefined)
          }} style={{ position: 'absolute', top: -5, right: -5 }}>
            <AntDesign name="closecircle" size={24} color={colors.palette.neutral500} />

          </TouchableOpacity>}
        </View>}
      </TouchableOpacity>
      <ImageView
        images={[{ uri: imageResponse?.uri }]}
        imageIndex={0}
        visible={visible}
        onRequestClose={() => setIsVisible(false)}
      />
    </View>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
}

const $imageContainer: ViewStyle = {
  width: 110, height: 110, backgroundColor: colors.palette.neutral300, borderRadius: 7, justifyContent: 'center', alignItems: 'center'
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
