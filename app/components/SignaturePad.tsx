import * as React from "react"
import { StyleProp, TextStyle, TouchableOpacity, View, ViewStyle, useWindowDimensions } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
import Signature, { SignatureViewRef } from "react-native-signature-canvas";
import { useRef } from "react"
import { Ionicons } from '@expo/vector-icons';
export interface SignaturePadProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  onRemove: () => void
  confirmed?: boolean
  onSuccess: (signature: string) => void
}

/**
 * Describe your component here
 */
export const SignaturePad = observer(function SignaturePad(props: SignaturePadProps) {
  const { style, onRemove, onSuccess, confirmed } = props
  const $styles = [$container, style]
  const { width } = useWindowDimensions()
  const imgWidth = width - (spacing.sm * 2);
  const imgHeight = 200;
  const signature_style = `.m-signature-pad {box-shadow: none; border: none; } 
                .m-signature-pad--body {border: none;}
                .m-signature-pad--footer {display: none; margin: 0px;}
                body,html {
                width: ${imgWidth}px; height: ${imgHeight}px;}`;

  const ref = useRef<SignatureViewRef>();

  // Called after ref.current.readSignature() reads a non-empty base64 string
  const handleOK = (signature) => {
    console.log({ signature });
    onSuccess(signature)


  };

  // Called after ref.current.readSignature() reads an empty string
  const handleEmpty = () => {
    console.log("Empty");
    onRemove();
  };

  // Called after ref.current.clearSignature()
  const handleClear = () => {
    console.log("clear success!");
    onRemove();
  };
  // Called after ref.current.getData()
  const handleData = (data) => {
    console.log(data);
  };
  const checkStyle = { borderWidth: 2, borderColor: colors.success, borderRadius: mesures.borderRadius }
  return (
    <View style={[$styles, (confirmed ? checkStyle : {})]}>

      <Signature
        ref={ref}
        style={{ height: 200, marginTop: 5 }}
        overlayWidth={imgWidth}
        overlayHeight={imgHeight}
        webStyle={signature_style}
        onOK={handleOK}
        onEmpty={handleEmpty}
        onClear={handleClear}
        
        onGetData={handleData}
        imageType="image/png"
        autoClear={false}
        confirmText="Save"
      />
      <View style={{ flexDirection: 'row',justifyContent:'flex-end'}}>
       { <TouchableOpacity
          onPress={() => {
            if (ref.current) ref.current.clearSignature()
          }}
          style={{ padding: spacing.md }}>
          <Text style={{ color: colors.error }}>Clear</Text>
        </TouchableOpacity>}
        {!confirmed && <TouchableOpacity
          onPress={() => {
            if (ref.current) ref.current.readSignature()
          }}
          style={{ padding: spacing.md }}>
          <Text style={{ color: colors.success }}>Confirm</Text>
        </TouchableOpacity>}
      </View>
      {confirmed && <View style={{ position: 'absolute', top: -10, right: -10 }}>
        <Ionicons name="checkmark-circle" size={30} color={colors.success} />
      </View>}
    </View>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
  height: 240,
  borderRadius: mesures.borderRadius,
  backgroundColor: '#FFF'

}


