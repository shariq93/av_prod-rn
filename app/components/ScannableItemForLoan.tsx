import * as React from "react"
import { Pressable, StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
import { AntDesign } from '@expo/vector-icons';
import { TagData } from "app/models"
import { TagView } from "./TagView"
export interface ScannableItemForLoanProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  scanned?: boolean
  item: TagData
  childs?: TagData[]
  scannedTags?: any[]
  showChilds?: boolean
  onPress?: () => void
  showScanDetails?: boolean
  hideTagId?:boolean
}

/**
 * Describe your component here
 */
export const ScannableItemForLoan = observer(function ScannableItemForLoan(props: ScannableItemForLoanProps) {
  const { style, scanned, item, childs, scannedTags, showChilds, onPress, showScanDetails ,hideTagId} = props
  const $styles = [$container, style]
  const getScannedTagsCount = (tags: TagData[], scannedTags: any[]) => {

    return tags?.filter(it => it.scannable && scannedTags?.includes(it?.tagId)).length
  }

  const getIconColor = (tags: TagData[], scannedTags: any[]) => {
    const scannedCount = getScannedTagsCount(tags, scannedTags)
    const childsCount = tags?.length
    if (scannedCount == childsCount)
      return colors.success
    else return colors.palette.info
  }

  return (
    <Pressable onPress={() => {
      if (onPress) onPress()
    }} style={$card}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <View>
          <Text >{item.name}</Text>
          {!hideTagId && <Text style={$generalText}>{item.tagId}</Text>}
        </View>
        {scanned && !showScanDetails && <View>
          <AntDesign name="checkcircle" size={18} color={colors.success} />
        </View>}
        {scanned && showScanDetails && <View>
          <AntDesign name="checkcircle" size={18} color={getIconColor(childs, scannedTags)} />
        </View>}

      </View>
      {showScanDetails && <View>

        <Text style={$generalText}>Scanned Assets: {getScannedTagsCount(childs, scannedTags)} / {childs.length}</Text>
      </View>

      }
      {item.maintainance && <TagView color={colors.palette.warning} text={"Maintenance"} />}
      {showChilds && childs.length == 0 && <Text size='xs' style={{ color: colors.textDim, alignSelf: 'center' }} >{'No Asset available'}</Text>}
      {showChilds && childs?.map((it) => {
        return <View style={{ marginLeft: 10, flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ height: 40, width: 1, backgroundColor: '#ccc' }} />
          <View style={{ marginLeft: 10, flex: 1 }}>
            <Text >{it.name}</Text>
            <Text style={$generalText}>{(it.tagid || it.tagId)}</Text>
          </View>
          {scannedTags.includes((it.tagid || it.tagId)) && <View>
            <AntDesign name="checkcircle" size={18} color={colors.success} />
          </View>}
        </View>
      })}
    </Pressable>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
}



const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
const $title: TextStyle = {
  fontFamily: typography.primary.medium,
  color: colors.palette.primary600
}
const $card: ViewStyle = {
  padding: spacing.xs,
  backgroundColor: colors.palette.neutral100,
  marginTop: spacing.sm,
  borderRadius: mesures.borderRadius,

}
const $listTitle: TextStyle = {
  fontFamily: typography.primary.semiBold,
  fontSize: 12,
  color: colors.palette.primary500,
}

const $generalText: TextStyle = {
  fontFamily: typography.primary.light,
  fontSize: 13,
  color: colors.palette.neutral900,
}