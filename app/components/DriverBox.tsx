import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle, Dimensions, Pressable } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
const { width } = Dimensions.get('window')
export interface DriverBoxProps {
  /**
   * An optional style override useful for padding & margin.
   */
  title: string
  content: string
  style?: StyleProp<ViewStyle>
  onPress: () => void
}

/**
 * Describe your component here
 */
export const DriverBox = observer(function DriverBox(props: DriverBoxProps) {

  const { title, content, onPress } = props

  return (
    <View style={$root}>
      <Pressable
        onPress={() => onPress()}
      >
        <View
          style={$box}
        >
          <Text style={$title}>{title}</Text>
          <View
            style={{ height: 15 }}
          />
          <Text style={$content}>{content}</Text>

        </View>
      </Pressable>
    </View>
  )
})

const boxWidth = (width / 2) - (spacing.sm * 2)
const $root: ViewStyle = {


}
const $title: TextStyle = {
  fontSize: 16, color: colors.palette.accent100,
  paddingHorizontal: 20,
  textAlign: 'center',
  fontFamily: typography.primary.semiBold
}
const $content: TextStyle = {
  fontSize: 24, color: colors.palette.accent100,
  paddingHorizontal: 20,
  textAlign: 'center',
  fontFamily: typography.primary.semiBold
}
const $box: ViewStyle = {
  width: boxWidth, height: boxWidth,
  backgroundColor: colors.palette.primary600,
  borderRadius: mesures.borderRadius,
  justifyContent: 'center', alignItems: 'center'
}
