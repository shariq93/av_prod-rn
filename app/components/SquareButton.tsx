import * as React from "react"
import { Pressable, StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, typography } from "app/theme"
import { Text } from "app/components/Text"
import { FontAwesome } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { TouchableOpacity } from "react-native-gesture-handler"
export interface SquareButtonProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  icon: 'handshake-o' | 'text-box-check-outline' | 'tools'
  lable: string
  onPress: () => void
}

/**
 * Describe your component here
 */
export const SquareButton = observer(function SquareButton(props: SquareButtonProps) {
  const { style, icon, lable, onPress } = props
  const $styles = [$container, style]

  return (
    <Pressable onPress={() => {
      onPress()
    }} style={$styles}>
      {icon == 'handshake-o' && <FontAwesome name="handshake-o" size={30} color={colors.palette.primary600} />}
      {icon == 'text-box-check-outline' && <MaterialCommunityIcons name="text-box-check-outline" size={30} color={colors.palette.primary600} />}
      {icon == 'tools' && <MaterialCommunityIcons name="tools" size={30} color={colors.palette.primary600} />}
      <Text style={$text}>{lable}</Text>
    </Pressable>
  )
})

const $container: ViewStyle = {
  justifyContent: "center", alignItems: 'center',
  height: 95, width: 110, backgroundColor: '#fff',
  borderRadius: mesures.borderRadius, borderWidth: 1, borderColor: colors.separator
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.neutral900,
}
