import React from 'react';
import { StyleSheet,  View } from 'react-native';
import { colors, typography } from '../theme';
import { Text, TextProps } from "./Text"
export const HeadingWithUnderline = (props: any) => {
  return (
    <View style={styles.container}>
      <Text size='xl'text={props.label}  style={styles.txt}></Text>
      <View
        style={{
          ...styles.underLine,
         
        }}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
  
    alignSelf: 'center',
    justifyContent: 'center',
   
  },
  txt: {
  
   
    color: '#000000',
    fontFamily: typography.primary.bold,
    textAlign:"center"
  },
  underLine: {
    height:3.5,
    width:70,
    borderRadius:3.5/2,
    marginTop:7,
    backgroundColor:colors.palette.primary600,
    alignSelf: 'center',
  },
});
