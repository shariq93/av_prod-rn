import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, typography } from "../theme"
import { Text } from "./Text"
import { TextField } from "./TextField"
import { Ionicons } from '@expo/vector-icons';
import { useRef } from "react"
export interface SearchFieldProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  placeholder?: string
  onChangeText?: (text: string) => void
  onSubmit?: (text: string) => void
  value?: string
}

/**
 * Describe your component here
 */
export const SearchField = observer(function SearchField(props: SearchFieldProps) {
  const { style, onChangeText, onSubmit, value, placeholder } = props
  const $styles = [$container, style]
  const textInputRef = useRef(null);
  return (
    <View style={$styles}>
      <TextField
        ref={textInputRef}
        inputWrapperStyle={$fieldStyle}
        placeholder={placeholder || "Search"}
        value={value}
        returnKeyType='search'
        onChangeText={onChangeText}
        placeholderTextColor={colors.border}
        onSubmitEditing={(e) => {
          onSubmit(e.nativeEvent.text)
          // Clear the text input
          if (textInputRef.current) {
            textInputRef.current?.clear();
            textInputRef.current?.focus();
          }

          // Refocus on the text input
        
        }}
        LeftAccessory={() => <View style={{ paddingLeft: 10 }}>
          <Ionicons name="ios-search" size={24} color={colors.palette.secondary500} />
        </View>}
      ></TextField>
    </View>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
}
const $fieldStyle: TextStyle = {
  borderColor: colors.border,
  borderWidth: mesures.borderWidthNormal,
  borderRadius: mesures.borderRadius,
  backgroundColor: colors.palette.neutral100,
  justifyContent: 'center', alignItems: 'center',
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
