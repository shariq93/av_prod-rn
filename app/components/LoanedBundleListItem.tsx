import * as React from "react"
import { StyleProp, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
import { LoanBundle } from "app/models"
import moment from "moment"
import { ScannerDialog } from "./ScannerDialog"
import { useState } from "react"
import { useNavigation } from "@react-navigation/native"

export interface LoanedBundleListItemProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  item: LoanBundle
  onPress: () => void
}

/**
 * Describe your component here
 */
export const LoanedBundleListItem = observer(function LoanedBundleListItem(props: LoanedBundleListItemProps) {
  const { style, item, onPress } = props
  const $styles = [$container, style]
  const [showScannerDialog, setShowScannerDialog] = useState(false)
  // navigation.navigate('BundleDetails', { bundle: item })
  const navigation = useNavigation()
  return (
    <View>
      <ScannerDialog
        hideTitle
        title={"Scan Bundle"}
        openDialog={showScannerDialog}
        items={[{ name: item.name, tagId: (item.tagId || item.tagid) }]} type={"BUNDLE"} onDismiss={function (): void {
          setShowScannerDialog(false)
        }} onScan={function (items): void {
          if (items.includes(item.tagId || item.tagid)) {
            navigation.navigate('BundleDetails', { bundle: item })
          }
        }}
      />
      <TouchableOpacity onPress={() => {
        if (onPress) {
          onPress()
        } else {
          const items = [{ name: item.name, tagId: (item.tagid || item.tagId) }]
          navigation.navigate('Scanner', { items: items, type: 'BUNDLE', bundleAssets: item })
        }
        // setShowScannerDialog(true)
      }} style={$styles}>
        <Text style={{ fontFamily: typography.primary.semiBold }}>{item.name}</Text>
        <Text size="xs" style={{ fontFamily: typography.primary.normal }}>{item.tagid || item.tagId}</Text>
        <View style={{ flexDirection: 'row' }}>
          <Text size="xs">Assets: </Text>
          <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{item.assets.length} item(s)</Text>
        </View>

        {item.loanee.name && <View style={{ flexDirection: 'row' }}>
          <Text size="xs">Loanee: </Text>
          <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{item.loanee.name}</Text>
        </View>}
        {item.loan_date && <View style={{ flexDirection: 'row' }}>
          <Text size="xs">Loaned On: </Text>
          <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{moment(item.loan_date).format('MM-DD-YYYY')}</Text>
        </View>}


      </TouchableOpacity>
    </View>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
  backgroundColor: '#fff',
  borderRadius: mesures.borderRadius,
  padding: spacing.sm
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
