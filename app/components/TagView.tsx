import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"

export interface TagViewProps {
  /**
   * An optional style override useful for padding & margin.
   */
  color: string
  text: string
  width?:number
  style?: StyleProp<ViewStyle>
}

/**
 * Describe your component here
 */
export const TagView = observer(function TagView(props: TagViewProps) {
  const { style, color, text,width } = props
  const $styles = [$container, style]

  return (

    <View style={{ ...$container, backgroundColor: color,width }}>
      <View style={{ height: 8, width: 8, borderRadius: 8 / 2, backgroundColor: '#fff', marginRight: 10 }} />
      <Text style={$text}>{text}</Text>
    </View>

  )
})

const $container: ViewStyle = {
  flexDirection: 'row',
  
  maxWidth: 140,alignItems:'center',paddingHorizontal:spacing.xs,
  borderRadius:7,
  marginRight:5,
}

const $text: TextStyle = {
  fontFamily: typography.primary.semiBold,
  fontSize: 12,
  color: '#fff',
}
