import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle, Dimensions, FlatList, ActivityIndicator, TouchableOpacity } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "../theme"
import { Text } from "./Text"
import { Dialog } from 'react-native-simple-dialogs';
import { Ionicons } from '@expo/vector-icons';
import { Button } from "./Button"
// import { getValueOfKeys, useDebouncedCallback } from "./utils/helpers"
import { ShadowStyle } from "../theme/ShadowStyle"
import { SearchField } from "./SearchField"
import { getValueOfKeys, useDebouncedCallback } from "app/utils/helper"
const { height, width } = Dimensions.get('window');
const BUTTON_HALF_WIDTH = width / 2 - 60;
const BUTTON_WIDTH = width - 90;
export interface ListModalProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  show: boolean
  data: any[]
  titleKeys: string[]
  subTitleKeys?: string[]
  title?: string
  isLoading?: boolean
  loadMore?: () => void
  hasNextPage?: boolean
  hasPagination?: boolean
  onItemSelected: (item: any) => void
  onMultiSelected?: (item: any[]) => void
  multiSelect?: boolean,
  onDismiss: () => void
  filter?: (text: string) => void
}

/**
 * Describe your component here
 */
export const ListModal = observer(function ListModal(props: ListModalProps) {
  const { style, data, show, title, titleKeys, filter, onDismiss, subTitleKeys, onMultiSelected, hasPagination, multiSelect, loadMore, isLoading, hasNextPage, onItemSelected } = props
  const [visible, setVisile] = React.useState(false)
  const [selectedItems, setSelectedItems] = React.useState([])

  React.useEffect(() => {
    setVisile(show)
  }, [show])

  const debouncedSearch = useDebouncedCallback((text: string) => {
    filter(text)
  }, 500);
  return (
    <Dialog
      dialogStyle={{ backgroundColor: '#fff', borderRadius: 7, maxHeight: 400 }}
      visible={visible}
      onTouchOutside={() => {
        onDismiss()
        setVisile(false)
      }} >
      <View style={$container}>
        {title && <Text style={$title} size='md' text={title} />}
        <FlatList
          data={data}
          style={{ maxHeight: 300 }}
          ListHeaderComponent={filter ? <SearchField
            onChangeText={(text) => {
              debouncedSearch(text)
            }} style={{ flex: 1 }}></SearchField> : <View />}
          renderItem={({ item }) => {
            const isSelected = selectedItems.filter(it => it.id == item.id)[0]

            return <TouchableOpacity onPress={() => {
              if (!multiSelect) onItemSelected(item)
              if (multiSelect) {
                if (!selectedItems.includes(item)) {

                  selectedItems.push(item)
                  setSelectedItems([...selectedItems])
                } else {

                  // const selectedIndex =

                  // alert('include, index'+selectedIndex+' - '+selectedItems.slice(selectedIndex,1).length)

                  setSelectedItems(selectedItems.filter(it => it.id != item.id))

                }

              }
            }} style={[$card, isSelected ? { borderColor: colors.palette.success, borderWidth: 1 } : undefined]}>
              <Text size="xs" style={$listTitle}>{getValueOfKeys(item, titleKeys)}</Text>
              {subTitleKeys && <Text
                style={$generalText}
                size='xxs'
                numberOfLines={1}
              > {getValueOfKeys(item, subTitleKeys)}</Text>
              }
            </TouchableOpacity>
          }}
          ListFooterComponent={
            <View>
              {hasPagination && <View>
                {isLoading && <ActivityIndicator size={'small'} color={colors.palette.primary400} />}
                {hasNextPage && !isLoading && <Button onPress={() => {
                  loadMore()
                }} text="Load More" style={{ backgroundColor: colors.transparent, minHeight: 30 }} textStyle={{
                  color: colors.palette.primary600, fontSize: 14
                }}></Button>}
                <View style={{ height: 30 }} />
              </View>}

            </View>
          }
          ListEmptyComponent={() => <View>
            <Text size="xs">No data available!</Text>
          </View>}
        />
        {/* <Button
          // disabled={selectedItems.length < 1}

          onPress={() => {
            onMultiSelected(selectedItems)
          }} text="Select" style={{ backgroundColor: selectedItems.length < 1 ? colors.palette.neutral300 : colors.palette.primary600, minHeight: 40 }} textStyle={{
            color: colors.palette.neutral100, fontSize: 14
          }}></Button> */}
      </View>
    </Dialog>
  )
})

const $container: ViewStyle = {
  paddingBottom: spacing.sm
}

const $title: TextStyle = {
  fontFamily: typography.primary.medium,
  color: colors.palette.primary600
}
const $card: ViewStyle = {
  padding: spacing.xs,
  backgroundColor: colors.palette.neutral200,
  marginTop: spacing.sm,
  borderRadius: mesures.borderRadius,

}
const $listTitle: TextStyle = {
  fontFamily: typography.primary.semiBold,
  fontSize: 12,
  color: colors.palette.primary500,
}

const $generalText: TextStyle = {
  fontFamily: typography.primary.light,
  fontSize: 11,
  color: colors.palette.neutral900,
}