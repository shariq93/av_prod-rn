import * as React from "react"
import { StyleProp, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
import moment from "moment"
import { MaintainanceAsset, useStores } from "app/models"
import { Dialogs } from "./Dialogs"

export interface MaintanceAssetListItemProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  item?: MaintainanceAsset
}

/**
 * Describe your component here
 */
export const MaintanceAssetListItem = observer(function MaintanceAssetListItem(props: MaintanceAssetListItemProps) {
  const { style, item } = props
  const $styles = [$container, style]
  const {maintainance,mainDb} = useStores()
  const [showDialog, setShowDialog] = React.useState(false)

  return (
    <View style={$styles}>
      <Dialogs show={showDialog} showIcon
        title="Remove Asset"
        type={"info"} discription={"Are you sure want to remove " + item?.asset?.name + " from maintanance?"}

        onPossitivePress={async () => {

          maintainance.removeAsset(item.id).then(()=>{
            mainDb.fetchAllData()
          })
          setShowDialog(false)
        }}
        onNegativePress={function (): void {
          setShowDialog(false)
        }}
        negativeText="No"
        possitiveText={"Yes"}>

      </Dialogs>

      <View>
        <Text style={{ fontFamily: typography.primary.semiBold }}>{item?.asset?.name}</Text>
        <Text size="xs">{item?.asset?.tagid}</Text>

        <TouchableOpacity onPress={() => {
          setShowDialog(true)
        }} style={{ alignItems: 'center', marginTop: 10 }}>
          <Text style={{ color: colors.palette.primary600, fontFamily: typography.primary.semiBold }}>Remove</Text>
        </TouchableOpacity>
      </View>



    </View>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
  backgroundColor: '#fff',
  borderRadius: mesures.borderRadius,
  padding: spacing.sm
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
