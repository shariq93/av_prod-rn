import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle, Dimensions } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, typography } from "../theme"
import { Text } from "./Text"
import { Dialog } from 'react-native-simple-dialogs';
import { Ionicons } from '@expo/vector-icons';
import { Button } from "./Button"
const { height, width } = Dimensions.get('window');
const BUTTON_HALF_WIDTH = width / 2 - 60;
const BUTTON_WIDTH = width - 90;
export interface ModalDialogProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  show: boolean
  body: React.ReactNode
  title?:string
  onDismis:()=>void
}

/**
 * Describe your component here
 */
export const ModalDialog = observer(function ModalDialog(props: ModalDialogProps) {
  const { style, show, body,title,onDismis} = props
  const [visible, setVisile] = React.useState(false)

  React.useEffect(() => {
    setVisile(show)
  }, [show])


  return (
    <Dialog
      dialogStyle={{ backgroundColor: '#fff', borderRadius: 7 }}
      visible={visible}
       >
      <View style={$container}>
        {title && <Text style={$title} size='sm' text={title} />}
        {body}
      </View>
    </Dialog>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",

}

const $title: TextStyle = {
  fontFamily:typography.primary.medium,

}


