import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { StyleSheet, Animated } from 'react-native';

import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "../theme"
import { Text } from "./Text"
import { useStores } from "../models";
import { useEffect } from "react";
import { ShadowStyle } from "../theme/ShadowStyle";
import { color } from "react-native-reanimated";
import { translate } from "../i18n";

export interface ToastMessageProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>

}

/**
 * Describe your component here
 */
export const ToastMessage = observer(function ToastMessage(props: ToastMessageProps) {
  const { style, } = props
  const $styles = [$container, style]

  const { toast } = useStores();
  const { isVisible, message, hideToast, type } = toast;

  const COLORS = {
    success: "#2ecc71",
    warning: "#e67e22",
    info: "#f1c40f",
    error: "#fc7978",
  }
  const TITLES = {
    success:"SUCCESS",
    warning: "WARNING!",
    info: "ALERT!",
    error:'ERROR',
  }
  const fadeAnim = new Animated.Value(0);
  const slideAnim = new Animated.Value(-150);
  useEffect(() => {
    if (isVisible) {
      // Hide the toast after 5 seconds
      // const timer = setTimeout(() => {
      //   hideToast();
      // }, 5000);

      // return () => {
      //   clearTimeout(timer);
      // };
    }
  }, [isVisible]);
  const fadeIn = Animated.timing(fadeAnim, {
    toValue: 1,
    duration: 300,
    useNativeDriver: true,
  })
  const fadeOut = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 300,
   
    useNativeDriver: true,
  })
  const slideInAnimation = Animated.timing(slideAnim, {
    toValue: 0,
    duration: 300,
  
    useNativeDriver: true,
  });
  const slideOutAnimation = Animated.timing(slideAnim, {
    toValue: -150,
    duration: 300,
  
    useNativeDriver: true,
  });
  const inAnimation = Animated.parallel([fadeIn, slideInAnimation]);
  const outAnimation = Animated.parallel([fadeOut, slideOutAnimation]);

  const sequenceAnimation = Animated.sequence([inAnimation, Animated.delay(4000), outAnimation]);


  useEffect(() => {
    if (isVisible) {
      // Start the fade-in animation
      sequenceAnimation.start(() => {
        hideToast()
      });

    }
  }, [isVisible]);
  const handleToastPress = () => {
    hideToast();
  };

  if (!isVisible) {
    return null;
  }
  return (
    <Animated.View style={[styles.container, { opacity: fadeAnim, transform: [{ translateY: slideAnim }], }]} >
      <View style={{ height: 5, backgroundColor: COLORS[type] }} />
      <View style={{ padding: spacing.md }}>
        <Text style={$text}>{TITLES[type]}</Text>
        <Text style={styles.messageText}>{message??""}</Text>
      </View>
    </Animated.View>
  );
})

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 60,
    left: 20,
    right: 20,
    borderBottomLeftRadius: mesures.borderRadius,
    borderBottomRightRadius: mesures.borderRadius,
    backgroundColor: '#fff',

    ...ShadowStyle
  },
  messageText: {
    color: '#333',
    fontSize: 16,
  },
});

const $container: ViewStyle = {
  justifyContent: "center",
}

const $text: TextStyle = {
  fontFamily: typography.primary.bold,
  fontSize: 14,
  color: colors.palette.secondary500,
}
