import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
import moment from "moment"
import { Asset, LoanedAsset } from "app/models"

export interface AssetListItemProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  item: LoanedAsset


}

/**
 * Describe your component here
 */
export const AssetListItem = observer(function AssetListItem(props: AssetListItemProps) {
  const { style, item } = props
  const $styles = [$container, style]

  return (
    <View style={$styles}>
      <View>
        {/* <Text>ID: {item.id}</Text> */}
      </View>

      <View>
        <Text style={{ fontFamily: typography.primary.semiBold }}>{item.name}</Text>

        <Text size="xs">{item.tagid}</Text>

       {item.loneeName &&  <View style={{ flexDirection: 'row' }}>
          <Text size="xs">Loanee: </Text>
          <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{item.loneeName}</Text>
        </View>}
        <View style={{ flexDirection: 'row' }}>
          <Text size="xs">Loaned On: </Text>
          <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{moment(item.loadDate).format('MM-DD-YYYY')}</Text>
        </View>
        {item.bundleName && <View style={{ flexDirection: 'row' }}>
          <Text size="xs">Bundle: </Text>
          <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{item.bundleName}</Text>
        </View>}
        {item.projectName && <View style={{ flexDirection: 'row' }}>
          <Text size="xs">Project: </Text>
          <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{item.projectName}</Text>
        </View>}

      </View>


    </View>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
  backgroundColor: '#fff',
  borderRadius: mesures.borderRadius,
  padding: spacing.sm
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
