import * as React from "react"
import { StyleProp, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { colors, mesures, spacing, typography } from "app/theme"
import { Text } from "app/components/Text"
import { Projects } from "app/models"

export interface ProjectListItemProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  item: Projects
  onPress: () => void
}

/**
 * Describe your component here
 */
export const ProjectListItem = observer(function ProjectListItem(props: ProjectListItemProps) {
  const { style, item, onPress } = props
  const $styles = [$container, style]

  return (
    <TouchableOpacity onPress={() => {
      onPress()
    }} style={$styles}>
      <Text style={{ fontFamily: typography.primary.semiBold }}>{item.name}</Text>
      <View style={{ flexDirection: 'row' }}>

        <Text size="xs" style={{ }}>{item.tagid}</Text>
      </View>

      {/* {item.loanee.name && <View style={{ flexDirection: 'row' }}>
        <Text size="xs">Loanee: </Text>
        <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{item.loanee.name}</Text>
      </View>}
      {item.loan_date && <View style={{ flexDirection: 'row' }}>
        <Text size="xs">Loaned On: </Text>
        <Text size="xs" style={{ fontFamily: typography.primary.semiBold }}>{moment(item.loan_date).format('MM-DD-YYYY')}</Text>
      </View>} */}


    </TouchableOpacity>
  )
})

const $container: ViewStyle = {
  justifyContent: "center",
  backgroundColor: '#fff',
  borderRadius: mesures.borderRadius,
  padding: spacing.sm
}

const $text: TextStyle = {
  fontFamily: typography.primary.normal,
  fontSize: 14,
  color: colors.palette.primary500,
}
